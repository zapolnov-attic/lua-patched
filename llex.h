/*
** $Id: llex.h,v 1.72.1.1 2013/04/12 18:48:47 roberto Exp $
** Lexical Analyzer
** See Copyright Notice in lua.h
*/

#ifndef llex_h
#define llex_h

#include "lobject.h"
#include "lzio.h"


#define LUA_FIRST_RESERVED	257



/*
* WARNING: if you change the order of this enumeration,
* grep "ORDER RESERVED"
*/
enum LUA_RESERVED {
  /* terminal symbols denoted by reserved words */
  LUA_TK_AND = LUA_FIRST_RESERVED, LUA_TK_BREAK,
  LUA_TK_DO, LUA_TK_ELSE, LUA_TK_ELSEIF, LUA_TK_END, LUA_TK_FALSE, LUA_TK_FOR, LUA_TK_FUNCTION,
  LUA_TK_GOTO, LUA_TK_IF, LUA_TK_IN, LUA_TK_LOCAL, LUA_TK_NIL, LUA_TK_NOT, LUA_TK_OR, LUA_TK_REPEAT,
  LUA_TK_VEC2,
  LUA_TK_RETURN, LUA_TK_THEN, LUA_TK_TRUE, LUA_TK_UNTIL, LUA_TK_WHILE,
  /* other terminal symbols */
  LUA_TK_MALFORMED_NUMBER, LUA_TK_MALFORMED_STRING, LUA_TK_UNFINISHED_STRING, LUA_TK_UNFINISHED_COMMENT,
  LUA_TK_INVALID_LONG_STRING_DELIMITER, LUA_TK_COMMENT,
  LUA_TK_CONCAT, LUA_TK_DOTS, LUA_TK_EQ, LUA_TK_GE, LUA_TK_LE, LUA_TK_NE, LUA_TK_DBCOLON, LUA_TK_EOS,
  LUA_TK_NUMBER, LUA_TK_NAME, LUA_TK_STRING
};

/* number of reserved words */
#define LUA_NUM_RESERVED	(lua_cast(int, LUA_TK_WHILE-LUA_FIRST_RESERVED+1))


typedef union {
  lua_Number r;
  lua_TString *ts;
} lua_SemInfo;  /* semantics information */


typedef struct lua_Token {
  int token;
  size_t start, end;
  lua_SemInfo seminfo;
} lua_Token;


/* state of the lexer plus state of the parser when shared by all
   functions */
typedef struct lua_LexState {
  int current;  /* current character (charint) */
  size_t currentoff;
  int linenumber;  /* input line counter */
  int lastline;  /* line of last token `consumed' */
  int sep;
  int in_long_string;
  int in_long_comment;
  lua_Token t;  /* current token */
  lua_Token lookahead;  /* look ahead token */
  struct lua_FuncState *fs;  /* current function (parser) */
  struct lua_State *L;
  lua_ZIO *z;  /* input stream */
  lua_Mbuffer *buff;  /* buffer for tokens */
  struct lua_Dyndata *dyd;  /* dynamic structures used by the parser */
  lua_TString *source;  /* current source name */
  lua_TString *envn;  /* environment variable name */
  char decpoint;  /* locale decimal point */
  void * custom_lexer;
  void (* highlight_error)(struct lua_LexState * state, size_t start, size_t end, int baseToken);
} lua_LexState;


LUAI_FUNC void luaX_init (lua_State *L);
LUAI_FUNC void luaX_setinput (lua_State *L, lua_LexState *ls, lua_ZIO *z,
                              lua_TString *source, int firstchar);
LUAI_FUNC lua_TString *luaX_newstring (lua_LexState *ls, const char *str, size_t l);
LUAI_FUNC void luaX_next (lua_LexState *ls);
LUAI_FUNC int luaX_lookahead (lua_LexState *ls);
LUAI_FUNC lua_noret luaX_syntaxerror (lua_LexState *ls, const char *s);
LUAI_FUNC const char *luaX_token2str (lua_LexState *ls, int token);

#endif
