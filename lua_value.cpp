/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "lua_value.hpp"
#include <sstream>
#include <stdexcept>
#include <cassert>

Lua::Value::Value(const Value & src)
{
	*(lua_TValue *)this = *(const lua_TValue *)&src;
	acquire();
}

Lua::Value & Lua::Value::operator=(const Value & src)
{
	src.acquire();
	release();
	*(lua_TValue *)this = *(const lua_TValue *)&src;
	return *this;
}

void Lua::Value::acquire() const
{
	int ttyp = lua_ttypenv(this);
	switch (ttyp)
	{
	case LUA_TNONE:
	case LUA_TNIL:
	case LUA_TBOOLEAN:
	case LUA_TLIGHTUSERDATA:
	case LUA_TNUMBER:
	case LUA_TVEC2:
		return;

	case LUA_TSTRING:
		lua_assert(lua_tsvalue(this)->refcount < LUA_MAX_REFCOUNT);
		++lua_tsvalue(this)->refcount;
		lua_setbit(lua_tsvalue(this)->marked, LUA_FIXEDBIT);
		return;

	case LUA_TTABLE:
	case LUA_TUSERDATA:
	case LUA_TTHREAD:
	case LUA_TUSERPOINTER:
		lua_assert(lua_gcvalue(this)->gch.refcount < LUA_MAX_REFCOUNT);
		++lua_gcvalue(this)->gch.refcount;
		return;

	case LUA_TFUNCTION:
		if (!lua_ttislcf(this))
		{
			lua_assert(lua_gcvalue(this)->gch.refcount < LUA_MAX_REFCOUNT);
			++lua_gcvalue(this)->gch.refcount;
		}
		return;
	}

	std::stringstream ss;
	ss << "invalid lua value type " << ttyp << '.';
	throw std::runtime_error(ss.str());
}

void Lua::Value::release()
{
	int ttyp = lua_ttypenv(this);
	switch (ttyp)
	{
	case LUA_TNONE:
	case LUA_TNIL:
	case LUA_TBOOLEAN:
	case LUA_TLIGHTUSERDATA:
	case LUA_TNUMBER:
	case LUA_TVEC2:
		return;

	case LUA_TSTRING:
		lua_assert(lua_tsvalue(this)->refcount > 0);
		if (--lua_tsvalue(this)->refcount == 0)
			lua_resetbit(lua_tsvalue(this)->marked, LUA_FIXEDBIT);
		return;

	case LUA_TTABLE:
	case LUA_TUSERDATA:
	case LUA_TTHREAD:
	case LUA_TUSERPOINTER:
		lua_assert(lua_gcvalue(this)->gch.refcount > 0);
		--lua_gcvalue(this)->gch.refcount;
		return;

	case LUA_TFUNCTION:
		if (!lua_ttislcf(this))
		{
			lua_assert(lua_gcvalue(this)->gch.refcount > 0);
			--lua_gcvalue(this)->gch.refcount;
		}
		return;
	}

	std::stringstream ss;
	ss << "invalid lua value type " << ttyp << '.';
	throw std::runtime_error(ss.str());
}
