/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "lua_rtti.hpp"
#include "lua_vm.hpp"
#include "lua_function.hpp"
#include <new>
#include <cstdio>

#include RTTI_TYPE_H
#include RTTI_TYPE_CONVERSION_H
#include RTTI_META_OBJECT_H
#include RTTI_OBJECT_H
#include RTTI_PROPERTY_H
#include RTTI_METHOD_H
#include RTTI_CONSTRUCTOR_H

class Test : public RTTI::Object
{
	RTTI_OBJECT(Test)
		RTTI_PARENT_CLASS(RTTI::Object)
		RTTI_CONSTRUCTOR2(int, float)
		RTTI_READONLY_PROPERTY(int, prop)
		RTTI_PROPERTY(int, prop2, setProp2)
		RTTI_METHOD(meth)
	END_RTTI_OBJECT

public:
	inline Test() { printf("Default constructor\n"); }
	inline Test(int a, float b) { printf("Constructor %d %f\n", a, b); }
	inline ~Test() { printf("Destructor.\n"); }

	inline int prop() const { return 31; }

	inline void setProp2(int propval) { printf("Set prop2 %d\n", propval); }
	inline int prop2() const { return 87; }

	inline bool meth(long l, double d) { printf("meth(): %ld %f\n", l, d); return true; }
};

#define DOSTRING(STR) \
	fprintf(stderr, "--------------------------\n"); \
	fprintf(stderr, "%s\n", STR); \
	fprintf(stderr, "--------------------------\n"); \
	vm.doString(STR, std::nothrow)

static void test(Lua::VM & vm, int a, float b)
{
	lua_Number x = lua_tonumber(vm.L, lua_upvalueindex(1));
	printf("%f %d %f\n", x, a, b);
}

static int test2(int a, float b)
{
	printf("%d %f\n", a, b);
	return 321;
}

int main()
{
	Lua::VM vm;

	new (Lua::alloc<Test>(vm)) Test();

	lua_pushliteral(vm.L, "newTest");
	Lua::pushConstructorForClass<Test>(vm);
	vm.rawSet(vm.globals());

	DOSTRING("test = newTest()");
	DOSTRING("test = newTest(61, 93)");

	DOSTRING("print(test.prop)");
	DOSTRING("print(test.prop2)");

	DOSTRING("test.prop = 9");
	DOSTRING("test.prop2 = 11");

	DOSTRING("print(test.meth(91, 75))");
	DOSTRING("print(test:meth(92, 74))");
	DOSTRING("test.meth = function() end");

	lua_pushnumber(vm.L, 17);
	Lua::pushClosure(vm, test, 1);
	lua_pushnumber(vm.L, 18);
	lua_pushnumber(vm.L, 35.98);
	lua_call(vm.L, 2, 0);

	Lua::pushFunction(vm, test2);
	lua_pushnumber(vm.L, 25);
	lua_pushnumber(vm.L, 27.931);
	lua_call(vm.L, 2, 1);
	printf("%f\n", (float)lua_tonumber(vm.L, -1));

	return 0;
}
