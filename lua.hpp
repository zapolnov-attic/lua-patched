// lua.hpp
// Lua header files for C++
// <<extern "C">> not supplied automatically because Lua also compiles as C++
#ifndef __9fac7d7bf2a86babec4b57d3904495c8__
#define __9fac7d7bf2a86babec4b57d3904495c8__

extern "C" {
#include "lapi.h"
#include "lcode.h"
#include "lctype.h"
#include "ldebug.h"
#include "ldo.h"
#include "lfunc.h"
#include "lgc.h"
#include "llex.h"
#include "llimits.h"
#include "lmem.h"
#include "lobject.h"
#include "lopcodes.h"
#include "lparser.h"
#include "lstate.h"
#include "lstring.h"
#include "ltable.h"
#include "ltm.h"
#include "lua.h"
#include "luaconf.h"
#include "lualib.h"
#include "lauxlib.h"
#include "lundump.h"
#include "lvm.h"
#include "lzio.h"
}

#endif
