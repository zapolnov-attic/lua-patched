/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __ffee047c1ba1f50ae2344f9744ce17b2__
#define __ffee047c1ba1f50ae2344f9744ce17b2__

#include "lua.hpp"
#include <string>
#include <utility>

#ifdef RTTI_OBJECT_H
 #include RTTI_OBJECT_H
 #include RTTI_META_OBJECT_H
#endif

namespace Lua
{
	class Value;

	class VM
  #ifdef RTTI_OBJECT_H
		: public RTTI::Object
  #endif
	{
  #ifdef RTTI_OBJECT_H
		RTTI_OBJECT(Lua::VM)
			RTTI_PARENT_CLASS(RTTI::Object)
		END_RTTI_OBJECT
  #endif

	public:
		lua_State * const L;

		VM();
		~VM();

		static inline VM & fromL(lua_State * L) { return *reinterpret_cast<VM *>(LUA_G(L)->ud); }

		inline lua_Table * registry() { return lua_hvalue(&LUA_G(L)->l_registry); }
		inline lua_Table * globals()
			{ rawGetI(registry(), LUA_RIDX_GLOBALS); --L->top; return lua_hvalue(L->top); }

		void doString(const char * str, const char * name = NULL);
		bool doString(const char * str, const std::nothrow_t &);
		bool doString(const char * str, const char * name, const std::nothrow_t &);

		void doFile(const char * name);
		bool doFile(const char * name, const std::nothrow_t &);

		std::string stackTrace(const char * msg = NULL, int level = 0);

		void call(int nargs = 0, int nresults = 0);
		bool pcall(int nargs = 0, int nresults = 0);

		inline bool toBoolean(const lua_TValue * o) const { return !lua_isfalse(o); }
		lua_Integer toInteger(const lua_TValue * o) const;
		lua_Unsigned toUnsigned(const lua_TValue * o) const;
		lua_Number toNumber(const lua_TValue * o) const;
		void * toUserData(const lua_TValue * o) const;
		std::pair<float, float> toVec2(const lua_TValue * o) const;
		std::string toStdString(const lua_TValue * o) const;

		void push(const Value & value);
		lua_Table * pushNewTable(int na = 0, int nr = 0)
			{ lua_createtable(L, na, nr); return lua_hvalue(L->top - 1); }
		inline void pushTable(lua_Table * table) { lua_sethvalue(L, L->top, table); lua_api_incr_top(L); }

		Value pop();

		inline void rawGet(lua_Table * table)
			{ lua_checknelems(L, 1); lua_setobj2s(L, L->top - 1, luaH_get(table, L->top - 1)); }
		inline void rawGetI(lua_Table * table, int key)
			{ lua_setobj2s(L, L->top, luaH_getint(table, key)); lua_api_incr_top(L); }
		inline void rawGetP(lua_Table * table, const void * key)
		{
			lua_setpvalue(L->top, lua_cast(void *, key));
			lua_setobj2s(L, L->top, luaH_get(table, L->top));
			lua_api_incr_top(L);
		}

		void rawSet(lua_Table * table);
		void rawSetI(lua_Table * table, int key);
		void rawSetP(lua_Table * table, const void * key);

		inline void tableGet(lua_Table * table)
			{ lua_TValue v; lua_sethvalue(L, &v, table); luaV_gettable(L, &v, L->top - 1, L->top - 1); }
		inline void tableSet(lua_Table * table)
			{ lua_TValue v; lua_sethvalue(L, &v, table); luaV_settable(L, &v, L->top - 2, L->top - 1); }

		bool next(lua_Table * table);

		lua_Table * persistentTableForKey(const void * key, bool * isNew = NULL);

	private:
		static int errorHandler(lua_State * L);

		void reportCallError(int status);

		VM(const VM &);
		VM & operator=(const VM &);
	};
}

#endif
