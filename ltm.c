/*
** $Id: ltm.c,v 2.14.1.1 2013/04/12 18:48:47 roberto Exp $
** Tag methods
** See Copyright Notice in lua.h
*/


#include <string.h>

#define ltm_c
#define LUA_CORE

#include "lua.h"

#include "lobject.h"
#include "lstate.h"
#include "lstring.h"
#include "ltable.h"
#include "ltm.h"


static const char udatatypename[] = "userdata";

LUAI_DDEF const char *const luaT_typenames_[LUA_TOTALTAGS] = {
  "no value",
  "nil", "boolean", udatatypename, "number",
  "string", "table", "function", udatatypename, "thread", "object",
  "vec2",
  "proto", "upval"  /* these last two cases are used for tests only */
};


void luaT_init (lua_State *L) {
  static const char *const luaT_eventname[] = {  /* ORDER TM */
    "__index", "__newindex",
    "__gc", "__mode", "__len", "__eq",
    "__add", "__sub", "__mul", "__div", "__mod",
    "__pow", "__unm", "__lt", "__le",
    "__concat", "__call"
  };
  int i;
  for (i=0; i<LUA_TM_N; i++) {
    LUA_G(L)->tmname[i] = luaS_new(L, luaT_eventname[i]);
    luaS_fix(LUA_G(L)->tmname[i]);  /* never collect these names */
  }
}


/*
** function to be used with macro "lua_fasttm": optimized for absence of
** tag methods
*/
const lua_TValue *luaT_gettm (lua_Table *events, lua_TMS event, lua_TString *ename) {
  const lua_TValue *tm = luaH_getstr(events, ename);
  lua_assert(event <= LUA_TM_EQ);
  if (lua_ttisnil(tm)) {  /* no tag method? */
    events->flags |= lua_cast_byte(1u<<event);  /* cache this fact */
    return NULL;
  }
  else return tm;
}


const lua_TValue *luaT_gettmbyobj (lua_State *L, const lua_TValue *o, lua_TMS event) {
  lua_Table *mt;
  switch (lua_ttypenv(o)) {
    case LUA_TTABLE:
      mt = lua_hvalue(o)->metatable;
      break;
    case LUA_TUSERPOINTER:
      mt = lua_uptrvalue(o)->metatable;
      break;
    case LUA_TUSERDATA:
      mt = lua_uvalue(o)->metatable;
      break;
    default:
      mt = LUA_G(L)->mt[lua_ttypenv(o)];
  }
  return (mt ? luaH_getstr(mt, LUA_G(L)->tmname[event]) : luaO_nilobject);
}

