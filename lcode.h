/*
** $Id: lcode.h,v 1.58.1.1 2013/04/12 18:48:47 roberto Exp $
** Code generator for Lua
** See Copyright Notice in lua.h
*/

#ifndef lcode_h
#define lcode_h

#include "llex.h"
#include "lobject.h"
#include "lopcodes.h"
#include "lparser.h"


/*
** Marks the end of a patch list. It is an invalid value both as an absolute
** address, and as a list link (would link an element to itself).
*/
#define LUA_NO_JUMP (-1)


/*
** grep "ORDER OPR" if you change these enums  (ORDER OP)
*/
typedef enum lua_BinOpr {
  LUA_OPR_VEC2,
  LUA_OPR_ADD, LUA_OPR_SUB, LUA_OPR_MUL, LUA_OPR_DIV, LUA_OPR_MOD, LUA_OPR_POW,
  LUA_OPR_CONCAT,
  LUA_OPR_EQ, LUA_OPR_LT, LUA_OPR_LE,
  LUA_OPR_NE, LUA_OPR_GT, LUA_OPR_GE,
  LUA_OPR_AND, LUA_OPR_OR,
  LUA_OPR_NOBINOPR
} lua_BinOpr;


typedef enum lua_UnOpr { LUA_OPR_MINUS, LUA_OPR_NOT, LUA_OPR_LEN, LUA_OPR_NOUNOPR } lua_UnOpr;


#define lua_getcode(fs,e)	((fs)->f->code[(e)->u.info])

#define luaK_codeAsBx(fs,o,A,sBx)	luaK_codeABx(fs,o,A,(sBx)+LUA_MAXARG_sBx)

#define luaK_setmultret(fs,e)	luaK_setreturns(fs, e, LUA_MULTRET)

#define luaK_jumpto(fs,t)	luaK_patchlist(fs, luaK_jump(fs), t)

LUAI_FUNC int luaK_codeABx (lua_FuncState *fs, lua_OpCode o, int A, unsigned int Bx);
LUAI_FUNC int luaK_codeABC (lua_FuncState *fs, lua_OpCode o, int A, int B, int C);
LUAI_FUNC int luaK_codek (lua_FuncState *fs, int reg, int k);
LUAI_FUNC void luaK_fixline (lua_FuncState *fs, int line);
LUAI_FUNC void luaK_nil (lua_FuncState *fs, int from, int n);
LUAI_FUNC void luaK_reserveregs (lua_FuncState *fs, int n);
LUAI_FUNC void luaK_checkstack (lua_FuncState *fs, int n);
LUAI_FUNC int luaK_stringK (lua_FuncState *fs, lua_TString *s);
LUAI_FUNC int luaK_numberK (lua_FuncState *fs, lua_Number r);
LUAI_FUNC void luaK_dischargevars (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC int luaK_exp2anyreg (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC void luaK_exp2anyregup (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC void luaK_exp2nextreg (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC void luaK_exp2val (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC int luaK_exp2RK (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC void luaK_self (lua_FuncState *fs, lua_expdesc *e, lua_expdesc *key);
LUAI_FUNC void luaK_indexed (lua_FuncState *fs, lua_expdesc *t, lua_expdesc *k);
LUAI_FUNC void luaK_goiftrue (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC void luaK_goiffalse (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC void luaK_storevar (lua_FuncState *fs, lua_expdesc *var, lua_expdesc *e);
LUAI_FUNC void luaK_setreturns (lua_FuncState *fs, lua_expdesc *e, int nresults);
LUAI_FUNC void luaK_setoneret (lua_FuncState *fs, lua_expdesc *e);
LUAI_FUNC int luaK_jump (lua_FuncState *fs);
LUAI_FUNC void luaK_ret (lua_FuncState *fs, int first, int nret);
LUAI_FUNC void luaK_patchlist (lua_FuncState *fs, int list, int target);
LUAI_FUNC void luaK_patchtohere (lua_FuncState *fs, int list);
LUAI_FUNC void luaK_patchclose (lua_FuncState *fs, int list, int level);
LUAI_FUNC void luaK_concat (lua_FuncState *fs, int *l1, int l2);
LUAI_FUNC int luaK_getlabel (lua_FuncState *fs);
LUAI_FUNC void luaK_prefix (lua_FuncState *fs, lua_UnOpr op, lua_expdesc *v, int line);
LUAI_FUNC void luaK_infix (lua_FuncState *fs, lua_BinOpr op, lua_expdesc *v);
LUAI_FUNC void luaK_posfix (lua_FuncState *fs, lua_BinOpr op, lua_expdesc *v1,
                            lua_expdesc *v2, int line);
LUAI_FUNC void luaK_setlist (lua_FuncState *fs, int base, int nelems, int tostore);


#endif
