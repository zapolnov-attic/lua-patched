/*
** $Id: lstate.h,v 2.82.1.1 2013/04/12 18:48:47 roberto Exp $
** Global State
** See Copyright Notice in lua.h
*/

#ifndef lstate_h
#define lstate_h

#include "lua.h"

#include "lobject.h"
#include "ltm.h"
#include "lzio.h"


/*

** Some notes about garbage-collected objects:  All objects in Lua must
** be kept somehow accessible until being freed.
**
** Lua keeps most objects linked in list g->allgc. The link uses field
** 'next' of the lua_CommonHeader.
**
** Strings are kept in several lists headed by the array g->strt.hash.
**
** Open upvalues are not subject to independent garbage collection. They
** are collected together with their respective threads. Lua keeps a
** double-linked list with all open upvalues (g->uvhead) so that it can
** mark objects referred by them. (They are always gray, so they must
** be remarked in the atomic step. Usually their contents would be marked
** when traversing the respective threads, but the thread may already be
** dead, while the upvalue is still accessible through closures.)
**
** Objects with finalizers are kept in the list g->finobj.
**
** The list g->tobefnz links all objects being finalized.

*/


struct lua_longjmp;  /* defined in ldo.c */



/* extra stack space to handle TM calls and some other extras */
#define LUA_EXTRA_STACK   5


#define LUA_BASIC_STACK_SIZE        (2*LUA_MINSTACK)


/* kinds of Garbage Collection */
#define LUA_KGC_NORMAL	0
#define LUA_KGC_EMERGENCY	1	/* gc was forced by an allocation failure */
#define LUA_KGC_GEN		2	/* generational collection */


typedef struct lua_stringtable {
  lua_GCObject **hash;
  lua_uint32 nuse;  /* number of elements */
  int size;
} lua_stringtable;


/*
** information about a call
*/
typedef struct lua_CallInfo {
  lua_StkId func;  /* function index in the stack */
  lua_StkId	top;  /* top for this function */
  struct lua_CallInfo *previous, *next;  /* dynamic call link */
  short nresults;  /* expected number of results from this function */
  lua_ubyte callstatus;
  ptrdiff_t extra;
  union {
    struct {  /* only for Lua functions */
      lua_StkId base;  /* base for this function */
      const lua_Instruction *savedpc;
    } l;
    struct {  /* only for C functions */
      int ctx;  /* context info. in case of yields */
      lua_CFunction k;  /* continuation in case of yields */
      ptrdiff_t old_errfunc;
      lua_ubyte old_allowhook;
      lua_ubyte status;
    } c;
  } u;
} lua_CallInfo;


/*
** Bits in lua_CallInfo status
*/
#define LUA_CIST_LUA	(1<<0)	/* call is running a Lua function */
#define LUA_CIST_HOOKED	(1<<1)	/* call is running a debug hook */
#define LUA_CIST_REENTRY	(1<<2)	/* call is running on same invocation of
                                   luaV_execute of previous call */
#define LUA_CIST_YIELDED	(1<<3)	/* call reentered after suspension */
#define LUA_CIST_YPCALL	(1<<4)	/* call is a yieldable protected call */
#define LUA_CIST_STAT	(1<<5)	/* call has an error status (pcall) */
#define LUA_CIST_TAIL	(1<<6)	/* call was tail called */
#define LUA_CIST_HOOKYIELD	(1<<7)	/* last hook called yielded */


#define isLua(ci)	((ci)->callstatus & LUA_CIST_LUA)


/*
** `global state', shared by all threads of this state
*/
typedef struct global_State {
  lua_Alloc frealloc;  /* function to reallocate memory */
  void *ud;         /* auxiliary data to `frealloc' */
  lua_umem totalbytes;  /* number of bytes currently allocated - GCdebt */
  lua_mem GCdebt;  /* bytes allocated not yet compensated by the collector */
  lua_umem GCmemtrav;  /* memory traversed by the GC */
  lua_umem GCestimate;  /* an estimate of the non-garbage memory in use */
  lua_stringtable strt;  /* hash table for strings */
  lua_TValue l_registry;
  unsigned int seed;  /* randomized seed for hashes */
  lua_ubyte currentwhite;
  lua_ubyte gcstate;  /* state of garbage collector */
  lua_ubyte gckind;  /* kind of GC running */
  lua_ubyte gcrunning;  /* true if GC is running */
  int sweepstrgc;  /* position of sweep in `strt' */
  lua_GCObject *allgc;  /* list of all collectable objects */
  lua_GCObject *finobj;  /* list of collectable objects with finalizers */
  lua_GCObject **sweepgc;  /* current position of sweep in list 'allgc' */
  lua_GCObject **sweepfin;  /* current position of sweep in list 'finobj' */
  lua_GCObject *gray;  /* list of gray objects */
  lua_GCObject *grayagain;  /* list of objects to be traversed atomically */
  lua_GCObject *weak;  /* list of tables with weak values */
  lua_GCObject *ephemeron;  /* list of ephemeron tables (weak keys) */
  lua_GCObject *allweak;  /* list of all-weak tables */
  lua_GCObject *tobefnz;  /* list of userdata to be GC */
  lua_UpVal uvhead;  /* head of double-linked list of all open upvalues */
  lua_Mbuffer buff;  /* temporary buffer for string concatenation */
  int gcpause;  /* size of pause between successive GCs */
  int gcmajorinc;  /* pause between major collections (only in gen. mode) */
  int gcstepmul;  /* GC `granularity' */
  lua_CFunction panic;  /* to be called in unprotected errors */
  struct lua_State *mainthread;
  const lua_Number *version;  /* pointer to version number */
  lua_TString *memerrmsg;  /* memory-error message */
  lua_TString *tmname[LUA_TM_N];  /* array with tag-method names */
  struct lua_Table *mt[LUA_NUMTAGS];  /* metatables for basic types */
} global_State;


/*
** `per thread' state
*/
struct lua_State {
  lua_CommonHeader;
  lua_ubyte status;
  lua_StkId top;  /* first free slot in the stack */
  global_State *l_G;
  lua_CallInfo *ci;  /* call info for current function */
  const lua_Instruction *oldpc;  /* last pc traced */
  lua_StkId stack_last;  /* last free slot in the stack */
  lua_StkId stack;  /* stack base */
  int stacksize;
  unsigned short nny;  /* number of non-yieldable calls in stack */
  unsigned short nCcalls;  /* number of nested C calls */
  lua_ubyte hookmask;
  lua_ubyte allowhook;
  int basehookcount;
  int hookcount;
  lua_Hook hook;
  lua_GCObject *openupval;  /* list of open upvalues in this stack */
  lua_GCObject *gclist;
  struct lua_longjmp *errorJmp;  /* current error recover point */
  ptrdiff_t errfunc;  /* current error handling function (stack index) */
  lua_CallInfo base_ci;  /* lua_CallInfo for first level (C calling Lua) */
};


#define LUA_G(L)	(L->l_G)


/*
** Union of all collectable objects
*/
union lua_GCObject {
  lua_GCheader gch;  /* common header */
  union lua_TString ts;
  union lua_Udata u;
  union lua_Closure cl;
  struct lua_Table h;
  struct lua_Proto p;
  struct lua_UpVal uv;
  struct lua_State th;  /* thread */
};


#define lua_gch(o)		(&(o)->gch)

/* macros to convert a lua_GCObject into a specific value */
#define lua_rawgco2ts(o)  \
	lua_check_exp(lua_novariant((o)->gch.tt) == LUA_TSTRING, &((o)->ts))
#define lua_gco2ts(o)	(&lua_rawgco2ts(o)->tsv)
#define lua_rawgco2u(o)	lua_check_exp((o)->gch.tt == LUA_TUSERDATA, &((o)->u))
#define lua_gco2u(o)	(&lua_rawgco2u(o)->uv)
#define lua_rawgco2uptr(o)	lua_check_exp((o)->gch.tt == LUA_TUSERPOINTER, &((o)->u))
#define lua_gco2uptr(o)	(&lua_rawgco2uptr(o)->uv)
#define lua_gco2lcl(o)	lua_check_exp((o)->gch.tt == LUA_TLCL, &((o)->cl.l))
#define lua_gco2ccl(o)	lua_check_exp((o)->gch.tt == LUA_TCCL, &((o)->cl.c))
#define lua_gco2cl(o)  \
	lua_check_exp(lua_novariant((o)->gch.tt) == LUA_TFUNCTION, &((o)->cl))
#define lua_gco2t(o)	lua_check_exp((o)->gch.tt == LUA_TTABLE, &((o)->h))
#define lua_gco2p(o)	lua_check_exp((o)->gch.tt == LUA_TPROTO, &((o)->p))
#define lua_gco2uv(o)	lua_check_exp((o)->gch.tt == LUA_TUPVAL, &((o)->uv))
#define lua_gco2th(o)	lua_check_exp((o)->gch.tt == LUA_TTHREAD, &((o)->th))

/* macro to convert any Lua object into a lua_GCObject */
#define lua_obj2gco(v)	(lua_cast(lua_GCObject *, (v)))


/* actual number of total bytes allocated */
#define lua_gettotalbytes(g)	((g)->totalbytes + (g)->GCdebt)

LUAI_FUNC void luaE_setdebt (global_State *g, lua_mem debt);
LUAI_FUNC void luaE_freethread (lua_State *L, lua_State *L1);
LUAI_FUNC lua_CallInfo *luaE_extendCI (lua_State *L);
LUAI_FUNC void luaE_freeCI (lua_State *L);


#endif

