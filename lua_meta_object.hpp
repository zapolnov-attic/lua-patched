/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __b2855db4430a3b277103e4eee2b76dc0__
#define __b2855db4430a3b277103e4eee2b76dc0__

#include "lua_vm.hpp"
#include "lua_value.hpp"

#include RTTI_TYPE_H
#include RTTI_META_OBJECT_H
#include RTTI_METHOD_H
#include RTTI_CONSTRUCTOR_H
#include RTTI_PROPERTY_H

namespace Lua
{
	class Object;

	class MetaObject : public RTTI::TypeInfo, public RTTI::MetaObject
	{
	public:
		MetaObject(const std::string & name);
		~MetaObject();

		inline void setDestructor(const Lua::Value & destructor) { m_Destructor = destructor; }

	private:
		Lua::Value m_Destructor;

		inline MetaObject * getThis() { return this; }

		MetaObject(const MetaObject &);
		MetaObject & operator=(const MetaObject &);

		friend class Lua::Object;
	};

	class Constructor : public RTTI::Object, public RTTI::Constructor
	{
		RTTI_OBJECT(Constructor)
			RTTI_PARENT_CLASS(RTTI::Object)
		END_RTTI_OBJECT

	public:
		Constructor(Lua::VM & vm, const Lua::MetaObject * metaObj, const Lua::Value & function);

		RTTI::Object * invokeDynamic(size_t n, const RTTI::ConstReference * args) const;
		RTTI::Object * pinvokeDynamic(void * ptr, size_t n, const RTTI::ConstReference * args) const;

	private:
		Lua::VM & m_VM;
		const Lua::MetaObject * m_MetaObject;
		Lua::Value m_Function;

		Constructor(const Constructor &);
		Constructor & operator=(const Constructor &);
	};

	class Method : public RTTI::Object, public RTTI::Method
	{
		RTTI_OBJECT(Method)
			RTTI_PARENT_CLASS(RTTI::Object)
		END_RTTI_OBJECT

	public:
		Method(Lua::VM & vm, const char * nm, const Lua::Value & function);

		void invokeDynamic(RTTI::Object * object, const RTTI::Reference & out,
			size_t n, const RTTI::ConstReference * args) const;

	private:
		Lua::VM & m_VM;
		Lua::Value m_Function;

		Method(const Method &);
		Method & operator=(const Method &);
	};

	class Property : public RTTI::Object, public RTTI::Property
	{
		RTTI_OBJECT(Property)
			RTTI_PARENT_CLASS(RTTI::Object)
		END_RTTI_OBJECT

	public:
		Property(Lua::VM & vm, const char * nm, const Lua::Value & getter, const Lua::Value & setter);

		void getValue(const RTTI::Object * object, void * output, RTTI::Type outputType) const;
		void setValue(RTTI::Object * object, const void * ptr, RTTI::Type valueType) const;

	private:
		Lua::VM & m_VM;
		Lua::Value m_Getter;
		Lua::Value m_Setter;

		Property(const Property &);
		Property & operator=(const Property &);
	};

	class ObjectBase : public RTTI::Object
	{
		RTTI_OBJECT(ObjectBase)
			RTTI_PARENT_CLASS(RTTI::Object)
		END_RTTI_OBJECT
	};

	class Object : public ObjectBase
	{
	public:
		inline Lua::VM & vm() const { return m_VM; }
		inline lua_Table * table() const { return m_Table; }

		virtual const ::RTTI::MetaObject * metaObject() const { return m_MetaObject; }

	private:
		Lua::VM & m_VM;
		const Lua::MetaObject * m_MetaObject;
		lua_Table * m_Table;

		Object(Lua::VM & vmref, const Lua::MetaObject * metaObj);
		~Object();

		static const RTTI::MetaObject * staticMetaObject() { return Lua::ObjectBase::staticMetaObject(); }

		Object(const Object &);
		Object & operator=(const Object &);

		friend class Method;
		friend class Constructor;
		friend class Property;
	};
}

#endif
