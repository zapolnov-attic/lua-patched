/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "lua_rtti.hpp"
#include "lua_stack_entry.hpp"
#include "lua_value.hpp"
#include <sstream>
#include <stdexcept>
#include <new>
#include <cstdlib>
#include <cstring>

#ifdef _MSC_VER
 #include <malloc.h>
 #define alloca _alloca
#endif

#include RTTI_TYPE_H
#include RTTI_TYPE_CONVERSION_H
#include RTTI_META_OBJECT_H
#include RTTI_OBJECT_H
#include RTTI_PROPERTY_H
#include RTTI_METHOD_H
#include RTTI_CONSTRUCTOR_H
#include RTTI_ENUM_H

using RTTI::Constructor;
using RTTI::ConstReference;
using RTTI::Property;
using RTTI::Method;
using RTTI::MetaObject;

char Lua::IDX_META_OBJECT;
char Lua::IDX_PROPERTIES_TABLE;
char Lua::IDX_METHODS_TABLE;

/* Type casts */

RTTI_TYPE_CONVERSION(bool, Lua::StackEntry, result = value.toBoolean());
RTTI_TYPE_CONVERSION(char, Lua::StackEntry, result = static_cast<char>(value.toInteger()));
RTTI_TYPE_CONVERSION(signed char, Lua::StackEntry, result = static_cast<signed char>(value.toInteger()));
RTTI_TYPE_CONVERSION(unsigned char, Lua::StackEntry, result = static_cast<unsigned char>(value.toUnsigned()));
RTTI_TYPE_CONVERSION(short, Lua::StackEntry, result = static_cast<short>(value.toInteger()));
RTTI_TYPE_CONVERSION(unsigned short, Lua::StackEntry, result = static_cast<unsigned short>(value.toUnsigned()));
RTTI_TYPE_CONVERSION(int, Lua::StackEntry, result = static_cast<int>(value.toInteger()));
RTTI_TYPE_CONVERSION(unsigned int, Lua::StackEntry, result = static_cast<unsigned int>(value.toUnsigned()));
RTTI_TYPE_CONVERSION(long, Lua::StackEntry, result = static_cast<long>(value.toNumber()));
RTTI_TYPE_CONVERSION(unsigned long, Lua::StackEntry, result = static_cast<unsigned long>(value.toNumber()));
RTTI_TYPE_CONVERSION(long long, Lua::StackEntry, result = static_cast<long long>(value.toNumber()));
RTTI_TYPE_CONVERSION(unsigned long long, Lua::StackEntry, result = static_cast<unsigned long long>(value.toNumber()));
RTTI_TYPE_CONVERSION(float, Lua::StackEntry, result = static_cast<float>(value.toNumber()));
RTTI_TYPE_CONVERSION(double, Lua::StackEntry, result = static_cast<double>(value.toNumber()));
RTTI_TYPE_CONVERSION(long double, Lua::StackEntry, result = static_cast<long double>(value.toNumber()));
RTTI_TYPE_CONVERSION(void *, Lua::StackEntry, result = value.toUserData());
RTTI_TYPE_CONVERSION(const void *, Lua::StackEntry, result = value.toUserData());
RTTI_TYPE_CONVERSION(std::string, Lua::StackEntry, result = value.toStdString());
RTTI_TYPE_CONVERSION(RTTI::Object *, Lua::StackEntry, result = Lua::toObject<RTTI::Object>(*value.vm, value.ptr()));
RTTI_TYPE_CONVERSION(RTTI::MetaObject *, Lua::StackEntry, result = Lua::toObject<RTTI::MetaObject>(*value.vm, value.ptr()));
RTTI_TYPE_CONVERSION(const RTTI::Object *, Lua::StackEntry, result = Lua::toObject<RTTI::Object>(*value.vm, value.ptr()));
RTTI_TYPE_CONVERSION(const RTTI::MetaObject *, Lua::StackEntry, result = Lua::toObject<RTTI::MetaObject>(*value.vm, value.ptr()));

RTTI_TYPE_CONVERSION(Lua::StackEntry, bool, result.setBoolean(value));
RTTI_TYPE_CONVERSION(Lua::StackEntry, char, result.setInteger(static_cast<lua_Integer>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, signed char, result.setInteger(static_cast<lua_Integer>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, unsigned char, result.setUnsigned(static_cast<lua_Unsigned>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, short, result.setInteger(static_cast<lua_Integer>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, unsigned short, result.setUnsigned(static_cast<lua_Unsigned>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, int, result.setInteger(static_cast<lua_Integer>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, unsigned int, result.setUnsigned(static_cast<lua_Unsigned>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, long, result.setNumber(static_cast<lua_Number>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, unsigned long, result.setNumber(static_cast<lua_Number>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, long long, result.setNumber(static_cast<lua_Number>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, unsigned long long, result.setNumber(static_cast<lua_Number>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, float, result.setNumber(static_cast<lua_Number>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, double, result.setNumber(static_cast<lua_Number>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, long double, result.setNumber(static_cast<lua_Number>(value)));
RTTI_TYPE_CONVERSION(Lua::StackEntry, lua_CFunction, result.setCFunction(value));
RTTI_TYPE_CONVERSION(Lua::StackEntry, void *, result.setLightUserData(value));
RTTI_TYPE_CONVERSION(Lua::StackEntry, const void *, result.setLightUserData(value));
RTTI_TYPE_CONVERSION(Lua::StackEntry, Lua::Value, *result.ptr() = value);

RTTI_TYPE_CONVERSION(Lua::StackEntry, char *,
	lua_pushstring(result.vm->L, value); *result.ptr() = *(--result.vm->L->top));
RTTI_TYPE_CONVERSION(Lua::StackEntry, const char *,
	lua_pushstring(result.vm->L, value); *result.ptr() = *(--result.vm->L->top));
RTTI_TYPE_CONVERSION(Lua::StackEntry, std::string,
	lua_pushlstring(result.vm->L, value.data(), value.length()); *result.ptr() = *(--result.vm->L->top));


/* Object push/pop */

/*
void * Lua::allocRttiObject(VM & vm, const RTTI::MetaObject * metaObject)
{
	lua_State * const L = vm.L;

	size_t size = metaObject->type()->size;
	void * ptr = lua_newuserdata(L, size);
	memset(ptr, 0, size);

	lua_Table * metatable = metatableForClass(vm, metaObject);
	vm.pushTable(metatable);
	lua_setmetatable(L, -2);

	return ptr;
}
*/

RTTI::Object * Lua::toRttiObject(VM & vm, const lua_TValue * value, const RTTI::MetaObject * metaObject)
{
	lua_State * const L = vm.L;
	RTTI::Object * object;
	lua_Table * metatable;
	lua_UserPointer * up;

	switch (lua_ttypenv(value))
	{
	case LUA_TUSERPOINTER:
		metatable = lua_uptrvalue(value)->metatable;
		up = reinterpret_cast<lua_UserPointer *>(lua_rawuptrvalue(value) + 1);
		object = reinterpret_cast<RTTI::Object *>(up->ptr);
		break;

/*
	case LUA_TUSERDATA:
		metatable = lua_uvalue(value)->metatable;
		object = reinterpret_cast<RTTI::Object *>(lua_rawuvalue(value) + 1);
		break;
*/

	default:
		return NULL;
	}

	vm.rawGetP(metatable, &Lua::IDX_META_OBJECT);
	const MetaObject * objectMetaObject = reinterpret_cast<MetaObject *>(vm.toUserData(L->top - 1));
	--L->top;

	if (!objectMetaObject)
		return NULL;

	if (metaObject && !objectMetaObject->metaInstanceOf(metaObject))
		return NULL;

	return object;
}

void Lua::pushRttiObjectRawPtr(VM & vm, RTTI::Object * object)
{
	lua_State * L = vm.L;

	if (!object)
		lua_pushnil(L);
	else
	{
		lua_pushuserpointer(L, object, 1);
		vm.pushTable(metatableForClass(vm, object->metaObject()));
		lua_setmetatable(L, -2);
	}
}

void Lua::pushRttiObjectStrongPtr(VM & vm, const RTTI::ObjectPtr & object)
{
	lua_State * L = vm.L;

	if (!object)
		lua_pushnil(L);
	else
	{
		lua_pushuserpointer(L, object.get(), 0);
		vm.pushTable(metatableForClass(vm, object->metaObject()));
		lua_setmetatable(L, -2);
		object->retain();
	}
}

bool Lua::setRttiObjectToNil(VM & vm, const lua_TValue * value)
{
	if (lua_ttypenv(value) != LUA_TUSERPOINTER)
		return false;

	(void)vm;
	lua_UserPointer * uptr = reinterpret_cast<lua_UserPointer *>(lua_rawuptrvalue(value) + 1);
	if (uptr->ptr)
	{
		if (!uptr->weak)
			reinterpret_cast<RTTI::Object *>(uptr->ptr)->release();
		uptr->ptr = NULL;
	}

	return true;
}


/* Properties */

int Lua::propertyAccessor(VM & vm, RTTI::Object * object, const RTTI::Property * property)
{
	switch (lua_gettop(vm.L))
	{
	case 1: {
		lua_setnilvalue(vm.L->top);
		++vm.L->top;
		Lua::StackEntry entry(vm, vm.L->top - 1);
		property->getValue(object, entry);
		return 1;
		}
	case 2:
		property->setValue(object, Lua::StackEntry(vm, 2));
		return 0;
	}

	return luaL_error(vm.L, "invalid number of arguments for the accessor of property '%s'.",
		property->name().c_str());
}

static int propertyAccessor(lua_State * L)
{
	Lua::VM & vm = Lua::VM::fromL(L);

	const Property * property = reinterpret_cast<Property *>(lua_touserdata(L, lua_upvalueindex(1)));
	RTTI::Object * object = toRttiObject(vm, 1, property->metaObject());
	if (!object)
		return luaL_error(L, "attempted to access property '%s' of invalid object.", property->name().c_str());

	return propertyAccessor(vm, object, property);
}

void Lua::pushAccessorForProperty(VM & vm, const RTTI::Property * property)
{
	lua_State * const L = vm.L;
	lua_pushlightuserdata(L, const_cast<RTTI::Property *>(property));
	lua_pushcclosure(L, ::propertyAccessor, 1);
}


/* Method */

int Lua::methodCaller(VM & vm, RTTI::Object * object, const RTTI::Method * method)
{
	RTTI::Type voidType = RTTI::typeOf<void>();

	lua_setnilvalue(vm.L->top);
	++vm.L->top;
	Lua::StackEntry result(vm, vm.L->top - 1);

	RTTI::Reference resultRef(&result, method->returnType() == voidType ? voidType : RTTI::typeOf(result));

	size_t n = static_cast<size_t>(lua_gettop(vm.L)) - 2;
	if (n == 0)
		method->invokeDynamic(object, resultRef);
	else
	{
		RTTI::Type stackEntryType = RTTI::typeOf<Lua::StackEntry>();
		lua_CallInfo * ci = vm.L->ci;

		Lua::StackEntry * entries = reinterpret_cast<Lua::StackEntry *>(alloca(sizeof(Lua::StackEntry) * n));
		ConstReference * args = reinterpret_cast<ConstReference *>(alloca(sizeof(ConstReference) * n));
		for (size_t i = 0; i < n; i++)
		{
			new (&entries[i]) Lua::StackEntry(vm, ci->func + i + 2);
			new (&args[i]) ConstReference(&entries[i], stackEntryType);
		}

		method->invokeDynamic(object, resultRef, n, args);
	}

	return 1;
}

static int methodCaller(lua_State * L)
{
	Lua::VM & vm = Lua::VM::fromL(L);

	const Method * method = reinterpret_cast<Method *>(lua_touserdata(L, lua_upvalueindex(1)));
	RTTI::Object * object = toRttiObject(vm, 1, method->metaObject());
	if (!object)
		return luaL_error(L, "attempted to invoke method '%s' on invalid object.", method->name().c_str());

	return methodCaller(vm, object, method);
}

void Lua::pushMethod(VM & vm, const RTTI::Method * method)
{
	lua_State * const L = vm.L;
	lua_pushlightuserdata(L, const_cast<RTTI::Method *>(method));
	lua_pushcclosure(L, ::methodCaller, 1);
}


/* Meta tables */

static int destructor(lua_State * L)
{
	Lua::VM & vm = Lua::VM::fromL(L);
	lua_Table * metatable;

	const lua_TValue * value = (lua_gettop(L) > 0 ? L->ci->func + 1 : luaO_nilobject);
	if (lua_ttisuserpointer(value))
	{
		metatable = lua_uptrvalue(value)->metatable;
		vm.rawGetP(metatable, &Lua::IDX_META_OBJECT);
		if (vm.toUserData(--L->top))
		{
			lua_UserPointer * uptr = reinterpret_cast<lua_UserPointer *>(lua_rawuptrvalue(value) + 1);
			if (uptr->ptr && !uptr->weak)
				reinterpret_cast<RTTI::Object *>(uptr->ptr)->release();
		}
	}
/*
	else if (lua_ttisuserdata(value))
	{
		metatable = lua_uvalue(value)->metatable;
		vm.rawGetP(metatable, &Lua::IDX_META_OBJECT);
		if (vm.toUserData(--L->top))
			reinterpret_cast<RTTI::Object *>(lua_rawuvalue(value) + 1)->~Object();
	}
*/

	return 0;
}

static int index(lua_State * L)
{
	Lua::VM & vm = Lua::VM::fromL(L);

	lua_Table * properties = lua_hvalue(lua_index2addr(L, lua_upvalueindex(2)));
	lua_Table * methods = lua_hvalue(lua_index2addr(L, lua_upvalueindex(3)));

	// Properties
	lua_pushvalue(L, 2);
	vm.rawGet(properties);
	if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
	{
		lua_pushvalue(L, 1);
		lua_call(L, 1, 1);
		return 1;
	}
	--L->top;

	// Methods
	lua_pushvalue(L, 2);
	vm.rawGet(methods);
	if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
		return 1;
	--L->top;

	// Properties, now through meta methods
	if (properties->metatable)
	{
		lua_pushvalue(L, 2);
		vm.tableGet(properties);
		if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
		{
			lua_pushvalue(L, 1);
			lua_call(L, 1, 1);
			return 1;
		}
		--L->top;
	}

	// Methods, now through meta methods
	if (methods->metatable)
	{
		lua_pushvalue(L, 2);
		vm.tableGet(methods);
		if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
			return 1;
		--L->top;
	}

	const MetaObject * metaObject = reinterpret_cast<MetaObject *>(lua_touserdata(L, lua_upvalueindex(1)));
	const char * name = luaL_tolstring(L, 2, NULL);
	return luaL_error(L, "'%s' is not a member of class '%s'.", name, metaObject->name().c_str());
}

static int newindex(lua_State * L)
{
	Lua::VM & vm = Lua::VM::fromL(L);

	lua_Table * properties = lua_hvalue(lua_index2addr(L, lua_upvalueindex(2)));
	lua_Table * methods = lua_hvalue(lua_index2addr(L, lua_upvalueindex(3)));

	// Properties
	lua_pushvalue(L, 2);
	vm.rawGet(properties);
	if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
	{
		lua_pushvalue(L, 1);
		lua_pushvalue(L, 3);
		lua_call(L, 2, 0);
		return 0;
	}
	--L->top;

	// Methods
	lua_pushvalue(L, 2);
	vm.rawGet(methods);
	if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
	{
		const MetaObject * metaObject = reinterpret_cast<MetaObject *>(lua_touserdata(L, lua_upvalueindex(1)));
		const char * name = luaL_tolstring(L, 2, NULL);
		return luaL_error(L, "method '%s' of class '%s' cannot be overriden.", name, metaObject->name().c_str());
	}
	--L->top;

	// Properties, now through meta methods
	if (properties->metatable)
	{
		lua_pushvalue(L, 2);
		vm.tableGet(properties);
		if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
		{
			lua_pushvalue(L, 1);
			lua_pushvalue(L, 3);
			lua_call(L, 2, 0);
			return 0;
		}
		--L->top;
	}

	// Methods, now through meta methods
	if (methods->metatable)
	{
		lua_pushvalue(L, 2);
		vm.tableGet(methods);
		if (lua_ttislcf(L->top - 1) || (lua_ttisCclosure(L->top - 1)))
		{
			const MetaObject * metaObject = reinterpret_cast<MetaObject *>(lua_touserdata(L, lua_upvalueindex(1)));
			const char * name = luaL_tolstring(L, 2, NULL);
			return luaL_error(L, "method '%s' of class '%s' cannot be overriden.", name, metaObject->name().c_str());
		}
		--L->top;
	}

	const MetaObject * metaObject = reinterpret_cast<MetaObject *>(lua_touserdata(L, lua_upvalueindex(1)));
	const char * name = luaL_tolstring(L, 2, NULL);
	return luaL_error(L, "'%s' is not a member of class '%s'.", name, metaObject->name().c_str());
}

void Lua::initMetatableForClass(Lua::VM & vm, lua_Table * metatable, const RTTI::MetaObject * metaObject)
{
	lua_State * const L = vm.L;

	lua_pushlightuserdata(L, const_cast<RTTI::MetaObject *>(metaObject));
	vm.rawSetP(metatable, &Lua::IDX_META_OBJECT);

	lua_Table * properties = vm.pushNewTable();
	vm.rawSetP(metatable, &Lua::IDX_PROPERTIES_TABLE);

	lua_Table * methods = vm.pushNewTable();
	vm.rawSetP(metatable, &Lua::IDX_METHODS_TABLE);

	lua_pushliteral(L, "__metatable");
	lua_pushnil(L);
	vm.rawSet(metatable);

	lua_pushliteral(L, "__gc");
	lua_pushlightuserdata(L, const_cast<RTTI::MetaObject *>(metaObject));
	lua_pushcclosure(L, destructor, 1);
	vm.rawSet(metatable);

	lua_pushliteral(L, "__index");
	lua_pushlightuserdata(L, const_cast<RTTI::MetaObject *>(metaObject));
	vm.pushTable(properties);
	vm.pushTable(methods);
	lua_pushcclosure(L, index, 3);
	vm.rawSet(metatable);

	lua_pushliteral(L, "__newindex");
	lua_pushlightuserdata(L, const_cast<RTTI::MetaObject *>(metaObject));
	vm.pushTable(properties);
	vm.pushTable(methods);
	lua_pushcclosure(L, newindex, 3);
	vm.rawSet(metatable);

	size_t numProperties = metaObject->numProperties();
	for (size_t i = 0; i < numProperties; i++)
	{
		const RTTI::Property * property = metaObject->property(i);
		lua_pushlstring(L, property->name().c_str(), property->name().length());
		pushAccessorForProperty(vm, property);
		vm.rawSet(properties);
	}

	size_t numMethods = metaObject->numMethods();
	for (size_t i = 0; i < numMethods; i++)
	{
		const RTTI::Method * method = metaObject->method(i);
		lua_pushlstring(L, method->name().c_str(), method->name().length());
		pushMethod(vm, method);
		vm.rawSet(methods);
	}
}

lua_Table * Lua::metatableForClass(VM & vm, const RTTI::MetaObject * metaObject)
{
	bool isNew = false;
	lua_Table * metatable = vm.persistentTableForKey(metaObject, &isNew);
	if (isNew)
		initMetatableForClass(vm, metatable, metaObject);
	return metatable;
}


/* Constructors */

static int constructor(lua_State * L)
{
	const Constructor * constructor = reinterpret_cast<Constructor *>(lua_touserdata(L, lua_upvalueindex(1)));
	size_t n = static_cast<size_t>(lua_gettop(L));
	Lua::VM & vm = Lua::VM::fromL(L);
	RTTI::Object * object;

	if (n == 0)
		object = constructor->invokeDynamic();
	else
	{
		RTTI::Type stackEntryType = RTTI::typeOf<Lua::StackEntry>();
		lua_CallInfo * ci = L->ci;

		Lua::StackEntry * entries = reinterpret_cast<Lua::StackEntry *>(alloca(sizeof(Lua::StackEntry) * n));
		ConstReference * args = reinterpret_cast<ConstReference *>(alloca(sizeof(ConstReference) * n));
		for (size_t i = 0; i < n; i++)
		{
			new (&entries[i]) Lua::StackEntry(vm, ci->func + i + 1);
			new (&args[i]) ConstReference(&entries[i], stackEntryType);
		}

		object = constructor->invokeDynamic(n, args);
	}

	Lua::pushRttiObjectStrongPtr(vm, object);

	lua_Table * metatable = metatableForClass(vm, constructor->metaObject());
	vm.pushTable(metatable);
	lua_setmetatable(L, -2);

	return 1;
}

void Lua::pushConstructor(VM & vm, const RTTI::Constructor * constructorPtr)
{
	lua_State * const L = vm.L;
	lua_pushlightuserdata(L, const_cast<RTTI::Constructor *>(constructorPtr));
	lua_pushcclosure(L, constructor, 1);
}

lua_Table * Lua::pushEnum(VM & vm, const RTTI::Enum * enumPtr)
{
	lua_Table * table = vm.pushNewTable();
	for (size_t i = 0; i < enumPtr->numConstants(); i++)
	{
		const RTTI::Enum::Constant & constant = enumPtr->constant(i);
		lua_pushlstring(vm.L, constant.name().c_str(), constant.name().length());
		lua_pushinteger(vm.L, static_cast<lua_Integer>(constant.value()));
		vm.rawSet(table);
	}
	return table;
}

void Lua::addAllEnumsIntoTable(VM & vm, lua_Table * table)
{
	const std::vector<const RTTI::Enum *> & enums = RTTI::allEnumerations();
	for (std::vector<const RTTI::Enum *>::const_iterator it = enums.begin(); it != enums.end(); ++it)
	{
		lua_pushlstring(vm.L, (*it)->name().c_str(), (*it)->name().length());
		pushEnum(vm, *it);
		vm.rawSet(table);
	}
}
