/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __69a7a8e1d6320db5d5f5fe6b4682fe4f__
#define __69a7a8e1d6320db5d5f5fe6b4682fe4f__

#include "lua_vm.hpp"
#include <string>
#include <utility>

#ifdef ASSERT_H
 #include ASSERT_H
 #define LUA_ASSERT(x) ASSERT(x)
#else
 #include <cassert>
 #define LUA_ASSERT(x) assert(x)
#endif

namespace Lua
{
	class StackEntry
	{
	public:
		VM * vm;
		ptrdiff_t index;

		inline StackEntry() : vm(NULL), index(-1) {}
		inline StackEntry(const StackEntry & entry) : vm(entry.vm), index(entry.index) {}
		inline StackEntry(VM & v, lua_TValue * o) : vm(&v), index(lua_savestack(vm->L, o)) {}
		inline StackEntry(VM * v, lua_TValue * o) : vm(v), index(lua_savestack(vm->L, o)) {}
		inline StackEntry(VM & v, int idx) : vm(&v), index(lua_savestack(vm->L, lua_index2addr(vm->L, idx))) {}
		inline StackEntry(VM * v, int idx) : vm(v), index(lua_savestack(vm->L, lua_index2addr(vm->L, idx))) {}
		inline ~StackEntry() {}

		StackEntry & operator=(const StackEntry & entry);

		inline lua_TValue * ptr() const { LUA_ASSERT(vm && index >= 0); return lua_restorestack(vm->L, index); }

		inline bool toBoolean() const { return vm->toBoolean(ptr()); }
		inline lua_Integer toInteger() const { return vm->toInteger(ptr()); }
		inline lua_Unsigned toUnsigned() const { return vm->toUnsigned(ptr()); }
		inline lua_Number toNumber() const { return vm->toNumber(ptr()); }
		inline void * toUserData() const { return vm->toUserData(ptr()); }
		inline std::pair<float, float> toVec2() const { return vm->toVec2(ptr()); }
		inline lua_Table * toTable() const { return (lua_ttistable(ptr()) ? lua_hvalue(ptr()) : NULL); }
		std::string toStdString() const { return vm->toStdString(ptr()); }

		inline void setNil() { lua_setnilvalue(ptr()); }
		inline void setBoolean(bool v) { lua_setbvalue(ptr(), (v ? 1 : 0)); }
		inline void setInteger(lua_Integer v) { lua_setnvalue(ptr(), lua_cast_num(v)); }
		inline void setUnsigned(lua_Unsigned v) { lua_setnvalue(ptr(), lua_unsigned2number(v)); }
		inline void setNumber(lua_Number v) { lua_setnvalue(ptr(), v); }
		inline void setLightUserData(const void * v) { lua_setpvalue(ptr(), const_cast<void *>(v)); }
		inline void setCFunction(lua_CFunction v) { lua_setfvalue(ptr(), v); }
		inline void setVec2(float x, float y) { lua_setvec2value(ptr(), x, y); }
	};
}

#endif
