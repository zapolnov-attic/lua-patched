/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "file.hpp"
#include "lua.hpp"
#include <sstream>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <cstdio>

namespace
{
	class File : public Lua::File
	{
	public:
		File(const char * name);
		~File();

		bool reopenInBinaryMode();

		bool atEnd();
		size_t read(void * buffer, size_t bytes);

	private:
		FILE * m_Handle;
		bool m_Close;
		std::string m_Name;
	};

	File::File(const char * name)
	{
		if (!name)
		{
			m_Name = "stdin";
			m_Handle = stdin;
			m_Close = false;
			return;
		}

		m_Name = name;
		m_Close = true;

		m_Handle = fopen(name, "r");
		if (!m_Handle)
		{
			int err = errno;
			std::stringstream ss;
			ss << "unable to open file \"" << name << "\": " << strerror(err);
			throw std::runtime_error(ss.str());
		}
	}

	File::~File()
	{
		if (m_Close && m_Handle)
			fclose(m_Handle);
	}

	bool File::reopenInBinaryMode()
	{
		m_Handle = freopen(m_Name.c_str(), "rb", m_Handle);
		if (!m_Handle)
		{
			int err = errno;
			std::stringstream ss;
			ss << "unable to reopen file \"" << m_Name << "\" in binary mode: " << strerror(err);
			throw std::runtime_error(ss.str());
		}
		return true;
	}

	bool File::atEnd()
	{
		return feof(m_Handle);
	}

	size_t File::read(void * buffer, size_t bytes)
	{
		bytes = fread(buffer, 1, bytes, m_Handle);
		if (ferror(m_Handle))
		{
			int err = errno;
			std::stringstream ss;
			ss << "unable to read from file \"" << m_Name << "\": " << strerror(err);
			throw std::runtime_error(ss.str());
		}
		return bytes;
	}
}

static Lua::File * lua_fopen(const char * filename)
{
	return new File(filename);
}

Lua::File * (* luai_fopen)(const char * filename) = lua_fopen;
