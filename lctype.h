/*
** $Id: lctype.h,v 1.12.1.1 2013/04/12 18:48:47 roberto Exp $
** 'ctype' functions for Lua
** See Copyright Notice in lua.h
*/

#ifndef lctype_h
#define lctype_h

#include "lua.h"


/*
** WARNING: the functions defined here do not necessarily correspond
** to the similar functions in the standard C ctype.h. They are
** optimized for the specific needs of Lua
*/

#if !defined(LUA_USE_CTYPE)

#if 'A' == 65 && '0' == 48
/* ASCII case: can use its own tables; faster and fixed */
#define LUA_USE_CTYPE	0
#else
/* must use standard C ctype */
#define LUA_USE_CTYPE	1
#endif

#endif


#if !LUA_USE_CTYPE	/* { */

#include <limits.h>

#include "llimits.h"


#define LUA_ALPHABIT	0
#define LUA_DIGITBIT	1
#define LUA_PRINTBIT	2
#define LUA_SPACEBIT	3
#define LUA_XDIGITBIT	4


#define LUA_MASK(B)		(1 << (B))


/*
** add 1 to char to allow index -1 (LUA_EOZ)
*/
#define lua_testprop(c,p)	(luai_ctype_[(c)+1] & (p))

/*
** 'lalpha' (Lua alphabetic) and 'lalnum' (Lua alphanumeric) both include '_'
*/
#define lislalpha(c)	lua_testprop(c, LUA_MASK(LUA_ALPHABIT))
#define lislalnum(c)	lua_testprop(c, (LUA_MASK(LUA_ALPHABIT) | LUA_MASK(LUA_DIGITBIT)))
#define lisdigit(c)	lua_testprop(c, LUA_MASK(LUA_DIGITBIT))
#define lisspace(c)	lua_testprop(c, LUA_MASK(LUA_SPACEBIT))
#define lisprint(c)	lua_testprop(c, LUA_MASK(LUA_PRINTBIT))
#define lisxdigit(c)	lua_testprop(c, LUA_MASK(LUA_XDIGITBIT))

/*
** this 'ltolower' only works for alphabetic characters
*/
#define ltolower(c)	((c) | ('A' ^ 'a'))


/* two more entries for 0 and -1 (LUA_EOZ) */
LUAI_DDEC const lua_ubyte luai_ctype_[UCHAR_MAX + 2];


#else			/* }{ */

/*
** use standard C ctypes
*/

#include <ctype.h>


#define lislalpha(c)	(isalpha(c) || (c) == '_')
#define lislalnum(c)	(isalnum(c) || (c) == '_')
#define lisdigit(c)	(isdigit(c))
#define lisspace(c)	(isspace(c))
#define lisprint(c)	(isprint(c))
#define lisxdigit(c)	(isxdigit(c))

#define ltolower(c)	(tolower(c))

#endif			/* } */

#endif

