/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define LUA_CORE
#define LUA_LIB

#define lapi_c
#define lauxlib_c
#define lbaselib_c
#define lbitlib_c
#define lcode_c
#define lcorolib_c
#define lctype_c
#define ldblib_c
#define ldebug_c
#define ldo_c
#define ldump_c
#define lfunc_c
#define lgc_c
#define linit_c
#define llex_c
#define lmathlib_c
#define lmem_c
#define lobject_c
#define lopcodes_c
#define lparser_c
#define lstate_c
#define lstring_c
#define lstrlib_c
#define ltable_c
#define ltablib_c
#define ltm_c
#define lundump_c
#define lvm_c
#define lzio_c

#include "lua.hpp"

#include "lua_vm.cpp"
#ifdef RTTI_OBJECT_H
#include "lua_meta_object.cpp"
#include "lua_rtti.cpp"
#endif
#include "file.cpp"
#include "lapi.c"
#include "lauxlib.cpp"
#include "lbaselib.c"
#include "lbitlib.c"
#include "lcode.c"
#include "lcorolib.c"
#include "lctype.c"
#include "ldblib.c"
#include "ldebug.c"
#include "ldo.cpp"
#include "ldump.c"
#include "lexer.cpp"
#include "lfunc.c"
#include "lgc.c"
#include "linit.c"
#include "llex.c"
#include "lmathlib.c"
#include "lmem.c"
#include "lobject.c"
#include "lopcodes.c"
#include "lparser.c"
#include "lstate.c"
#include "lstring.c"
#include "lstrlib.c"
#include "ltable.c"
#include "ltablib.c"
#include "ltm.c"
#include "lua_value.cpp"
#include "luaconf.c"
#include "lundump.c"
#include "lvm.c"
#include "lzio.c"
