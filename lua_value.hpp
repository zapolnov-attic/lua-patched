/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __a2f7ff6841328f4fbfce152659259680__
#define __a2f7ff6841328f4fbfce152659259680__

#include "lua_vm.hpp"

namespace Lua
{
	class Value : public lua_TValue
	{
	public:
		inline Value() { lua_setnilvalue(this); }
		inline Value(const lua_TValue * v) { *static_cast<lua_TValue *>(this) = *v; acquire(); }
		inline Value(bool v) { lua_setbvalue(this, (v ? 1 : 0)); }
		inline Value(char v) { lua_setnvalue(this, lua_cast_num(static_cast<lua_Integer>(v))); }
		inline Value(signed char v) { lua_setnvalue(this, lua_cast_num(static_cast<lua_Integer>(v))); }
		inline Value(unsigned char v)
			{ lua_Unsigned V = static_cast<lua_Unsigned>(v); lua_setnvalue(this, lua_unsigned2number(V)); }
		inline Value(short v) { lua_setnvalue(this, lua_cast_num(static_cast<lua_Integer>(v))); }
		inline Value(unsigned short v)
			{ lua_Unsigned V = static_cast<lua_Unsigned>(v); lua_setnvalue(this, lua_unsigned2number(V)); }
		inline Value(int v) { lua_setnvalue(this, lua_cast_num(static_cast<lua_Integer>(v))); }
		inline Value(unsigned int v) { lua_setnvalue(this, lua_unsigned2number(static_cast<lua_Unsigned>(v))); }
		inline Value(long v) { lua_setnvalue(this, static_cast<lua_Number>(v)); }
		inline Value(unsigned long v) { lua_setnvalue(this, static_cast<lua_Number>(v)); }
		inline Value(long long v) { lua_setnvalue(this, static_cast<lua_Number>(v)); }
		inline Value(unsigned long long v) { lua_setnvalue(this, static_cast<lua_Number>(v)); }
		inline Value(float v) { lua_setnvalue(this, static_cast<lua_Number>(v)); }
		inline Value(double v) { lua_setnvalue(this, static_cast<lua_Number>(v)); }
		inline Value(long double v) { lua_setnvalue(this, static_cast<lua_Number>(v)); }
		inline Value(const void * v) { lua_setpvalue(this, const_cast<void *>(v)); }
		inline Value(lua_CFunction v) { lua_setfvalue(this, v); }
		inline Value(float x, float y) { lua_setvec2value(this, x, y); }
		inline Value(Lua::VM & vm, lua_Table * table) { (void)vm; lua_sethvalue(vm.L, this, table); acquire(); }
		Value(const Value & src);
		inline ~Value() { release(); }

		Value & operator=(const Value & src);

		inline int type() const { return lua_ttypenv(this); }

	private:
		void acquire() const;
		void release();
	};
}

#endif
