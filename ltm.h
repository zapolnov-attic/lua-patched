/*
** $Id: ltm.h,v 2.11.1.1 2013/04/12 18:48:47 roberto Exp $
** Tag methods
** See Copyright Notice in lua.h
*/

#ifndef ltm_h
#define ltm_h


#include "lobject.h"


/*
* WARNING: if you change the order of this enumeration,
* grep "ORDER TM"
*/
typedef enum {
  LUA_TM_INDEX,
  LUA_TM_NEWINDEX,
  LUA_TM_GC,
  LUA_TM_MODE,
  LUA_TM_LEN,
  LUA_TM_EQ,  /* last tag method with `fast' access */
  LUA_TM_ADD,
  LUA_TM_SUB,
  LUA_TM_MUL,
  LUA_TM_DIV,
  LUA_TM_MOD,
  LUA_TM_POW,
  LUA_TM_UNM,
  LUA_TM_LT,
  LUA_TM_LE,
  LUA_TM_CONCAT,
  LUA_TM_CALL,
  LUA_TM_N		/* number of elements in the enum */
} lua_TMS;



#define lua_gfasttm(g,et,e) ((et) == NULL ? NULL : \
  ((et)->flags & (1u<<(e))) ? NULL : luaT_gettm(et, e, (g)->tmname[e]))

#define lua_fasttm(l,et,e)	lua_gfasttm(LUA_G(l), et, e)

#define lua_ttypename(x)	luaT_typenames_[(x) + 1]
#define lua_objtypename(x)	lua_ttypename(lua_ttypenv(x))

LUAI_DDEC const char *const luaT_typenames_[LUA_TOTALTAGS];


LUAI_FUNC const lua_TValue *luaT_gettm (lua_Table *events, lua_TMS event, lua_TString *ename);
LUAI_FUNC const lua_TValue *luaT_gettmbyobj (lua_State *L, const lua_TValue *o,
                                                       lua_TMS event);
LUAI_FUNC void luaT_init (lua_State *L);

#endif
