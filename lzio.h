/*
** $Id: lzio.h,v 1.26.1.1 2013/04/12 18:48:47 roberto Exp $
** Buffered streams
** See Copyright Notice in lua.h
*/


#ifndef lzio_h
#define lzio_h

#include "lua.h"

#include "lmem.h"


#define LUA_EOZ	(-1)			/* end of stream */

typedef struct lua_Zio lua_ZIO;

#define lua_zgetc(z)  (((z)->n--)>0 ?  lua_cast_uchar(*(z)->p++) : luaZ_fill(z))


typedef struct lua_Mbuffer {
  char *buffer;
  size_t n;
  size_t buffsize;
} lua_Mbuffer;

#define luaZ_initbuffer(L, buff) ((buff)->buffer = NULL, (buff)->buffsize = 0)

#define luaZ_buffer(buff)	((buff)->buffer)
#define luaZ_sizebuffer(buff)	((buff)->buffsize)
#define luaZ_bufflen(buff)	((buff)->n)

#define luaZ_resetbuffer(buff) ((buff)->n = 0)


#define luaZ_resizebuffer(L, buff, size) \
	(luaM_reallocvector(L, (buff)->buffer, (buff)->buffsize, size, char), \
	(buff)->buffsize = size)

#define luaZ_freebuffer(L, buff)	luaZ_resizebuffer(L, buff, 0)


LUAI_FUNC char *luaZ_openspace (lua_State *L, lua_Mbuffer *buff, size_t n);
LUAI_FUNC void luaZ_init (lua_State *L, lua_ZIO *z, lua_Reader reader,
                                        void *data);
LUAI_FUNC size_t luaZ_read (lua_ZIO* z, void* b, size_t n);	/* read next n bytes */



/* --------- Private Part ------------------ */

struct lua_Zio {
  size_t n;			/* bytes still unread */
  const char *p;		/* current position in buffer */
  lua_Reader reader;		/* reader function */
  void* data;			/* additional data */
  lua_State *L;			/* Lua state (for reader) */
};


LUAI_FUNC int luaZ_fill (lua_ZIO *z);

#endif
