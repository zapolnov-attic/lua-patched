/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "lua_vm.hpp"
#include "lua_value.hpp"
#include "lua_stack_entry.hpp"
#include "lua_stack_top.hpp"
#include <cstring>
#include <sstream>
#include <stdexcept>

Lua::VM::VM()
	: L(luaL_newstate())
{
	if (!L)
		throw std::bad_alloc();

	try
	{
		LUA_G(L)->ud = this;
		luaL_openlibs(L);
	}
	catch (...)
	{
		lua_close(L);
		throw;
	}
}

Lua::VM::~VM()
{
	lua_close(L);
}

void Lua::VM::doString(const char * str, const char * name)
{
	StackTop top(*this);

	if (!name)
		name = "";

	int status = luaL_loadbuffer(L, str, strlen(str), name);
	if (status != 0)
	{
		std::stringstream ss;
		ss << "syntax error in '" << name << "': ";
		if (top < lua_gettop(L))
			ss << lua_tostring(L, -1);
		else
			ss << status;
		throw std::runtime_error(ss.str());
	}

	pcall();
}

bool Lua::VM::doString(const char * str, const std::nothrow_t &)
{
	try {
		doString(str);
		return true;
	} catch (const std::exception & e) {
		luai_writestringerror(L, e.what(), NULL);
		return false;
	}
}

bool Lua::VM::doString(const char * str, const char * name, const std::nothrow_t &)
{
	try {
		doString(str, name);
		return true;
	} catch (const std::exception & e) {
		luai_writestringerror(L, e.what(), NULL);
		return false;
	}
}

void Lua::VM::doFile(const char * name)
{
	StackTop top(*this);

	int status = luaL_loadfile(L, name);
	if (status != 0)
	{
		std::stringstream ss;
		ss << "syntax error in '" << name << "': ";
		if (top < lua_gettop(L))
			ss << lua_tostring(L, -1);
		else
			ss << status;
		throw std::runtime_error(ss.str());
	}

	call();
}

bool Lua::VM::doFile(const char * name, const std::nothrow_t &)
{
	try {
		doFile(name);
		return true;
	} catch (const std::exception & e) {
		luai_writestringerror(L, e.what(), NULL);
		return false;
	}
}

std::string Lua::VM::stackTrace(const char * msg, int level)
{
	StackTop top(*this);
	luaL_traceback(L, L, msg, level);
	size_t length = 0;
	const char * traceback = lua_tolstring(L, -1, &length);
	return std::string(traceback, length);
}

void Lua::VM::call(int nargs, int nresults)
{
	if (!pcall(nargs, nresults))
		throw std::runtime_error("lua code execution failed.");
}

bool Lua::VM::pcall(int nargs, int nresults)
{
	StackTop top(*this);

	lua_setfvalue(L->top, errorHandler);
	lua_api_incr_top(L);

	int errorHandlerIdx = lua_absindex(L, -(nargs + 2));
	lua_insert(L, errorHandlerIdx);

	int status = lua_pcall(L, nargs, nresults, -(nargs + 2));
	if (status == 0)
	{
		lua_remove(L, errorHandlerIdx);
		top = lua_gettop(L);
		return true;
	}
	else
	{
		reportCallError(status);
		return false;
	}
}

lua_Integer Lua::VM::toInteger(const lua_TValue * o) const
{
	lua_TValue n;
	if (luai_tonumber(o, &n))
	{
		lua_Integer res;
		lua_Number num = lua_nvalue(o);
		lua_number2integer(res, num);
		return res;
	}
	return 0;
}

lua_Unsigned Lua::VM::toUnsigned(const lua_TValue * o) const
{
	lua_TValue n;
	if (luai_tonumber(o, &n))
	{
		lua_Unsigned res;
		lua_Number num = lua_nvalue(o);
		lua_number2unsigned(res, num);
		return res;
	}
	return 0;
}

lua_Number Lua::VM::toNumber(const lua_TValue * o) const
{
	lua_TValue n;
	if (luai_tonumber(o, &n))
		return lua_nvalue(o);
	return 0;
}

void * Lua::VM::toUserData(const lua_TValue * o) const
{
	switch (lua_ttype(o))
	{
	case LUA_TUSERDATA: return (lua_rawuvalue(o) + 1);
	case LUA_TLIGHTUSERDATA: return lua_pvalue(o);
	}
	return NULL;
}

std::pair<float, float> Lua::VM::toVec2(const lua_TValue * o) const
{
	if (lua_ttisvec2(o))
	{
		const float * p = lua_vec2value(o);
		return std::pair<float, float>(p[0], p[1]);
	}

	lua_TValue n;
	if (luai_tonumber(o, &n))
	{
		float v = static_cast<float>(lua_nvalue(o));
		return std::pair<float, float>(v, v);
	}

	return std::pair<float, float>(0.0f, 0.0f);
}

std::string Lua::VM::toStdString(const lua_TValue * o) const
{
	if (lua_ttisstring(o))
		return std::string(lua_svalue(o), lua_tsvalue(o)->len);

	if (!lua_ttisnumber(o))
		return std::string();

	char buf[LUAI_MAXNUMBER2STR];
	lua_Number num = lua_nvalue(o);
	int len = lua_number2str(buf, num);

	return std::string(buf, static_cast<size_t>(len));
}

void Lua::VM::push(const Value & value)
{
	lua_setobjs2s(L, L->top, &value);
	lua_api_incr_top(L);
}

Lua::Value Lua::VM::pop()
{
	Lua::Value v(L->top - 1);
	--L->top;
	return v;
}

void Lua::VM::rawSet(lua_Table * table)
{
	lua_checknelems(L, 2);
	lua_setobj2t(L, luaH_set(L, table, L->top - 2), L->top - 1);
	lua_invalidateTMcache(table);
	luaC_barrierback(L, reinterpret_cast<lua_GCObject *>(table), L->top - 1);
	L->top -= 2;
}

void Lua::VM::rawSetI(lua_Table * table, int key)
{
	lua_checknelems(L, 1);
	luaH_setint(L, table, key, L->top - 1);
	luaC_barrierback(L, reinterpret_cast<lua_GCObject *>(table), L->top - 1);
	--L->top;
}

void Lua::VM::rawSetP(lua_Table * table, const void * key)
{
	lua_TValue k;

	lua_checknelems(L, 1);
	lua_setpvalue(&k, lua_cast(void *, key));
	lua_setobj2t(L, luaH_set(L, table, &k), L->top - 1);
	luaC_barrierback(L, reinterpret_cast<lua_GCObject *>(table), L->top - 1);
	--L->top;
}

bool Lua::VM::next(lua_Table * table)
{
	if (!table)
		return false;

	int more = luaH_next(L, table, L->top - 1);
	if (!more)
		--L->top;
	else {
		lua_api_incr_top(L);
	}

	return (more != 0);
}

lua_Table * Lua::VM::persistentTableForKey(const void * key, bool * isNew)
{
	lua_Table * reg = registry();
	lua_TValue k;

	lua_setpvalue(&k, lua_cast(void *, key));
	const lua_TValue * value = luaH_get(reg, &k);
	if (lua_ttistable(value))
	{
		if (isNew)
			*isNew = false;
		return lua_hvalue(value);
	}

	lua_Table * table = luaH_new(L);
	lua_sethvalue(L, L->top, table);
	lua_api_incr_top(L);

	lua_setobj2t(L, luaH_set(L, reg, &k), L->top - 1);
	luaC_barrierback(L, reinterpret_cast<lua_GCObject *>(reg), L->top - 1);
	--L->top;

	if (isNew)
		*isNew = true;

	return table;
}

int Lua::VM::errorHandler(lua_State * L)
{
	const char * msg = (lua_gettop(L) > 0 ? lua_tostring(L, -1) : NULL);
	luai_writestringerror(L, Lua::VM::fromL(L).stackTrace(msg, 1).c_str(), NULL);
	return 1;
}

void Lua::VM::reportCallError(int status)
{
	switch (status)
	{
	case LUA_ERRRUN:
		return;

	case LUA_ERRMEM:
		luai_writestringerror(L, "lua_pcall() failed", "LUA_ERRMEM.");
		return;

	case LUA_ERRERR:
		luai_writestringerror(L, "lua_pcall() failed", "LUA_ERRERR.");
		return;
	}

	std::stringstream ss;
	ss << status;
	luai_writestringerror(L, "lua_pcall() failed", ss.str().c_str());
}
