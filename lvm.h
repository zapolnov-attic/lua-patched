/*
** $Id: lvm.h,v 2.18.1.1 2013/04/12 18:48:47 roberto Exp $
** Lua virtual machine
** See Copyright Notice in lua.h
*/

#ifndef lvm_h
#define lvm_h


#include "ldo.h"
#include "lobject.h"
#include "ltm.h"


#define luai_tostring(L,o) (lua_ttisstring(o) || (luaV_tostring(L, o)))

#define luai_tonumber(o,n)	(lua_ttisnumber(o) || (((o) = luaV_tonumber(o,n)) != NULL))

#define lua_equalobj(L,o1,o2)  (lua_ttisequal(o1, o2) && luaV_equalobj_(L, o1, o2))

#define luaV_rawequalobj(o1,o2)		lua_equalobj(NULL,o1,o2)


/* not to called directly */
LUAI_FUNC int luaV_equalobj_ (lua_State *L, const lua_TValue *t1, const lua_TValue *t2);


LUAI_FUNC int luaV_lessthan (lua_State *L, const lua_TValue *l, const lua_TValue *r);
LUAI_FUNC int luaV_lessequal (lua_State *L, const lua_TValue *l, const lua_TValue *r);
LUAI_FUNC const lua_TValue *luaV_tonumber (const lua_TValue *obj, lua_TValue *n);
LUAI_FUNC const lua_TValue *luaV_tovec2 (const lua_TValue *obj, lua_TValue *n);
LUAI_FUNC int luaV_tostring (lua_State *L, lua_StkId obj);
LUAI_FUNC void luaV_gettable (lua_State *L, const lua_TValue *t, lua_TValue *key,
                                            lua_StkId val);
LUAI_FUNC void luaV_settable (lua_State *L, const lua_TValue *t, lua_TValue *key,
                                            lua_StkId val);
LUAI_FUNC void luaV_finishOp (lua_State *L);
LUAI_FUNC void luaV_execute (lua_State *L);
LUAI_FUNC void luaV_concat (lua_State *L, int total);
LUAI_FUNC void luaV_arith (lua_State *L, lua_StkId ra, const lua_TValue *rb,
                           const lua_TValue *rc, lua_TMS op);
LUAI_FUNC void luaV_objlen (lua_State *L, lua_StkId ra, const lua_TValue *rb);

#endif
