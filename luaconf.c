/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "luaconf.h"
#include <stdio.h>

static void writestring(struct lua_State * L, const char * str, size_t length)
{
	(void)L;
	fwrite(str, sizeof(char), length, stdout);
	fputc('\n', stdout);
	fflush(stdout);
}

static void writestringerror(struct lua_State * L, const char * msg, const char * extra)
{
	(void)L;
	if (!extra)
		fprintf(stderr, "%s\n", msg);
	else
		fprintf(stderr, "%s: %s\n", msg, extra);
	fflush(stderr);
}

void (* luai_writestring)(struct lua_State * L, const char * str, size_t length) = writestring;
void (* luai_writestringerror)(struct lua_State * L, const char * msg, const char * extra) = writestringerror;
