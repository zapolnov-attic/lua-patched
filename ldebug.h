/*
** $Id: ldebug.h,v 2.7.1.1 2013/04/12 18:48:47 roberto Exp $
** Auxiliary functions from Debug Interface module
** See Copyright Notice in lua.h
*/

#ifndef ldebug_h
#define ldebug_h


#include "lstate.h"


#define lua_pcRel(pc, p)	(lua_cast(int, (pc) - (p)->code) - 1)

#define lua_getfuncline(f,pc)	(((f)->lineinfo) ? (f)->lineinfo[pc] : 0)

#define lua_resethookcount(L)	(L->hookcount = L->basehookcount)

/* Active Lua function (given call info) */
#define lua_ci_func(ci)		(lua_clLvalue((ci)->func))


LUAI_FUNC lua_noret luaG_typeerror (lua_State *L, const lua_TValue *o,
                                                const char *opname);
LUAI_FUNC lua_noret luaG_concaterror (lua_State *L, lua_StkId p1, lua_StkId p2);
LUAI_FUNC lua_noret luaG_aritherror (lua_State *L, const lua_TValue *p1,
                                                 const lua_TValue *p2);
LUAI_FUNC lua_noret luaG_ordererror (lua_State *L, const lua_TValue *p1,
                                                 const lua_TValue *p2);
LUAI_FUNC lua_noret luaG_runerror (lua_State *L, const char *fmt, ...);
LUAI_FUNC lua_noret luaG_errormsg (lua_State *L);

#endif
