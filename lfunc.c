/*
** $Id: lfunc.c,v 2.30.1.1 2013/04/12 18:48:47 roberto Exp $
** Auxiliary functions to manipulate prototypes and closures
** See Copyright Notice in lua.h
*/


#include <stddef.h>

#define lfunc_c
#define LUA_CORE

#include "lua.h"

#include "lfunc.h"
#include "lgc.h"
#include "lmem.h"
#include "lobject.h"
#include "lstate.h"



lua_Closure *luaF_newCclosure (lua_State *L, int n) {
  lua_Closure *c = &luaC_newobj(L, LUA_TCCL, lua_sizeCclosure(n), NULL, 0)->cl;
  c->c.nupvalues = lua_cast_byte(n);
  return c;
}


lua_Closure *luaF_newLclosure (lua_State *L, int n) {
  lua_Closure *c = &luaC_newobj(L, LUA_TLCL, lua_sizeLclosure(n), NULL, 0)->cl;
  c->l.p = NULL;
  c->l.nupvalues = lua_cast_byte(n);
  while (n--) c->l.upvals[n] = NULL;
  return c;
}


lua_UpVal *luaF_newupval (lua_State *L) {
  lua_UpVal *uv = &luaC_newobj(L, LUA_TUPVAL, sizeof(lua_UpVal), NULL, 0)->uv;
  uv->v = &uv->u.value;
  lua_setnilvalue(uv->v);
  return uv;
}


lua_UpVal *luaF_findupval (lua_State *L, lua_StkId level) {
  global_State *g = LUA_G(L);
  lua_GCObject **pp = &L->openupval;
  lua_UpVal *p;
  lua_UpVal *uv;
  while (*pp != NULL && (p = lua_gco2uv(*pp))->v >= level) {
    lua_GCObject *o = lua_obj2gco(p);
    lua_assert(p->v != &p->u.value);
    lua_assert(!lua_isold(o) || lua_isold(lua_obj2gco(L)));
    if (p->v == level) {  /* found a corresponding upvalue? */
      if (lua_isdead(g, o))  /* is it dead? */
        lua_changewhite(o);  /* resurrect it */
      return p;
    }
    pp = &p->next;
  }
  /* not found: create a new one */
  uv = &luaC_newobj(L, LUA_TUPVAL, sizeof(lua_UpVal), pp, 0)->uv;
  uv->v = level;  /* current value lives in the stack */
  uv->u.l.prev = &g->uvhead;  /* double link it in `uvhead' list */
  uv->u.l.next = g->uvhead.u.l.next;
  uv->u.l.next->u.l.prev = uv;
  g->uvhead.u.l.next = uv;
  lua_assert(uv->u.l.next->u.l.prev == uv && uv->u.l.prev->u.l.next == uv);
  return uv;
}


static void unlinkupval (lua_UpVal *uv) {
  lua_assert(uv->u.l.next->u.l.prev == uv && uv->u.l.prev->u.l.next == uv);
  uv->u.l.next->u.l.prev = uv->u.l.prev;  /* remove from `uvhead' list */
  uv->u.l.prev->u.l.next = uv->u.l.next;
}


void luaF_freeupval (lua_State *L, lua_UpVal *uv) {
  if (uv->v != &uv->u.value)  /* is it open? */
    unlinkupval(uv);  /* remove from open list */
  luaM_free(L, uv);  /* free upvalue */
}


void luaF_close (lua_State *L, lua_StkId level) {
  lua_UpVal *uv;
  global_State *g = LUA_G(L);
  while (L->openupval != NULL && (uv = lua_gco2uv(L->openupval))->v >= level) {
    lua_GCObject *o = lua_obj2gco(uv);
    lua_assert(!lua_isblack(o) && uv->v != &uv->u.value);
    L->openupval = uv->next;  /* remove from `open' list */
    if (lua_isdead(g, o))
      luaF_freeupval(L, uv);  /* free upvalue */
    else {
      unlinkupval(uv);  /* remove upvalue from 'uvhead' list */
      lua_setobj(L, &uv->u.value, uv->v);  /* move value to upvalue slot */
      uv->v = &uv->u.value;  /* now current value lives here */
      lua_gch(o)->next = g->allgc;  /* link upvalue into 'allgc' list */
      g->allgc = o;
      luaC_checkupvalcolor(g, uv);
    }
  }
}


lua_Proto *luaF_newproto (lua_State *L) {
  lua_Proto *f = &luaC_newobj(L, LUA_TPROTO, sizeof(lua_Proto), NULL, 0)->p;
  f->k = NULL;
  f->sizek = 0;
  f->p = NULL;
  f->sizep = 0;
  f->code = NULL;
  f->cache = NULL;
  f->sizecode = 0;
  f->lineinfo = NULL;
  f->sizelineinfo = 0;
  f->upvalues = NULL;
  f->sizeupvalues = 0;
  f->numparams = 0;
  f->is_vararg = 0;
  f->maxstacksize = 0;
  f->locvars = NULL;
  f->sizelocvars = 0;
  f->linedefined = 0;
  f->lastlinedefined = 0;
  f->source = NULL;
  return f;
}


void luaF_freeproto (lua_State *L, lua_Proto *f) {
  luaM_freearray(L, f->code, f->sizecode);
  luaM_freearray(L, f->p, f->sizep);
  luaM_freearray(L, f->k, f->sizek);
  luaM_freearray(L, f->lineinfo, f->sizelineinfo);
  luaM_freearray(L, f->locvars, f->sizelocvars);
  luaM_freearray(L, f->upvalues, f->sizeupvalues);
  luaM_free(L, f);
}


/*
** Look for n-th local variable at line `line' in function `func'.
** Returns NULL if not found.
*/
const char *luaF_getlocalname (const lua_Proto *f, int local_number, int pc) {
  int i;
  for (i = 0; i<f->sizelocvars && f->locvars[i].startpc <= pc; i++) {
    if (pc < f->locvars[i].endpc) {  /* is variable active? */
      local_number--;
      if (local_number == 0)
        return lua_getstr(f->locvars[i].varname);
    }
  }
  return NULL;  /* not found */
}

