/*
** $Id: lparser.h,v 1.70.1.1 2013/04/12 18:48:47 roberto Exp $
** Lua Parser
** See Copyright Notice in lua.h
*/

#ifndef lparser_h
#define lparser_h

#include "llimits.h"
#include "lobject.h"
#include "lzio.h"


/*
** Expression descriptor
*/

typedef enum {
  LUA_VVOID,	/* no value */
  LUA_VNIL,
  LUA_VTRUE,
  LUA_VFALSE,
  LUA_VK,		/* info = index of constant in `k' */
  LUA_VKNUM,	/* nval = numerical value */
  LUA_VNONRELOC,	/* info = result register */
  LUA_VLOCAL,	/* info = local register */
  LUA_VUPVAL,       /* info = index of upvalue in 'upvalues' */
  LUA_VINDEXED,	/* t = table register/upvalue; idx = index R/K */
  LUA_VJMP,		/* info = instruction pc */
  LUA_VRELOCABLE,	/* info = instruction pc */
  LUA_VCALL,	/* info = instruction pc */
  LUA_VVARARG	/* info = instruction pc */
} lua_expkind;


#define lua_vkisvar(k)	(LUA_VLOCAL <= (k) && (k) <= LUA_VINDEXED)
#define lua_vkisinreg(k)	((k) == LUA_VNONRELOC || (k) == LUA_VLOCAL)

typedef struct lua_expdesc {
  lua_expkind k;
  union {
    struct {  /* for indexed variables (LUA_VINDEXED) */
      short idx;  /* index (R/K) */
      lua_ubyte t;  /* table (register or upvalue) */
      lua_ubyte vt;  /* whether 't' is register (LUA_VLOCAL) or upvalue (LUA_VUPVAL) */
    } ind;
    int info;  /* for generic use */
    lua_Number nval;  /* for LUA_VKNUM */
  } u;
  int t;  /* patch list of `exit when true' */
  int f;  /* patch list of `exit when false' */
} lua_expdesc;


/* description of active local variable */
typedef struct lua_Vardesc {
  short idx;  /* variable index in stack */
} lua_Vardesc;


/* description of pending goto statements and label statements */
typedef struct lua_Labeldesc {
  lua_TString *name;  /* label identifier */
  int pc;  /* position in code */
  int line;  /* line where it appeared */
  lua_ubyte nactvar;  /* local level where it appears in current block */
} lua_Labeldesc;


/* list of labels or gotos */
typedef struct lua_Labellist {
  lua_Labeldesc *arr;  /* array */
  int n;  /* number of entries in use */
  int size;  /* array size */
} lua_Labellist;


/* dynamic structures used by the parser */
typedef struct lua_Dyndata {
  struct {  /* list of active local variables */
    lua_Vardesc *arr;
    int n;
    int size;
  } actvar;
  lua_Labellist gt;  /* list of pending gotos */
  lua_Labellist label;   /* list of active labels */
} lua_Dyndata;


/* control of blocks */
struct lua_BlockCnt;  /* defined in lparser.c */


/* state needed to generate code for a given function */
typedef struct lua_FuncState {
  lua_Proto *f;  /* current function header */
  lua_Table *h;  /* table to find (and reuse) elements in `k' */
  struct lua_FuncState *prev;  /* enclosing function */
  struct lua_LexState *ls;  /* lexical state */
  struct lua_BlockCnt *bl;  /* chain of current blocks */
  int pc;  /* next position to code (equivalent to `ncode') */
  int lasttarget;   /* 'label' of last 'jump label' */
  int jpc;  /* list of pending jumps to `pc' */
  int nk;  /* number of elements in `k' */
  int np;  /* number of elements in `p' */
  int firstlocal;  /* index of first local var (in lua_Dyndata array) */
  short nlocvars;  /* number of elements in 'f->locvars' */
  lua_ubyte nactvar;  /* number of active local variables */
  lua_ubyte nups;  /* number of upvalues */
  lua_ubyte freereg;  /* first free register */
} lua_FuncState;


LUAI_FUNC lua_Closure *luaY_parser (lua_State *L, lua_ZIO *z, lua_Mbuffer *buff,
                                lua_Dyndata *dyd, const char *name, int firstchar);


#endif
