/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __35902fbc3b101809b693c0f81f39b01c__
#define __35902fbc3b101809b693c0f81f39b01c__

#include "lua_vm.hpp"
#include "lua_stack_entry.hpp"
#include "lua_value.hpp"
#include <cassert>

#include RTTI_TYPE_CONVERSION_H
#include RTTI_TYPE_TRAITS_H

namespace Lua
{
	namespace Internal
	{
		template <class RETURN_TYPE> struct FunctionInvoker
		{
			static inline int invoke(lua_State * L, int nargs, RETURN_TYPE (* const fn)(VM &))
			{
				VM & vm = VM::fromL(L);
				lua_settop(L, nargs + 1);
				StackEntry entry(vm, L->top - 1);
				RTTI::convert(entry, fn(vm));
				return 1;
			}
		};

		template <> struct FunctionInvoker<void>
		{
			static inline int invoke(lua_State * L, int nargs, void (* const fn)(VM &))
			{
				lua_settop(L, nargs);
				fn(VM::fromL(L));
				return 0;
			}
		};

		template <class TYPE> inline TYPE FunctionPtr(VM & vm)
		{
			lua_State * const L = vm.L;
			lua_assert(lua_ttisCclosure(L->ci->func));
			lua_CClosure * closure = lua_clCvalue(L->ci->func);
			lua_assert(closure->nupvalues > 0);
			lua_StkId upvalue = &closure->upvalue[closure->nupvalues - 1];
			lua_assert(lua_ttislcf(upvalue));
			return (TYPE)lua_fvalue(upvalue);
		}

		#include "lua_function_wrappers.hpp"
	}

	#include "lua_function_impl.hpp"
}

#endif
