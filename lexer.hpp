/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __81362c7860be8fc8530e6fd41b450b97__
#define __81362c7860be8fc8530e6fd41b450b97__

#include "lua.hpp"

namespace Lua
{
	class Lexer
	{
	public:
		Lexer();
		virtual ~Lexer();

		int parse(const void * data, size_t length, int prevBlockState = 0);

	protected:
		virtual void onBeforeParse() {}
		virtual void onAfterParse() {}
		virtual void onToken(size_t offset, size_t length, int token) { (void)offset; (void)length; (void)token; }
		virtual void onError(size_t offset, size_t length, int token) { (void)offset; (void)length; (void)token; }

	private:
		lua_State * L;
		lua_Mbuffer m_LexBuffer;
		lua_LexState m_LexState;

		static void errorCallback(lua_LexState * state, size_t start, size_t end, int baseToken);

		Lexer(const Lexer &);
		Lexer & operator=(const Lexer &);
	};
}

#endif
