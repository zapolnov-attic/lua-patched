/*
** $Id: lcode.c,v 2.62.1.1 2013/04/12 18:48:47 roberto Exp $
** Code generator for Lua
** See Copyright Notice in lua.h
*/


#include <stdlib.h>

#define lcode_c
#define LUA_CORE

#include "lua.h"

#include "lcode.h"
#include "ldebug.h"
#include "ldo.h"
#include "lgc.h"
#include "llex.h"
#include "lmem.h"
#include "lobject.h"
#include "lopcodes.h"
#include "lparser.h"
#include "lstring.h"
#include "ltable.h"
#include "lvm.h"


#define hasjumps(e)	((e)->t != (e)->f)


static int isnumeral(lua_expdesc *e) {
  return (e->k == LUA_VKNUM && e->t == LUA_NO_JUMP && e->f == LUA_NO_JUMP);
}


void luaK_nil (lua_FuncState *fs, int from, int n) {
  lua_Instruction *previous;
  int l = from + n - 1;  /* last register to set nil */
  if (fs->pc > fs->lasttarget) {  /* no jumps to current position? */
    previous = &fs->f->code[fs->pc-1];
    if (LUA_GET_OPCODE(*previous) == LUA_OP_LOADNIL) {
      int pfrom = LUA_GETARG_A(*previous);
      int pl = pfrom + LUA_GETARG_B(*previous);
      if ((pfrom <= from && from <= pl + 1) ||
          (from <= pfrom && pfrom <= l + 1)) {  /* can connect both? */
        if (pfrom < from) from = pfrom;  /* from = min(from, pfrom) */
        if (pl > l) l = pl;  /* l = max(l, pl) */
        LUA_SETARG_A(*previous, from);
        LUA_SETARG_B(*previous, l - from);
        return;
      }
    }  /* else go through */
  }
  luaK_codeABC(fs, LUA_OP_LOADNIL, from, n - 1, 0);  /* else no optimization */
}


int luaK_jump (lua_FuncState *fs) {
  int jpc = fs->jpc;  /* save list of jumps to here */
  int j;
  fs->jpc = LUA_NO_JUMP;
  j = luaK_codeAsBx(fs, LUA_OP_JMP, 0, LUA_NO_JUMP);
  luaK_concat(fs, &j, jpc);  /* keep them on hold */
  return j;
}


void luaK_ret (lua_FuncState *fs, int first, int nret) {
  luaK_codeABC(fs, LUA_OP_RETURN, first, nret+1, 0);
}


static int condjump (lua_FuncState *fs, lua_OpCode op, int A, int B, int C) {
  luaK_codeABC(fs, op, A, B, C);
  return luaK_jump(fs);
}


static void fixjump (lua_FuncState *fs, int pc, int dest) {
  lua_Instruction *jmp = &fs->f->code[pc];
  int offset = dest-(pc+1);
  lua_assert(dest != LUA_NO_JUMP);
  if (abs(offset) > LUA_MAXARG_sBx)
    luaX_syntaxerror(fs->ls, "control structure too long");
  LUA_SETARG_sBx(*jmp, offset);
}


/*
** returns current `pc' and marks it as a jump target (to avoid wrong
** optimizations with consecutive instructions not in the same basic block).
*/
int luaK_getlabel (lua_FuncState *fs) {
  fs->lasttarget = fs->pc;
  return fs->pc;
}


static int getjump (lua_FuncState *fs, int pc) {
  int offset = LUA_GETARG_sBx(fs->f->code[pc]);
  if (offset == LUA_NO_JUMP)  /* point to itself represents end of list */
    return LUA_NO_JUMP;  /* end of list */
  else
    return (pc+1)+offset;  /* turn offset into absolute position */
}


static lua_Instruction *getjumpcontrol (lua_FuncState *fs, int pc) {
  lua_Instruction *pi = &fs->f->code[pc];
  if (pc >= 1 && lua_testTMode(LUA_GET_OPCODE(*(pi-1))))
    return pi-1;
  else
    return pi;
}


/*
** check whether list has any jump that do not produce a value
** (or produce an inverted value)
*/
static int need_value (lua_FuncState *fs, int list) {
  for (; list != LUA_NO_JUMP; list = getjump(fs, list)) {
    lua_Instruction i = *getjumpcontrol(fs, list);
    if (LUA_GET_OPCODE(i) != LUA_OP_TESTSET) return 1;
  }
  return 0;  /* not found */
}


static int patchtestreg (lua_FuncState *fs, int node, int reg) {
  lua_Instruction *i = getjumpcontrol(fs, node);
  if (LUA_GET_OPCODE(*i) != LUA_OP_TESTSET)
    return 0;  /* cannot patch other instructions */
  if (reg != LUA_NO_REG && reg != LUA_GETARG_B(*i))
    LUA_SETARG_A(*i, reg);
  else  /* no register to put value or register already has the value */
    *i = LUA_CREATE_ABC(LUA_OP_TEST, LUA_GETARG_B(*i), 0, LUA_GETARG_C(*i));

  return 1;
}


static void removevalues (lua_FuncState *fs, int list) {
  for (; list != LUA_NO_JUMP; list = getjump(fs, list))
      patchtestreg(fs, list, LUA_NO_REG);
}


static void patchlistaux (lua_FuncState *fs, int list, int vtarget, int reg,
                          int dtarget) {
  while (list != LUA_NO_JUMP) {
    int next = getjump(fs, list);
    if (patchtestreg(fs, list, reg))
      fixjump(fs, list, vtarget);
    else
      fixjump(fs, list, dtarget);  /* jump to default target */
    list = next;
  }
}


static void dischargejpc (lua_FuncState *fs) {
  patchlistaux(fs, fs->jpc, fs->pc, LUA_NO_REG, fs->pc);
  fs->jpc = LUA_NO_JUMP;
}


void luaK_patchlist (lua_FuncState *fs, int list, int target) {
  if (target == fs->pc)
    luaK_patchtohere(fs, list);
  else {
    lua_assert(target < fs->pc);
    patchlistaux(fs, list, target, LUA_NO_REG, target);
  }
}


LUAI_FUNC void luaK_patchclose (lua_FuncState *fs, int list, int level) {
  level++;  /* argument is +1 to reserve 0 as non-op */
  while (list != LUA_NO_JUMP) {
    int next = getjump(fs, list);
    lua_assert(LUA_GET_OPCODE(fs->f->code[list]) == LUA_OP_JMP &&
                (LUA_GETARG_A(fs->f->code[list]) == 0 ||
                 LUA_GETARG_A(fs->f->code[list]) >= level));
    LUA_SETARG_A(fs->f->code[list], level);
    list = next;
  }
}


void luaK_patchtohere (lua_FuncState *fs, int list) {
  luaK_getlabel(fs);
  luaK_concat(fs, &fs->jpc, list);
}


void luaK_concat (lua_FuncState *fs, int *l1, int l2) {
  if (l2 == LUA_NO_JUMP) return;
  else if (*l1 == LUA_NO_JUMP)
    *l1 = l2;
  else {
    int list = *l1;
    int next;
    while ((next = getjump(fs, list)) != LUA_NO_JUMP)  /* find last element */
      list = next;
    fixjump(fs, list, l2);
  }
}


static int luaK_code (lua_FuncState *fs, lua_Instruction i) {
  lua_Proto *f = fs->f;
  dischargejpc(fs);  /* `pc' will change */
  /* put new instruction in code array */
  luaM_growvector(fs->ls->L, f->code, fs->pc, f->sizecode, lua_Instruction,
                  LUA_MAX_INT, "opcodes");
  f->code[fs->pc] = i;
  /* save corresponding line information */
  luaM_growvector(fs->ls->L, f->lineinfo, fs->pc, f->sizelineinfo, int,
                  LUA_MAX_INT, "opcodes");
  f->lineinfo[fs->pc] = fs->ls->lastline;
  return fs->pc++;
}


int luaK_codeABC (lua_FuncState *fs, lua_OpCode o, int a, int b, int c) {
  lua_assert(lua_getOpMode(o) == lua_iABC);
  lua_assert(lua_getBMode(o) != lua_OpArgN || b == 0);
  lua_assert(lua_getCMode(o) != lua_OpArgN || c == 0);
  lua_assert(a <= LUA_MAXARG_A && b <= LUA_MAXARG_B && c <= LUA_MAXARG_C);
  return luaK_code(fs, LUA_CREATE_ABC(o, a, b, c));
}


int luaK_codeABx (lua_FuncState *fs, lua_OpCode o, int a, unsigned int bc) {
  lua_assert(lua_getOpMode(o) == lua_iABx || lua_getOpMode(o) == lua_iAsBx);
  lua_assert(lua_getCMode(o) == lua_OpArgN);
  lua_assert(a <= LUA_MAXARG_A && bc <= LUA_MAXARG_Bx);
  return luaK_code(fs, LUA_CREATE_ABx(o, a, bc));
}


static int codeextraarg (lua_FuncState *fs, int a) {
  lua_assert(a <= LUA_MAXARG_Ax);
  return luaK_code(fs, LUA_CREATE_Ax(LUA_OP_EXTRAARG, a));
}


int luaK_codek (lua_FuncState *fs, int reg, int k) {
  if (k <= LUA_MAXARG_Bx)
    return luaK_codeABx(fs, LUA_OP_LOADK, reg, k);
  else {
    int p = luaK_codeABx(fs, LUA_OP_LOADKX, reg, 0);
    codeextraarg(fs, k);
    return p;
  }
}


void luaK_checkstack (lua_FuncState *fs, int n) {
  int newstack = fs->freereg + n;
  if (newstack > fs->f->maxstacksize) {
    if (newstack >= LUA_MAXSTACK)
      luaX_syntaxerror(fs->ls, "function or expression too complex");
    fs->f->maxstacksize = lua_cast_byte(newstack);
  }
}


void luaK_reserveregs (lua_FuncState *fs, int n) {
  luaK_checkstack(fs, n);
  fs->freereg += n;
}


static void freereg (lua_FuncState *fs, int reg) {
  if (!LUA_ISK(reg) && reg >= fs->nactvar) {
    fs->freereg--;
    lua_assert(reg == fs->freereg);
  }
}


static void freeexp (lua_FuncState *fs, lua_expdesc *e) {
  if (e->k == LUA_VNONRELOC)
    freereg(fs, e->u.info);
}


static int addk (lua_FuncState *fs, lua_TValue *key, lua_TValue *v) {
  lua_State *L = fs->ls->L;
  lua_TValue *idx = luaH_set(L, fs->h, key);
  lua_Proto *f = fs->f;
  int k, oldsize;
  if (lua_ttisnumber(idx)) {
    lua_Number n = lua_nvalue(idx);
    lua_number2int(k, n);
    if (luaV_rawequalobj(&f->k[k], v))
      return k;
    /* else may be a collision (e.g., between 0.0 and "\0\0\0\0\0\0\0\0");
       go through and create a new entry for this value */
  }
  /* constant not found; create a new entry */
  oldsize = f->sizek;
  k = fs->nk;
  /* numerical value does not need GC barrier;
     table has no metatable, so it does not need to invalidate cache */
  lua_setnvalue(idx, lua_cast_num(k));
  luaM_growvector(L, f->k, k, f->sizek, lua_TValue, LUA_MAXARG_Ax, "constants");
  while (oldsize < f->sizek) lua_setnilvalue(&f->k[oldsize++]);
  lua_setobj(L, &f->k[k], v);
  fs->nk++;
  luaC_barrier(L, f, v);
  return k;
}


int luaK_stringK (lua_FuncState *fs, lua_TString *s) {
  lua_TValue o;
  lua_setsvalue(fs->ls->L, &o, s);
  return addk(fs, &o, &o);
}


int luaK_numberK (lua_FuncState *fs, lua_Number r) {
  int n;
  lua_State *L = fs->ls->L;
  lua_TValue o;
  lua_setnvalue(&o, r);
  if (r == 0 || luai_numisnan(NULL, r)) {  /* handle -0 and NaN */
    /* use raw representation as key to avoid numeric problems */
    lua_setsvalue(L, L->top++, luaS_newlstr(L, (char *)&r, sizeof(r)));
    n = addk(fs, L->top - 1, &o);
    L->top--;
  }
  else
    n = addk(fs, &o, &o);  /* regular case */
  return n;
}


static int boolK (lua_FuncState *fs, int b) {
  lua_TValue o;
  lua_setbvalue(&o, b);
  return addk(fs, &o, &o);
}


static int nilK (lua_FuncState *fs) {
  lua_TValue k, v;
  lua_setnilvalue(&v);
  /* cannot use nil as key; instead use table itself to represent nil */
  lua_sethvalue(fs->ls->L, &k, fs->h);
  return addk(fs, &k, &v);
}


void luaK_setreturns (lua_FuncState *fs, lua_expdesc *e, int nresults) {
  if (e->k == LUA_VCALL) {  /* expression is an open function call? */
    LUA_SETARG_C(lua_getcode(fs, e), nresults+1);
  }
  else if (e->k == LUA_VVARARG) {
    LUA_SETARG_B(lua_getcode(fs, e), nresults+1);
    LUA_SETARG_A(lua_getcode(fs, e), fs->freereg);
    luaK_reserveregs(fs, 1);
  }
}


void luaK_setoneret (lua_FuncState *fs, lua_expdesc *e) {
  if (e->k == LUA_VCALL) {  /* expression is an open function call? */
    e->k = LUA_VNONRELOC;
    e->u.info = LUA_GETARG_A(lua_getcode(fs, e));
  }
  else if (e->k == LUA_VVARARG) {
    LUA_SETARG_B(lua_getcode(fs, e), 2);
    e->k = LUA_VRELOCABLE;  /* can relocate its simple result */
  }
}


void luaK_dischargevars (lua_FuncState *fs, lua_expdesc *e) {
  switch (e->k) {
    case LUA_VLOCAL: {
      e->k = LUA_VNONRELOC;
      break;
    }
    case LUA_VUPVAL: {
      e->u.info = luaK_codeABC(fs, LUA_OP_GETUPVAL, 0, e->u.info, 0);
      e->k = LUA_VRELOCABLE;
      break;
    }
    case LUA_VINDEXED: {
      lua_OpCode op = LUA_OP_GETTABUP;  /* assume 't' is in an upvalue */
      freereg(fs, e->u.ind.idx);
      if (e->u.ind.vt == LUA_VLOCAL) {  /* 't' is in a register? */
        freereg(fs, e->u.ind.t);
        op = LUA_OP_GETTABLE;
      }
      e->u.info = luaK_codeABC(fs, op, 0, e->u.ind.t, e->u.ind.idx);
      e->k = LUA_VRELOCABLE;
      break;
    }
    case LUA_VVARARG:
    case LUA_VCALL: {
      luaK_setoneret(fs, e);
      break;
    }
    default: break;  /* there is one value available (somewhere) */
  }
}


static int code_label (lua_FuncState *fs, int A, int b, int jump) {
  luaK_getlabel(fs);  /* those instructions may be jump targets */
  return luaK_codeABC(fs, LUA_OP_LOADBOOL, A, b, jump);
}


static void discharge2reg (lua_FuncState *fs, lua_expdesc *e, int reg) {
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case LUA_VNIL: {
      luaK_nil(fs, reg, 1);
      break;
    }
    case LUA_VFALSE: case LUA_VTRUE: {
      luaK_codeABC(fs, LUA_OP_LOADBOOL, reg, e->k == LUA_VTRUE, 0);
      break;
    }
    case LUA_VK: {
      luaK_codek(fs, reg, e->u.info);
      break;
    }
    case LUA_VKNUM: {
      luaK_codek(fs, reg, luaK_numberK(fs, e->u.nval));
      break;
    }
    case LUA_VRELOCABLE: {
      lua_Instruction *pc = &lua_getcode(fs, e);
      LUA_SETARG_A(*pc, reg);
      break;
    }
    case LUA_VNONRELOC: {
      if (reg != e->u.info)
        luaK_codeABC(fs, LUA_OP_MOVE, reg, e->u.info, 0);
      break;
    }
    default: {
      lua_assert(e->k == LUA_VVOID || e->k == LUA_VJMP);
      return;  /* nothing to do... */
    }
  }
  e->u.info = reg;
  e->k = LUA_VNONRELOC;
}


static void discharge2anyreg (lua_FuncState *fs, lua_expdesc *e) {
  if (e->k != LUA_VNONRELOC) {
    luaK_reserveregs(fs, 1);
    discharge2reg(fs, e, fs->freereg-1);
  }
}


static void exp2reg (lua_FuncState *fs, lua_expdesc *e, int reg) {
  discharge2reg(fs, e, reg);
  if (e->k == LUA_VJMP)
    luaK_concat(fs, &e->t, e->u.info);  /* put this jump in `t' list */
  if (hasjumps(e)) {
    int final;  /* position after whole expression */
    int p_f = LUA_NO_JUMP;  /* position of an eventual LOAD false */
    int p_t = LUA_NO_JUMP;  /* position of an eventual LOAD true */
    if (need_value(fs, e->t) || need_value(fs, e->f)) {
      int fj = (e->k == LUA_VJMP) ? LUA_NO_JUMP : luaK_jump(fs);
      p_f = code_label(fs, reg, 0, 1);
      p_t = code_label(fs, reg, 1, 0);
      luaK_patchtohere(fs, fj);
    }
    final = luaK_getlabel(fs);
    patchlistaux(fs, e->f, final, reg, p_f);
    patchlistaux(fs, e->t, final, reg, p_t);
  }
  e->f = e->t = LUA_NO_JUMP;
  e->u.info = reg;
  e->k = LUA_VNONRELOC;
}


void luaK_exp2nextreg (lua_FuncState *fs, lua_expdesc *e) {
  luaK_dischargevars(fs, e);
  freeexp(fs, e);
  luaK_reserveregs(fs, 1);
  exp2reg(fs, e, fs->freereg - 1);
}


int luaK_exp2anyreg (lua_FuncState *fs, lua_expdesc *e) {
  luaK_dischargevars(fs, e);
  if (e->k == LUA_VNONRELOC) {
    if (!hasjumps(e)) return e->u.info;  /* exp is already in a register */
    if (e->u.info >= fs->nactvar) {  /* reg. is not a local? */
      exp2reg(fs, e, e->u.info);  /* put value on it */
      return e->u.info;
    }
  }
  luaK_exp2nextreg(fs, e);  /* default */
  return e->u.info;
}


void luaK_exp2anyregup (lua_FuncState *fs, lua_expdesc *e) {
  if (e->k != LUA_VUPVAL || hasjumps(e))
    luaK_exp2anyreg(fs, e);
}


void luaK_exp2val (lua_FuncState *fs, lua_expdesc *e) {
  if (hasjumps(e))
    luaK_exp2anyreg(fs, e);
  else
    luaK_dischargevars(fs, e);
}


int luaK_exp2RK (lua_FuncState *fs, lua_expdesc *e) {
  luaK_exp2val(fs, e);
  switch (e->k) {
    case LUA_VTRUE:
    case LUA_VFALSE:
    case LUA_VNIL: {
      if (fs->nk <= LUA_MAXINDEXRK) {  /* constant fits in RK operand? */
        e->u.info = (e->k == LUA_VNIL) ? nilK(fs) : boolK(fs, (e->k == LUA_VTRUE));
        e->k = LUA_VK;
        return LUA_RKASK(e->u.info);
      }
      else break;
    }
    case LUA_VKNUM: {
      e->u.info = luaK_numberK(fs, e->u.nval);
      e->k = LUA_VK;
      /* go through */
    }
    case LUA_VK: {
      if (e->u.info <= LUA_MAXINDEXRK)  /* constant fits in argC? */
        return LUA_RKASK(e->u.info);
      else break;
    }
    default: break;
  }
  /* not a constant in the right range: put it in a register */
  return luaK_exp2anyreg(fs, e);
}


void luaK_storevar (lua_FuncState *fs, lua_expdesc *var, lua_expdesc *ex) {
  switch (var->k) {
    case LUA_VLOCAL: {
      freeexp(fs, ex);
      exp2reg(fs, ex, var->u.info);
      return;
    }
    case LUA_VUPVAL: {
      int e = luaK_exp2anyreg(fs, ex);
      luaK_codeABC(fs, LUA_OP_SETUPVAL, e, var->u.info, 0);
      break;
    }
    case LUA_VINDEXED: {
      lua_OpCode op = (var->u.ind.vt == LUA_VLOCAL) ? LUA_OP_SETTABLE : LUA_OP_SETTABUP;
      int e = luaK_exp2RK(fs, ex);
      luaK_codeABC(fs, op, var->u.ind.t, var->u.ind.idx, e);
      break;
    }
    default: {
      lua_assert(0);  /* invalid var kind to store */
      break;
    }
  }
  freeexp(fs, ex);
}


void luaK_self (lua_FuncState *fs, lua_expdesc *e, lua_expdesc *key) {
  int ereg;
  luaK_exp2anyreg(fs, e);
  ereg = e->u.info;  /* register where 'e' was placed */
  freeexp(fs, e);
  e->u.info = fs->freereg;  /* base register for op_self */
  e->k = LUA_VNONRELOC;
  luaK_reserveregs(fs, 2);  /* function and 'self' produced by op_self */
  luaK_codeABC(fs, LUA_OP_SELF, e->u.info, ereg, luaK_exp2RK(fs, key));
  freeexp(fs, key);
}


static void invertjump (lua_FuncState *fs, lua_expdesc *e) {
  lua_Instruction *pc = getjumpcontrol(fs, e->u.info);
  lua_assert(lua_testTMode(LUA_GET_OPCODE(*pc)) && LUA_GET_OPCODE(*pc) != LUA_OP_TESTSET &&
                                           LUA_GET_OPCODE(*pc) != LUA_OP_TEST);
  LUA_SETARG_A(*pc, !(LUA_GETARG_A(*pc)));
}


static int jumponcond (lua_FuncState *fs, lua_expdesc *e, int cond) {
  if (e->k == LUA_VRELOCABLE) {
    lua_Instruction ie = lua_getcode(fs, e);
    if (LUA_GET_OPCODE(ie) == LUA_OP_NOT) {
      fs->pc--;  /* remove previous LUA_OP_NOT */
      return condjump(fs, LUA_OP_TEST, LUA_GETARG_B(ie), 0, !cond);
    }
    /* else go through */
  }
  discharge2anyreg(fs, e);
  freeexp(fs, e);
  return condjump(fs, LUA_OP_TESTSET, LUA_NO_REG, e->u.info, cond);
}


void luaK_goiftrue (lua_FuncState *fs, lua_expdesc *e) {
  int pc;  /* pc of last jump */
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case LUA_VJMP: {
      invertjump(fs, e);
      pc = e->u.info;
      break;
    }
    case LUA_VK: case LUA_VKNUM: case LUA_VTRUE: {
      pc = LUA_NO_JUMP;  /* always true; do nothing */
      break;
    }
    default: {
      pc = jumponcond(fs, e, 0);
      break;
    }
  }
  luaK_concat(fs, &e->f, pc);  /* insert last jump in `f' list */
  luaK_patchtohere(fs, e->t);
  e->t = LUA_NO_JUMP;
}


void luaK_goiffalse (lua_FuncState *fs, lua_expdesc *e) {
  int pc;  /* pc of last jump */
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case LUA_VJMP: {
      pc = e->u.info;
      break;
    }
    case LUA_VNIL: case LUA_VFALSE: {
      pc = LUA_NO_JUMP;  /* always false; do nothing */
      break;
    }
    default: {
      pc = jumponcond(fs, e, 1);
      break;
    }
  }
  luaK_concat(fs, &e->t, pc);  /* insert last jump in `t' list */
  luaK_patchtohere(fs, e->f);
  e->f = LUA_NO_JUMP;
}


static void codenot (lua_FuncState *fs, lua_expdesc *e) {
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case LUA_VNIL: case LUA_VFALSE: {
      e->k = LUA_VTRUE;
      break;
    }
    case LUA_VK: case LUA_VKNUM: case LUA_VTRUE: {
      e->k = LUA_VFALSE;
      break;
    }
    case LUA_VJMP: {
      invertjump(fs, e);
      break;
    }
    case LUA_VRELOCABLE:
    case LUA_VNONRELOC: {
      discharge2anyreg(fs, e);
      freeexp(fs, e);
      e->u.info = luaK_codeABC(fs, LUA_OP_NOT, 0, e->u.info, 0);
      e->k = LUA_VRELOCABLE;
      break;
    }
    default: {
      lua_assert(0);  /* cannot happen */
      break;
    }
  }
  /* interchange true and false lists */
  { int temp = e->f; e->f = e->t; e->t = temp; }
  removevalues(fs, e->f);
  removevalues(fs, e->t);
}


void luaK_indexed (lua_FuncState *fs, lua_expdesc *t, lua_expdesc *k) {
  lua_assert(!hasjumps(t));
  t->u.ind.t = t->u.info;
  t->u.ind.idx = luaK_exp2RK(fs, k);
  t->u.ind.vt = (t->k == LUA_VUPVAL) ? LUA_VUPVAL
                                 : lua_check_exp(lua_vkisinreg(t->k), LUA_VLOCAL);
  t->k = LUA_VINDEXED;
}


static int constfolding (lua_OpCode op, lua_expdesc *e1, lua_expdesc *e2) {
  lua_Number r;
  if (!isnumeral(e1) || !isnumeral(e2)) return 0;
  if ((op == LUA_OP_DIV || op == LUA_OP_MOD) && e2->u.nval == 0)
    return 0;  /* do not attempt to divide by 0 */
  r = luaO_arith(op - LUA_OP_ADD + LUA_OPADD, e1->u.nval, e2->u.nval);
  e1->u.nval = r;
  return 1;
}


static void codearith (lua_FuncState *fs, lua_OpCode op,
                       lua_expdesc *e1, lua_expdesc *e2, int line) {
  if (constfolding(op, e1, e2))
    return;
  else {
    int o2 = (op != LUA_OP_UNM && op != LUA_OP_LEN) ? luaK_exp2RK(fs, e2) : 0;
    int o1 = luaK_exp2RK(fs, e1);
    if (o1 > o2) {
      freeexp(fs, e1);
      freeexp(fs, e2);
    }
    else {
      freeexp(fs, e2);
      freeexp(fs, e1);
    }
    e1->u.info = luaK_codeABC(fs, op, 0, o1, o2);
    e1->k = LUA_VRELOCABLE;
    luaK_fixline(fs, line);
  }
}


static void codecomp (lua_FuncState *fs, lua_OpCode op, int cond, lua_expdesc *e1,
                                                          lua_expdesc *e2) {
  int o1 = luaK_exp2RK(fs, e1);
  int o2 = luaK_exp2RK(fs, e2);
  freeexp(fs, e2);
  freeexp(fs, e1);
  if (cond == 0 && op != LUA_OP_EQ) {
    int temp;  /* exchange args to replace by `<' or `<=' */
    temp = o1; o1 = o2; o2 = temp;  /* o1 <==> o2 */
    cond = 1;
  }
  e1->u.info = condjump(fs, op, cond, o1, o2);
  e1->k = LUA_VJMP;
}


void luaK_prefix (lua_FuncState *fs, lua_UnOpr op, lua_expdesc *e, int line) {
  lua_expdesc e2;
  e2.t = e2.f = LUA_NO_JUMP; e2.k = LUA_VKNUM; e2.u.nval = 0;
  switch (op) {
    case LUA_OPR_MINUS: {
      if (isnumeral(e))  /* minus constant? */
        e->u.nval = luai_numunm(NULL, e->u.nval);  /* fold it */
      else {
        luaK_exp2anyreg(fs, e);
        codearith(fs, LUA_OP_UNM, e, &e2, line);
      }
      break;
    }
    case LUA_OPR_NOT: codenot(fs, e); break;
    case LUA_OPR_LEN: {
      luaK_exp2anyreg(fs, e);  /* cannot operate on constants */
      codearith(fs, LUA_OP_LEN, e, &e2, line);
      break;
    }
    default: lua_assert(0);
  }
}


void luaK_infix (lua_FuncState *fs, lua_BinOpr op, lua_expdesc *v) {
  switch (op) {
    case LUA_OPR_AND: {
      luaK_goiftrue(fs, v);
      break;
    }
    case LUA_OPR_OR: {
      luaK_goiffalse(fs, v);
      break;
    }
    case LUA_OPR_CONCAT: {
      luaK_exp2nextreg(fs, v);  /* operand must be on the `stack' */
      break;
    }
    case LUA_OPR_ADD: case LUA_OPR_SUB: case LUA_OPR_MUL: case LUA_OPR_DIV:
    case LUA_OPR_MOD: case LUA_OPR_POW: {
      if (!isnumeral(v)) luaK_exp2RK(fs, v);
      break;
    }
    default: {
      luaK_exp2RK(fs, v);
      break;
    }
  }
}


void luaK_posfix (lua_FuncState *fs, lua_BinOpr op,
                  lua_expdesc *e1, lua_expdesc *e2, int line) {
  switch (op) {
    case LUA_OPR_AND: {
      lua_assert(e1->t == LUA_NO_JUMP);  /* list must be closed */
      luaK_dischargevars(fs, e2);
      luaK_concat(fs, &e2->f, e1->f);
      *e1 = *e2;
      break;
    }
    case LUA_OPR_OR: {
      lua_assert(e1->f == LUA_NO_JUMP);  /* list must be closed */
      luaK_dischargevars(fs, e2);
      luaK_concat(fs, &e2->t, e1->t);
      *e1 = *e2;
      break;
    }
    case LUA_OPR_CONCAT: {
      luaK_exp2val(fs, e2);
      if (e2->k == LUA_VRELOCABLE && LUA_GET_OPCODE(lua_getcode(fs, e2)) == LUA_OP_CONCAT) {
        lua_assert(e1->u.info == LUA_GETARG_B(lua_getcode(fs, e2))-1);
        freeexp(fs, e1);
        LUA_SETARG_B(lua_getcode(fs, e2), e1->u.info);
        e1->k = LUA_VRELOCABLE; e1->u.info = e2->u.info;
      }
      else {
        luaK_exp2nextreg(fs, e2);  /* operand must be on the 'stack' */
        codearith(fs, LUA_OP_CONCAT, e1, e2, line);
      }
      break;
    }
    case LUA_OPR_VEC2:
      codearith(fs, lua_cast(lua_OpCode, LUA_OP_VEC2), e1, e2, line);
      break;
    case LUA_OPR_ADD: case LUA_OPR_SUB: case LUA_OPR_MUL: case LUA_OPR_DIV:
    case LUA_OPR_MOD: case LUA_OPR_POW: {
      codearith(fs, lua_cast(lua_OpCode, op - LUA_OPR_ADD + LUA_OP_ADD), e1, e2, line);
      break;
    }
    case LUA_OPR_EQ: case LUA_OPR_LT: case LUA_OPR_LE: {
      codecomp(fs, lua_cast(lua_OpCode, op - LUA_OPR_EQ + LUA_OP_EQ), 1, e1, e2);
      break;
    }
    case LUA_OPR_NE: case LUA_OPR_GT: case LUA_OPR_GE: {
      codecomp(fs, lua_cast(lua_OpCode, op - LUA_OPR_NE + LUA_OP_EQ), 0, e1, e2);
      break;
    }
    default: lua_assert(0);
  }
}


void luaK_fixline (lua_FuncState *fs, int line) {
  fs->f->lineinfo[fs->pc - 1] = line;
}


void luaK_setlist (lua_FuncState *fs, int base, int nelems, int tostore) {
  int c =  (nelems - 1)/LUA_LFIELDS_PER_FLUSH + 1;
  int b = (tostore == LUA_MULTRET) ? 0 : tostore;
  lua_assert(tostore != 0);
  if (c <= LUA_MAXARG_C)
    luaK_codeABC(fs, LUA_OP_SETLIST, base, b, c);
  else if (c <= LUA_MAXARG_Ax) {
    luaK_codeABC(fs, LUA_OP_SETLIST, base, b, 0);
    codeextraarg(fs, c);
  }
  else
    luaX_syntaxerror(fs->ls, "constructor too long");
  fs->freereg = base + 1;  /* free registers with list values */
}

