/*
** $Id: lobject.h,v 2.71.1.1 2013/04/12 18:48:47 roberto Exp $
** Type definitions for Lua objects
** See Copyright Notice in lua.h
*/


#ifndef lobject_h
#define lobject_h


#include <stdarg.h>


#include "llimits.h"
#include "lua.h"


/*
** Extra tags for non-values
*/
#define LUA_TPROTO	LUA_NUMTAGS
#define LUA_TUPVAL	(LUA_NUMTAGS+1)
#define LUA_TDEADKEY	(LUA_NUMTAGS+2)

/*
** number of all possible tags (including LUA_TNONE but excluding DEADKEY)
*/
#define LUA_TOTALTAGS	(LUA_TUPVAL+2)


/*
** tags for Tagged Values have the following use of bits:
** bits 0-3: actual tag (a LUA_T* value)
** bits 4-5: variant bits
** bit 6: whether value is collectable
*/

#define LUA_VARBITS		(3 << 4)


/*
** LUA_TFUNCTION variants:
** 0 - Lua function
** 1 - light C function
** 2 - regular C function (closure)
*/

/* Variant tags for functions */
#define LUA_TLCL	(LUA_TFUNCTION | (0 << 4))  /* Lua closure */
#define LUA_TLCF	(LUA_TFUNCTION | (1 << 4))  /* light C function */
#define LUA_TCCL	(LUA_TFUNCTION | (2 << 4))  /* C closure */


/* Variant tags for strings */
#define LUA_TSHRSTR	(LUA_TSTRING | (0 << 4))  /* short strings */
#define LUA_TLNGSTR	(LUA_TSTRING | (1 << 4))  /* long strings */


/* Bit mark for collectable types */
#define LUA_BIT_ISCOLLECTABLE	(1 << 6)

/* mark a tag as collectable */
#define lua_ctb(t)			((t) | LUA_BIT_ISCOLLECTABLE)


/*
** Union of all collectable objects
*/
typedef union lua_GCObject lua_GCObject;


/*
** Common Header for all collectable objects (in macro form, to be
** included in other objects)
*/
#define lua_CommonHeader	lua_GCObject *next; lua_ubyte tt; lua_ubyte marked; lua_ubyte refcount
#define LUA_MAX_REFCOUNT 0xFF


/*
** Common header in struct form
*/
typedef struct lua_GCheader {
  lua_CommonHeader;
} lua_GCheader;



/*
** Union of all Lua values
*/
typedef union lua_Value lua_Value;


#define lua_numfield	lua_Number n;    /* numbers */



/*
** Tagged Values. This is the basic representation of values in Lua,
** an actual value plus a tag with its type.
*/

#define lua_TValuefields	lua_Value value_; int tt_

typedef struct lua_TValue lua_TValue;


/* macro defining a nil value */
#define LUA_NILCONSTANT	{NULL}, LUA_TNIL


#define lua_val_(o)		((o)->value_)
#define lua_num_(o)		(lua_val_(o).n)


/* raw type tag of a lua_TValue */
#define lua_rttype(o)	((o)->tt_)

/* tag with no variants (bits 0-3) */
#define lua_novariant(x)	((x) & 0x0F)

/* type tag of a lua_TValue (bits 0-3 for tags + variant bits 4-5) */
#define lua_ttype(o)	(lua_rttype(o) & 0x3F)

/* type tag of a lua_TValue with no variants (bits 0-3) */
#define lua_ttypenv(o)	(lua_novariant(lua_rttype(o)))


/* Macros to test type */
#define lua_checktag(o,t)		(lua_rttype(o) == (t))
#define lua_checktype(o,t)		(lua_ttypenv(o) == (t))
#define lua_ttisnulluserpointer(o) \
	(lua_checktype((o), LUA_TUSERPOINTER) && !((lua_UserPointer *)(lua_rawuptrvalue((o)) + 1))->ptr)
#define lua_ttisnumber(o)		lua_checktag((o), LUA_TNUMBER)
#define lua_ttisvec2(o)		lua_checktag((o), LUA_TVEC2)
#define lua_ttisnil(o)		lua_checktag((o), LUA_TNIL)
#define lua_ttisboolean(o)		lua_checktag((o), LUA_TBOOLEAN)
#define lua_ttislightuserdata(o)	lua_checktag((o), LUA_TLIGHTUSERDATA)
#define lua_ttisstring(o)		lua_checktype((o), LUA_TSTRING)
#define lua_ttisshrstring(o)	lua_checktag((o), lua_ctb(LUA_TSHRSTR))
#define lua_ttislngstring(o)	lua_checktag((o), lua_ctb(LUA_TLNGSTR))
#define lua_ttistable(o)		lua_checktag((o), lua_ctb(LUA_TTABLE))
#define lua_ttisfunction(o)		lua_checktype(o, LUA_TFUNCTION)
#define lua_ttisclosure(o)		((lua_rttype(o) & 0x1F) == LUA_TFUNCTION)
#define lua_ttisCclosure(o)		lua_checktag((o), lua_ctb(LUA_TCCL))
#define lua_ttisLclosure(o)		lua_checktag((o), lua_ctb(LUA_TLCL))
#define lua_ttislcf(o)		lua_checktag((o), LUA_TLCF)
#define lua_ttisuserdata(o)		lua_checktag((o), lua_ctb(LUA_TUSERDATA))
#define lua_ttisuserpointer(o)	lua_checktag((o), lua_ctb(LUA_TUSERPOINTER))
#define lua_ttisthread(o)		lua_checktag((o), lua_ctb(LUA_TTHREAD))
#define lua_ttisdeadkey(o)		lua_checktag((o), LUA_TDEADKEY)

#define lua_ttisequal(o1,o2)	(lua_rttype(o1) == lua_rttype(o2))

/* Macros to access values */
#define lua_nvalue(o)	lua_check_exp(lua_ttisnumber(o), lua_num_(o))
#define lua_gcvalue(o)	lua_check_exp(lua_iscollectable(o), lua_val_(o).gc)
#define lua_pvalue(o)	lua_check_exp(lua_ttislightuserdata(o), lua_val_(o).p)
#define lua_rawtsvalue(o)	lua_check_exp(lua_ttisstring(o), &lua_val_(o).gc->ts)
#define lua_tsvalue(o)	(&lua_rawtsvalue(o)->tsv)
#define lua_rawuvalue(o)	lua_check_exp(lua_ttisuserdata(o), &lua_val_(o).gc->u)
#define lua_uvalue(o)	(&lua_rawuvalue(o)->uv)
#define lua_rawuptrvalue(o)	lua_check_exp(lua_ttisuserpointer(o), &lua_val_(o).gc->u)
#define lua_uptrvalue(o)	(&lua_rawuptrvalue(o)->uv)
#define lua_clvalue(o)	lua_check_exp(lua_ttisclosure(o), &lua_val_(o).gc->cl)
#define lua_clLvalue(o)	lua_check_exp(lua_ttisLclosure(o), &lua_val_(o).gc->cl.l)
#define lua_clCvalue(o)	lua_check_exp(lua_ttisCclosure(o), &lua_val_(o).gc->cl.c)
#define lua_fvalue(o)	lua_check_exp(lua_ttislcf(o), lua_val_(o).f)
#define lua_hvalue(o)	lua_check_exp(lua_ttistable(o), &lua_val_(o).gc->h)
#define lua_bvalue(o)	lua_check_exp(lua_ttisboolean(o), lua_val_(o).b)
#define lua_thvalue(o)	lua_check_exp(lua_ttisthread(o), &lua_val_(o).gc->th)
#define lua_vec2value(o)	lua_check_exp(lua_ttisvec2(o), lua_val_(o).vec2)
/* a dead value may get the 'gc' field, but cannot access its contents */
#define lua_deadvalue(o)	lua_check_exp(lua_ttisdeadkey(o), lua_cast(void *, lua_val_(o).gc))

#define lua_isfalse(o)	(lua_ttisnil(o) || (lua_ttisboolean(o) && lua_bvalue(o) == 0) || lua_ttisnulluserpointer(o))


#define lua_iscollectable(o)	(lua_rttype(o) & LUA_BIT_ISCOLLECTABLE)


/* Macros for internal tests */
#define lua_righttt(obj)		(lua_ttype(obj) == lua_gcvalue(obj)->gch.tt)

#define lua_checkliveness(g,obj) \
	lua_longassert(!lua_iscollectable(obj) || \
			(lua_righttt(obj) && !lua_isdead(g,lua_gcvalue(obj))))


/* Macros to set values */
#define lua_settt_(o,t)	((o)->tt_=(t))

#define lua_setnvalue(obj,x) \
  { lua_TValue *io=(obj); lua_num_(io)=(x); lua_settt_(io, LUA_TNUMBER); }

#define lua_setnilvalue(obj) lua_settt_(obj, LUA_TNIL)

#define lua_setfvalue(obj,x) \
  { lua_TValue *io=(obj); lua_val_(io).f=(x); lua_settt_(io, LUA_TLCF); }

#define lua_setpvalue(obj,x) \
  { lua_TValue *io=(obj); lua_val_(io).p=(x); lua_settt_(io, LUA_TLIGHTUSERDATA); }

#define lua_setbvalue(obj,x) \
  { lua_TValue *io=(obj); lua_val_(io).b=(x); lua_settt_(io, LUA_TBOOLEAN); }

#define lua_setgcovalue(L,obj,x) \
  { lua_TValue *io=(obj); lua_GCObject *i_g=(x); \
    lua_val_(io).gc=i_g; lua_settt_(io, lua_ctb(lua_gch(i_g)->tt)); }

#define lua_setsvalue(L,obj,x) \
  { lua_TValue *io=(obj); \
    lua_TString *x_ = (x); \
    lua_val_(io).gc=lua_cast(lua_GCObject *, x_); lua_settt_(io, lua_ctb(x_->tsv.tt)); \
    lua_checkliveness(LUA_G(L),io); }

#define lua_setuvalue(L,obj,x) \
  { lua_TValue *io=(obj); \
    lua_val_(io).gc=lua_cast(lua_GCObject *, (x)); lua_settt_(io, lua_ctb(LUA_TUSERDATA)); \
    lua_checkliveness(LUA_G(L),io); }

#define lua_setuptrvalue(L,obj,x) \
  { lua_TValue *io=(obj); \
    lua_val_(io).gc=lua_cast(lua_GCObject *, (x)); lua_settt_(io, lua_ctb(LUA_TUSERPOINTER)); \
    lua_checkliveness(LUA_G(L),io); }

#define lua_setthvalue(L,obj,x) \
  { lua_TValue *io=(obj); \
    lua_val_(io).gc=lua_cast(lua_GCObject *, (x)); lua_settt_(io, lua_ctb(LUA_TTHREAD)); \
    lua_checkliveness(LUA_G(L),io); }

#define lua_setclLvalue(L,obj,x) \
  { lua_TValue *io=(obj); \
    lua_val_(io).gc=lua_cast(lua_GCObject *, (x)); lua_settt_(io, lua_ctb(LUA_TLCL)); \
    lua_checkliveness(LUA_G(L),io); }

#define lua_setclCvalue(L,obj,x) \
  { lua_TValue *io=(obj); \
    lua_val_(io).gc=lua_cast(lua_GCObject *, (x)); lua_settt_(io, lua_ctb(LUA_TCCL)); \
    lua_checkliveness(LUA_G(L),io); }

#define lua_sethvalue(L,obj,x) \
  { lua_TValue *io=(obj); \
    lua_val_(io).gc=lua_cast(lua_GCObject *, (x)); lua_settt_(io, lua_ctb(LUA_TTABLE)); \
    lua_checkliveness(LUA_G(L),io); }

#define lua_setvec2value(obj,x,y) \
  { lua_TValue *io=(obj); lua_val_(io).vec2[0]=(x); lua_val_(io).vec2[1]=(y); lua_settt_(io, LUA_TVEC2); }

#define lua_setdeadvalue(obj)	lua_settt_(obj, LUA_TDEADKEY)



#define lua_setobj(L,obj1,obj2) \
	{ const lua_TValue *io2=(obj2); lua_TValue *io1=(obj1); \
	  io1->value_ = io2->value_; io1->tt_ = io2->tt_; \
	  lua_checkliveness(LUA_G(L),io1); }


/*
** different types of assignments, according to destination
*/

/* from stack to (same) stack */
#define lua_setobjs2s	lua_setobj
/* to stack (not from same stack) */
#define lua_setobj2s	lua_setobj
#define lua_setsvalue2s	lua_setsvalue
#define lua_sethvalue2s	lua_sethvalue
#define lua_setptvalue2s	lua_setptvalue
/* from table to same table */
#define lua_setobjt2t	lua_setobj
/* to table */
#define lua_setobj2t	lua_setobj
/* to new object */
#define lua_setobj2n	lua_setobj
#define lua_setsvalue2n	lua_setsvalue


/* check whether a number is valid (useful only for NaN trick) */
#define luai_checknum(L,o,c)	{ /* empty */ }


/*
** {======================================================
** NaN Trick
** =======================================================
*/
#if defined(LUA_NANTRICK)

/*
** numbers are represented in the 'd_' field. All other values have the
** value (LUA_NNMARK | tag) in 'tt__'. A number with such pattern would be
** a "signaled NaN", which is never generated by regular operations by
** the CPU (nor by 'strtod')
*/

/* allows for external implementation for part of the trick */
#if !defined(LUA_NNMARK)	/* { */


#if !defined(LUA_IEEEENDIAN)
#error option 'LUA_NANTRICK' needs 'LUA_IEEEENDIAN'
#endif


#define LUA_NNMARK		0x7FF7A500
#define LUA_NNMASK		0x7FFFFF00

#undef lua_TValuefields
#undef LUA_NILCONSTANT

#if (LUA_IEEEENDIAN == 0)	/* { */

/* little endian */
#define lua_TValuefields  \
	union { struct { lua_Value v__; int tt__; } i; double d__; } u
#define LUA_NILCONSTANT	{{{NULL}, lua_tag2tt(LUA_TNIL)}}
/* field-access macros */
#define lua_v_(o)		((o)->u.i.v__)
#define lua_d_(o)		((o)->u.d__)
#define lua_tt_(o)		((o)->u.i.tt__)

#else				/* }{ */

/* big endian */
#define lua_TValuefields  \
	union { struct { int tt__; lua_Value v__; } i; double d__; } u
#define LUA_NILCONSTANT	{{lua_tag2tt(LUA_TNIL), {NULL}}}
/* field-access macros */
#define lua_v_(o)		((o)->u.i.v__)
#define lua_d_(o)		((o)->u.d__)
#define lua_tt_(o)		((o)->u.i.tt__)

#endif				/* } */

#endif			/* } */


/* correspondence with standard representation */
#undef lua_val_
#define lua_val_(o)		lua_v_(o)
#undef lua_num_
#define lua_num_(o)		lua_d_(o)


#undef lua_numfield
#define lua_numfield	/* no such field; numbers are the entire struct */

/* basic check to distinguish numbers from non-numbers */
#undef lua_ttisnumber
#define lua_ttisnumber(o)	((lua_tt_(o) & LUA_NNMASK) != LUA_NNMARK)

#define lua_tag2tt(t)	(LUA_NNMARK | (t))

#undef lua_rttype
#define lua_rttype(o)	(lua_ttisnumber(o) ? LUA_TNUMBER : lua_tt_(o) & 0xff)

#undef lua_settt_
#define lua_settt_(o,t)	(lua_tt_(o) = lua_tag2tt(t))

#undef lua_setnvalue
#define lua_setnvalue(obj,x) \
	{ lua_TValue *io_=(obj); lua_num_(io_)=(x); lua_assert(lua_ttisnumber(io_)); }

#undef lua_setobj
#define lua_setobj(L,obj1,obj2) \
	{ const lua_TValue *o2_=(obj2); lua_TValue *o1_=(obj1); \
	  o1_->u = o2_->u; \
	  lua_checkliveness(LUA_G(L),o1_); }


/*
** these redefinitions are not mandatory, but these forms are more efficient
*/

#undef lua_checktag
#undef lua_checktype
#define lua_checktag(o,t)	(lua_tt_(o) == lua_tag2tt(t))
#define lua_checktype(o,t)	(lua_ctb(lua_tt_(o) | LUA_VARBITS) == lua_ctb(lua_tag2tt(t) | LUA_VARBITS))

#undef lua_ttisequal
#define lua_ttisequal(o1,o2)  \
	(lua_ttisnumber(o1) ? lua_ttisnumber(o2) : (lua_tt_(o1) == lua_tt_(o2)))


#undef luai_checknum
#define luai_checknum(L,o,c)	{ if (!lua_ttisnumber(o)) c; }

#endif
/* }====================================================== */



/*
** {======================================================
** types and prototypes
** =======================================================
*/


union lua_Value {
  lua_GCObject *gc;    /* collectable objects */
  void *p;         /* light userdata */
  int b;           /* booleans */
  lua_CFunction f; /* light C functions */
  float vec2[2];
  lua_numfield         /* numbers */
};


struct lua_TValue {
  lua_TValuefields;
};


typedef lua_TValue *lua_StkId;  /* index to stack elements */




/*
** Header for string value; string bytes follow the end of this structure
*/
typedef union lua_TString {
  lua_Umaxalign dummy;  /* ensures maximum alignment for strings */
  struct {
    lua_CommonHeader;
    lua_ubyte extra;  /* reserved words for short strings; "has hash" for longs */
    unsigned int hash;
    size_t len;  /* number of characters in string */
  } tsv;
} lua_TString;


/* get the actual string (array of bytes) from a lua_TString */
#define lua_getstr(ts)	lua_cast(const char *, (ts) + 1)

/* get the actual string (array of bytes) from a Lua value */
#define lua_svalue(o)       lua_getstr(lua_rawtsvalue(o))


/*
** Header for userdata; memory area follows the end of this structure
*/
typedef union lua_Udata {
  lua_Umaxalign dummy;  /* ensures maximum alignment for `local' udata */
  struct {
    lua_CommonHeader;
    struct lua_Table *metatable;
    struct lua_Table *env;
    size_t len;  /* number of bytes */
  } uv;
} lua_Udata;


typedef struct lua_UserPointer {
  void * ptr;
  char weak;
} lua_UserPointer;

/*
** Description of an upvalue for function prototypes
*/
typedef struct lua_Upvaldesc {
  lua_TString *name;  /* upvalue name (for debug information) */
  lua_ubyte instack;  /* whether it is in stack */
  lua_ubyte idx;  /* index of upvalue (in stack or in outer function's list) */
} lua_Upvaldesc;


/*
** Description of a local variable for function prototypes
** (used for debug information)
*/
typedef struct lua_LocVar {
  lua_TString *varname;
  int startpc;  /* first point where variable is active */
  int endpc;    /* first point where variable is dead */
} lua_LocVar;


/*
** Function Prototypes
*/
typedef struct lua_Proto {
  lua_CommonHeader;
  lua_TValue *k;  /* constants used by the function */
  lua_Instruction *code;
  struct lua_Proto **p;  /* functions defined inside the function */
  int *lineinfo;  /* map from opcodes to source lines (debug information) */
  lua_LocVar *locvars;  /* information about local variables (debug information) */
  lua_Upvaldesc *upvalues;  /* upvalue information */
  union lua_Closure *cache;  /* last created closure with this prototype */
  lua_TString  *source;  /* used for debug information */
  int sizeupvalues;  /* size of 'upvalues' */
  int sizek;  /* size of `k' */
  int sizecode;
  int sizelineinfo;
  int sizep;  /* size of `p' */
  int sizelocvars;
  int linedefined;
  int lastlinedefined;
  lua_GCObject *gclist;
  lua_ubyte numparams;  /* number of fixed parameters */
  lua_ubyte is_vararg;
  lua_ubyte maxstacksize;  /* maximum stack used by this function */
} lua_Proto;



/*
** Lua Upvalues
*/
typedef struct lua_UpVal {
  lua_CommonHeader;
  lua_TValue *v;  /* points to stack or to its own value */
  union {
    lua_TValue value;  /* the value (when closed) */
    struct {  /* double linked list (when open) */
      struct lua_UpVal *prev;
      struct lua_UpVal *next;
    } l;
  } u;
} lua_UpVal;


/*
** Closures
*/

#define lua_ClosureHeader \
	lua_CommonHeader; lua_ubyte nupvalues; lua_GCObject *gclist

typedef struct lua_CClosure {
  lua_ClosureHeader;
  lua_CFunction f;
  lua_TValue upvalue[1];  /* list of upvalues */
} lua_CClosure;


typedef struct lua_LClosure {
  lua_ClosureHeader;
  struct lua_Proto *p;
  lua_UpVal *upvals[1];  /* list of upvalues */
} lua_LClosure;


typedef union lua_Closure {
  lua_CClosure c;
  lua_LClosure l;
} lua_Closure;


#define lua_isLfunction(o)	lua_ttisLclosure(o)

#define lua_getproto(o)	(lua_clLvalue(o)->p)


/*
** Tables
*/

typedef union lua_TKey {
  struct {
    lua_TValuefields;
    struct lua_Node *next;  /* for chaining */
  } nk;
  lua_TValue tvk;
} lua_TKey;


typedef struct lua_Node {
  lua_TValue i_val;
  lua_TKey i_key;
} lua_Node;


typedef struct lua_Table {
  lua_CommonHeader;
  lua_ubyte flags;  /* 1<<p means tagmethod(p) is not present */
  lua_ubyte lsizenode;  /* log2 of size of `node' array */
  struct lua_Table *metatable;
  lua_TValue *array;  /* array part */
  lua_Node *node;
  lua_Node *lastfree;  /* any free position is before this position */
  lua_GCObject *gclist;
  int sizearray;  /* size of `array' array */
} lua_Table;



/*
** `module' operation for hashing (size is always a power of 2)
*/
#define lua_lmod(s,size) \
	(lua_check_exp((size&(size-1))==0, (lua_cast(int, (s) & ((size)-1)))))


#define lua_twoto(x)	(1<<(x))
#define lua_sizenode(t)	(lua_twoto((t)->lsizenode))


/*
** (address of) a fixed nil value
*/
#define luaO_nilobject		(&luaO_nilobject_)


LUAI_DDEC const lua_TValue luaO_nilobject_;


LUAI_FUNC int luaO_int2fb (unsigned int x);
LUAI_FUNC int luaO_fb2int (int x);
LUAI_FUNC int luaO_ceillog2 (unsigned int x);
LUAI_FUNC lua_Number luaO_arith (int op, lua_Number v1, lua_Number v2);
LUAI_FUNC void luaO_arithvec2 (int op, float *o, const float *a, const float *b);
LUAI_FUNC int luaO_str2d (const char *s, size_t len, lua_Number *result);
LUAI_FUNC int luaO_hexavalue (int c);
LUAI_FUNC const char *luaO_pushvfstring (lua_State *L, const char *fmt,
                                                       va_list argp);
LUAI_FUNC const char *luaO_pushfstring (lua_State *L, const char *fmt, ...);
LUAI_FUNC void luaO_chunkid (char *out, const char *source, size_t len);


#endif

