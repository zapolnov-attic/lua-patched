/*
** $Id: ltable.h,v 2.16.1.2 2013/08/30 15:49:41 roberto Exp $
** Lua tables (hash)
** See Copyright Notice in lua.h
*/

#ifndef ltable_h
#define ltable_h

#include "lobject.h"


#define lua_gnode(t,i)	(&(t)->node[i])
#define lua_gkey(n)		(&(n)->i_key.tvk)
#define lua_gval(n)		(&(n)->i_val)
#define lua_gnext(n)	((n)->i_key.nk.next)

#define lua_invalidateTMcache(t)	((t)->flags = 0)

/* returns the key, given the value of a table entry */
#define lua_keyfromval(v) \
  (lua_gkey(lua_cast(lua_Node *, lua_cast(char *, (v)) - offsetof(lua_Node, i_val))))


LUAI_FUNC const lua_TValue *luaH_getint (lua_Table *t, int key);
LUAI_FUNC void luaH_setint (lua_State *L, lua_Table *t, int key, lua_TValue *value);
LUAI_FUNC const lua_TValue *luaH_getstr (lua_Table *t, lua_TString *key);
LUAI_FUNC const lua_TValue *luaH_get (lua_Table *t, const lua_TValue *key);
LUAI_FUNC lua_TValue *luaH_newkey (lua_State *L, lua_Table *t, const lua_TValue *key);
LUAI_FUNC lua_TValue *luaH_set (lua_State *L, lua_Table *t, const lua_TValue *key);
LUAI_FUNC lua_Table *luaH_new (lua_State *L);
LUAI_FUNC void luaH_resize (lua_State *L, lua_Table *t, int nasize, int nhsize);
LUAI_FUNC void luaH_resizearray (lua_State *L, lua_Table *t, int nasize);
LUAI_FUNC void luaH_free (lua_State *L, lua_Table *t);
LUAI_FUNC int luaH_next (lua_State *L, lua_Table *t, lua_StkId key);
LUAI_FUNC int luaH_getn (lua_Table *t);


#if defined(LUA_DEBUG)
LUAI_FUNC lua_Node *luaH_mainposition (const lua_Table *t, const lua_TValue *key);
LUAI_FUNC int luaH_isdummy (lua_Node *n);
#endif


#endif
