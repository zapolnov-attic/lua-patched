/*
** $Id: llex.c,v 2.63.1.2 2013/08/30 15:49:41 roberto Exp $
** Lexical Analyzer
** See Copyright Notice in lua.h
*/


#include <locale.h>
#include <string.h>

#define llex_c
#define LUA_CORE

#include "lua.h"

#include "lctype.h"
#include "ldo.h"
#include "llex.h"
#include "lobject.h"
#include "lparser.h"
#include "lstate.h"
#include "lstring.h"
#include "ltable.h"
#include "lzio.h"



#define next(ls) (++ls->currentoff, ls->current = lua_zgetc(ls->z))



#define currIsNewline(ls)	(ls->current == '\n' || ls->current == '\r')


/* ORDER RESERVED */
static const char *const luaX_tokens [] = {
    "and", "break", "do", "else", "elseif",
    "end", "false", "for", "function", "goto", "if",
    "in", "local", "nil", "not", "or", "repeat",
    "vec2",
    "return", "then", "true", "until", "while",
    "<malformed-number>", "<malformed-string>", "<unfinished-string>", "<unfinished-comment>",
    "<invalid-long-string-delimiter>", "<comment>",
    "..", "...", "==", ">=", "<=", "~=", "::", "<eof>",
    "<number>", "<name>", "<string>"
};


#define save_and_next(ls) (save(ls, ls->current), next(ls))


static lua_noret lexerror (lua_LexState *ls, const char *msg, int token);


static void save (lua_LexState *ls, int c) {
  lua_Mbuffer *b = ls->buff;
  if (luaZ_bufflen(b) + 1 > luaZ_sizebuffer(b)) {
    size_t newsize;
    if (luaZ_sizebuffer(b) >= LUA_MAX_SIZET/2)
      lexerror(ls, "lexical element too long", 0);
    newsize = luaZ_sizebuffer(b) * 2;
    luaZ_resizebuffer(ls->L, b, newsize);
  }
  b->buffer[luaZ_bufflen(b)++] = lua_cast(char, c);
}


void luaX_init (lua_State *L) {
  int i;
  for (i=0; i<LUA_NUM_RESERVED; i++) {
    lua_TString *ts = luaS_new(L, luaX_tokens[i]);
    luaS_fix(ts);  /* reserved words are never collected */
    ts->tsv.extra = lua_cast_byte(i+1);  /* reserved word */
  }
}


const char *luaX_token2str (lua_LexState *ls, int token) {
  if (token < LUA_FIRST_RESERVED) {  /* single-byte symbols? */
    lua_assert(token == lua_cast(unsigned char, token));
    return (lisprint(token)) ? luaO_pushfstring(ls->L, LUA_QL("%c"), token) :
                              luaO_pushfstring(ls->L, "char(%d)", token);
  }
  else {
    const char *s = luaX_tokens[token - LUA_FIRST_RESERVED];
    if (token < LUA_TK_EOS)  /* fixed format (symbols and reserved words)? */
      return luaO_pushfstring(ls->L, LUA_QS, s);
    else  /* names, strings, and numerals */
      return s;
  }
}


static const char *txtToken (lua_LexState *ls, int token) {
  switch (token) {
    case LUA_TK_NAME:
    case LUA_TK_STRING:
    case LUA_TK_NUMBER:
      save(ls, '\0');
      return luaO_pushfstring(ls->L, LUA_QS, luaZ_buffer(ls->buff));
    default:
      return luaX_token2str(ls, token);
  }
}


static lua_noret lexerror (lua_LexState *ls, const char *msg, int token) {
  char buff[LUA_IDSIZE];
  luaO_chunkid(buff, (ls->source ? lua_getstr(ls->source) : ""), LUA_IDSIZE);
  msg = luaO_pushfstring(ls->L, "%s:%d: %s", buff, ls->linenumber, msg);
  if (token)
    luaO_pushfstring(ls->L, "%s near %s", msg, txtToken(ls, token));
  luaD_throw(ls->L, LUA_ERRSYNTAX);
}


lua_noret luaX_syntaxerror (lua_LexState *ls, const char *msg) {
  lexerror(ls, msg, ls->t.token);
}


/*
** creates a new string and anchors it in function's table so that
** it will not be collected until the end of the function's compilation
** (by that time it should be anchored in function's prototype)
*/
lua_TString *luaX_newstring (lua_LexState *ls, const char *str, size_t l) {
  lua_State *L = ls->L;
  lua_TValue *o;  /* entry for `str' */
  lua_TString *ts = luaS_newlstr(L, str, l);  /* create new string */
  lua_setsvalue2s(L, L->top++, ts);  /* temporarily anchor it in stack */
  o = luaH_set(L, ls->fs->h, L->top - 1);
  if (lua_ttisnil(o)) {  /* not in use yet? (see 'addK') */
    /* boolean value does not need GC barrier;
       table has no metatable, so it does not need to invalidate cache */
    lua_setbvalue(o, 1);  /* t[string] = true */
    luaC_checkGC(L);
  }
  else {  /* string already present */
    ts = lua_rawtsvalue(lua_keyfromval(o));  /* re-use value previously stored */
  }
  L->top--;  /* remove string from stack */
  return ts;
}


/*
** increment line number and skips newline sequence (any of
** \n, \r, \n\r, or \r\n)
*/
static void inclinenumber (lua_LexState *ls) {
  int old = ls->current;
  lua_assert(currIsNewline(ls));
  next(ls);  /* skip `\n' or `\r' */
  if (currIsNewline(ls) && ls->current != old)
    next(ls);  /* skip `\n\r' or `\r\n' */
  if (++ls->linenumber >= LUA_MAX_INT)
    luaX_syntaxerror(ls, "chunk has too many lines");
}


void luaX_setinput (lua_State *L, lua_LexState *ls, lua_ZIO *z, lua_TString *source,
                    int firstchar) {
  ls->decpoint = '.';
  ls->L = L;
  ls->current = firstchar;
  ls->currentoff = 0;
  ls->custom_lexer = 0;
  ls->lookahead.token = LUA_TK_EOS;  /* no look-ahead token */
  ls->z = z;
  ls->fs = NULL;
  ls->linenumber = 1;
  ls->lastline = 1;
  ls->sep = 0;
  ls->in_long_string = 0;
  ls->in_long_comment = 0;
  ls->source = source;
  ls->envn = luaS_new(L, LUA_ENV);  /* create env name */
  luaS_fix(ls->envn);  /* never collect this name */
  luaZ_resizebuffer(ls->L, ls->buff, LUA_MINBUFFER);  /* initialize buffer */
}



/*
** =======================================================
** LEXICAL ANALYZER
** =======================================================
*/



static int check_next (lua_LexState *ls, const char *set) {
  if (ls->current == '\0' || !strchr(set, ls->current))
    return 0;
  save_and_next(ls);
  return 1;
}


/*
** change all characters 'from' in buffer to 'to'
*/
static void buffreplace (lua_LexState *ls, char from, char to) {
  size_t n = luaZ_bufflen(ls->buff);
  char *p = luaZ_buffer(ls->buff);
  while (n--)
    if (p[n] == from) p[n] = to;
}


#if !defined(getlocaledecpoint)
#ifdef __ANDROID__
#define getlocaledecpoint()	'.'
#else
#define getlocaledecpoint()	(localeconv()->decimal_point[0])
#endif
#endif


#define buff2d(b,e)	luaO_str2d(luaZ_buffer(b), luaZ_bufflen(b) - 1, e)

/*
** in case of format error, try to change decimal point separator to
** the one defined in the current locale and check again
*/
static int trydecpoint (lua_LexState *ls, lua_SemInfo *seminfo) {
  char old = ls->decpoint;
  ls->decpoint = getlocaledecpoint();
  buffreplace(ls, old, ls->decpoint);  /* try new decimal separator */
  if (!buff2d(ls->buff, &seminfo->r)) {
    /* format error with correct decimal point: no more options */
    buffreplace(ls, ls->decpoint, '.');  /* undo change (for error message) */
    if (ls->custom_lexer)
      return 0;
    lexerror(ls, "malformed number", LUA_TK_NUMBER);
  }
  return 1;
}


/* LUA_NUMBER */
/*
** this function is quite liberal in what it accepts, as 'luaO_str2d'
** will reject ill-formed numerals.
*/
static int read_numeral (lua_LexState *ls, lua_SemInfo *seminfo) {
  const char *expo = "Ee";
  int first = ls->current;
  lua_assert(lisdigit(ls->current));
  save_and_next(ls);
  if (first == '0' && check_next(ls, "Xx"))  /* hexadecimal? */
    expo = "Pp";
  for (;;) {
    if (check_next(ls, expo))  /* exponent part? */
      check_next(ls, "+-");  /* optional exponent sign */
    if (lisxdigit(ls->current) || ls->current == '.')
      save_and_next(ls);
    else  break;
  }
  save(ls, '\0');
  buffreplace(ls, '.', ls->decpoint);  /* follow locale for decimal point */
  if (!buff2d(ls->buff, &seminfo->r))  /* format error? */
    if (!trydecpoint(ls, seminfo)) /* try to update decimal point separator */
      return LUA_TK_MALFORMED_NUMBER;
  return LUA_TK_NUMBER;
}


/*
** skip a sequence '[=*[' or ']=*]' and return its number of '='s or
** -1 if sequence is malformed
*/
static int skip_sep (lua_LexState *ls) {
  int count = 0;
  int s = ls->current;
  size_t start = ls->currentoff;
  lua_assert(s == '[' || s == ']');
  save_and_next(ls);
  while (ls->current == '=') {
    save_and_next(ls);
    count++;
  }
  if (count > 255)
  {
    if (ls->custom_lexer)
    {
      (ls->highlight_error)(ls, start, ls->currentoff, ' ');
      return -1;
    }
    lexerror(ls, "sequence is too long", LUA_TK_COMMENT);
  }
  return (ls->current == s) ? count : (-count) - 1;
}


static int read_long_string (lua_LexState *ls, lua_SemInfo *seminfo, int sep) {
  if (sep < 0) { save(ls, ls->current); sep = -sep - 1; goto do_cont; }
  save_and_next(ls);  /* skip 2nd `[' */
  if (currIsNewline(ls))  /* string starts with a newline? */
    inclinenumber(ls);  /* skip it */
  do_cont: ls->sep = sep;
  if (seminfo) ls->in_long_string = 1; else ls->in_long_comment = 1;
  for (;;) {
    switch (ls->current) {
      case LUA_EOZ:
        if (ls->custom_lexer)
          return (seminfo ? LUA_TK_UNFINISHED_STRING : LUA_TK_UNFINISHED_COMMENT);
        lexerror(ls, (seminfo) ? "unfinished long string" :
                                 "unfinished long comment", LUA_TK_EOS);
        break;  /* to avoid warnings */
      case ']': {
        if (skip_sep(ls) == sep) {
          save_and_next(ls);  /* skip 2nd `]' */
          goto endloop;
        }
        break;
      }
      case '\n': case '\r': {
        save(ls, '\n');
        inclinenumber(ls);
        if (!seminfo) luaZ_resetbuffer(ls->buff);  /* avoid wasting space */
        break;
      }
      default: {
        if (seminfo) save_and_next(ls);
        else next(ls);
      }
    }
  } endloop:
  if (seminfo) ls->in_long_string = 0; else ls->in_long_comment = 0;
  if (ls->custom_lexer) goto ret;
  if (seminfo)
    seminfo->ts = luaX_newstring(ls, luaZ_buffer(ls->buff) + (2 + sep),
                                     luaZ_bufflen(ls->buff) - 2*(2 + sep));
  ret: return (seminfo ? LUA_TK_STRING : LUA_TK_COMMENT);
}


static void escerror (lua_LexState *ls, int *c, int n, const char *msg) {
  int i;
  luaZ_resetbuffer(ls->buff);  /* prepare error message */
  save(ls, '\\');
  for (i = 0; i < n && c[i] != LUA_EOZ; i++)
    save(ls, c[i]);
  lexerror(ls, msg, LUA_TK_STRING);
}


static int readhexaesc (lua_LexState *ls) {
  int c[3], i;  /* keep input for error message */
  int r = 0;  /* result accumulator */
  size_t start = ls->currentoff;
  c[0] = 'x';  /* for error message */
  for (i = 1; i < 3; i++) {  /* read two hexadecimal digits */
    c[i] = next(ls);
    if (!lisxdigit(c[i]))
    {
      if (ls->custom_lexer)
      {
        (ls->highlight_error)(ls, start, ls->currentoff, LUA_TK_STRING);
        return -1;
      }
      escerror(ls, c, i + 1, "hexadecimal digit expected");
    }
    r = (r << 4) + luaO_hexavalue(c[i]);
  }
  return r;
}


static int readdecesc (lua_LexState *ls) {
  int c[3], i;
  int r = 0;  /* result accumulator */
  size_t start = ls->currentoff;
  for (i = 0; i < 3 && lisdigit(ls->current); i++) {  /* read up to 3 digits */
    c[i] = ls->current;
    r = 10*r + c[i] - '0';
    next(ls);
  }
  if (r > UCHAR_MAX)
  {
    if (ls->custom_lexer)
    {
      (ls->highlight_error)(ls, start, ls->currentoff, LUA_TK_STRING);
      return '?';
    }
    escerror(ls, c, i, "decimal escape too large");
  }
  return r;
}


static int read_string (lua_LexState *ls, int del, lua_SemInfo *seminfo) {
  int rs = LUA_TK_STRING;
  save_and_next(ls);  /* keep delimiter (for error messages) */
  while (ls->current != del) {
    switch (ls->current) {
      case LUA_EOZ:
        if (ls->custom_lexer) return LUA_TK_MALFORMED_STRING;
        lexerror(ls, "unfinished string", LUA_TK_EOS);
        break;  /* to avoid warnings */
      case '\n':
      case '\r':
        if (ls->custom_lexer) return LUA_TK_MALFORMED_STRING;
        lexerror(ls, "unfinished string", LUA_TK_STRING);
        break;  /* to avoid warnings */
      case '\\': {  /* escape sequences */
        int c;  /* final character to be saved */
        next(ls);  /* do not save the `\' */
        switch (ls->current) {
          case 'a': c = '\a'; goto read_save;
          case 'b': c = '\b'; goto read_save;
          case 'f': c = '\f'; goto read_save;
          case 'n': c = '\n'; goto read_save;
          case 'r': c = '\r'; goto read_save;
          case 't': c = '\t'; goto read_save;
          case 'v': c = '\v'; goto read_save;
          case 'x': c = readhexaesc(ls); if (c < 0) { c = '?'; goto only_save; } goto read_save;
          case '\n': case '\r':
            inclinenumber(ls); c = '\n'; goto only_save;
          case '\\': case '\"': case '\'':
            c = ls->current; goto read_save;
          case LUA_EOZ: goto no_save;  /* will raise an error next loop */
          case 'z': {  /* zap following span of spaces */
            next(ls);  /* skip the 'z' */
            while (lisspace(ls->current)) {
              if (currIsNewline(ls)) inclinenumber(ls);
              else next(ls);
            }
            goto no_save;
          }
          default: {
            if (!lisdigit(ls->current))
            {
              if (ls->custom_lexer)
              {
                (ls->highlight_error)(ls, ls->currentoff, ls->currentoff + 1, LUA_TK_STRING);
                c = '\\';
                goto only_save;
              }
              escerror(ls, &ls->current, 1, "invalid escape sequence");
            }
            /* digital escape \ddd */
            c = readdecesc(ls);
            goto only_save;
          }
        }
       read_save: next(ls);  /* read next character */
       only_save: save(ls, c);  /* save 'c' */
       no_save: break;
      }
      default:
        save_and_next(ls);
    }
  }
  save_and_next(ls);  /* skip delimiter */
  if (ls->custom_lexer) goto ret;
  seminfo->ts = luaX_newstring(ls, luaZ_buffer(ls->buff) + 1,
                                   luaZ_bufflen(ls->buff) - 2);
  ret: return rs;
}


static int llex (lua_LexState *ls, size_t *start, lua_SemInfo *seminfo) {
  luaZ_resetbuffer(ls->buff);
  if (ls->in_long_string) return read_long_string(ls, seminfo, -(ls->sep + 1));
  if (ls->in_long_comment) return read_long_string(ls, NULL, -(ls->sep + 1));
  for (;;) {
    switch (ls->current) {
      case '\n': case '\r': {  /* line breaks */
        inclinenumber(ls);
        *start = ls->currentoff;
        break;
      }
      case ' ': case '\f': case '\t': case '\v': {  /* spaces */
        next(ls);
        *start = ls->currentoff;
        break;
      }
      case '-': {  /* '-' or '--' (comment) */
        next(ls);
        if (ls->current != '-') return '-';
        /* else is a comment */
        next(ls);
        if (ls->current == '[') {  /* long comment? */
          int sep = skip_sep(ls);
          luaZ_resetbuffer(ls->buff);  /* `skip_sep' may dirty the buffer */
          if (sep >= 0) {
            int rs =
            read_long_string(ls, NULL, sep);  /* skip long comment */
            luaZ_resetbuffer(ls->buff);  /* previous call may dirty the buff. */
            if (ls->custom_lexer)
              return rs;
            break;
          }
        }
        /* else short comment */
        while (!currIsNewline(ls) && ls->current != LUA_EOZ)
          next(ls);  /* skip until end of line (or end of file) */
        if (ls->custom_lexer)
          return LUA_TK_COMMENT;
        break;
      }
      case '[': {  /* long string or simply '[' */
        int sep = skip_sep(ls);
        if (sep >= 0) {
          return read_long_string(ls, seminfo, sep);
        }
        else if (sep == -1) return '[';
        else if (ls->custom_lexer) return LUA_TK_INVALID_LONG_STRING_DELIMITER;
        else lexerror(ls, "invalid long string delimiter", LUA_TK_STRING);
      }
      case '=': {
        next(ls);
        if (ls->current != '=') return '=';
        else { next(ls); return LUA_TK_EQ; }
      }
      case '<': {
        next(ls);
        if (ls->current != '=') return '<';
        else { next(ls); return LUA_TK_LE; }
      }
      case '>': {
        next(ls);
        if (ls->current != '=') return '>';
        else { next(ls); return LUA_TK_GE; }
      }
      case '~': {
        next(ls);
        if (ls->current != '=') return '~';
        else { next(ls); return LUA_TK_NE; }
      }
      case ':': {
        next(ls);
        if (ls->current != ':') return ':';
        else { next(ls); return LUA_TK_DBCOLON; }
      }
      case '"': case '\'': {  /* short literal strings */
        return read_string(ls, ls->current, seminfo);
      }
      case '.': {  /* '.', '..', '...', or number */
        save_and_next(ls);
        if (check_next(ls, ".")) {
          if (check_next(ls, "."))
            return LUA_TK_DOTS;   /* '...' */
          else return LUA_TK_CONCAT;   /* '..' */
        }
        else if (!lisdigit(ls->current)) return '.';
        /* else go through */
      }
      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9': {
        return read_numeral(ls, seminfo);
      }
      case LUA_EOZ: {
        return LUA_TK_EOS;
      }
      default: {
        if (lislalpha(ls->current)) {  /* identifier or reserved word? */
          lua_TString *ts;
          do {
            save_and_next(ls);
          } while (lislalnum(ls->current));
          ts = luaX_newstring(ls, luaZ_buffer(ls->buff),
                                  luaZ_bufflen(ls->buff));
          seminfo->ts = ts;
          if (lua_isreserved(ts))  /* reserved word? */
            return ts->tsv.extra - 1 + LUA_FIRST_RESERVED;
          else {
            return LUA_TK_NAME;
          }
        }
        else {  /* single-char tokens (+ - / ...) */
          int c = ls->current;
          next(ls);
          return c;
        }
      }
    }
  }
}


void luaX_next (lua_LexState *ls) {
  ls->lastline = ls->linenumber;
  if (ls->lookahead.token != LUA_TK_EOS) {  /* is there a look-ahead token? */
    ls->t = ls->lookahead;  /* use this one */
    ls->lookahead.token = LUA_TK_EOS;  /* and discharge it */
  }
  else
  {
    ls->t.start = ls->currentoff;
    ls->t.token = llex(ls, &ls->t.start, &ls->t.seminfo);  /* read next token */
    ls->t.end = ls->currentoff;
  }
}


int luaX_lookahead (lua_LexState *ls) {
  lua_assert(ls->lookahead.token == LUA_TK_EOS);
  ls->lookahead.start = ls->currentoff;
  ls->lookahead.token = llex(ls, &ls->t.start, &ls->lookahead.seminfo);
  ls->lookahead.end = ls->currentoff;
  return ls->lookahead.token;
}

