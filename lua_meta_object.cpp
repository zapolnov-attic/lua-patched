/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "lua_meta_object.hpp"
#include "lua_rtti.hpp"
#include "lua_stack_entry.hpp"
#include "lua_stack_top.hpp"
#include RTTI_PROPERTY_NOT_WRITABLE_H
#include RTTI_TYPE_CONVERSION_H

static void pushArguments(Lua::VM & vm, Lua::Object * self, size_t n, const RTTI::ConstReference * args)
{
	lua_State * const L = vm.L;

	luaL_checkstack(L, static_cast<int>(n + 1), "pushArguments");
	if (self)
		vm.pushTable(self->table());
	for (size_t i = 0; i < n; i++)
	{
		lua_setnilvalue(vm.L->top);
		++vm.L->top;
		Lua::StackEntry entry(vm, vm.L->top - 1);
		RTTI::convert(entry, args[i].pointer(), args[i].type());
	}
}

/* MetaObject */

Lua::MetaObject::MetaObject(const std::string & name)
	: RTTI::TypeInfo(name, sizeof(Lua::Object)),
	  RTTI::MetaObject(getThis(),
		RTTI::Internal::META_PARENT_CLASS, Lua::ObjectBase::staticMetaObject(),
		RTTI::Internal::META_END)
{
}

Lua::MetaObject::~MetaObject()
{
}

/* Constructor */

Lua::Constructor::Constructor(Lua::VM & vm, const Lua::MetaObject * metaObj, const Lua::Value & function)
	: RTTI::Constructor(0),
	  m_VM(vm),
	  m_MetaObject(metaObj),
	  m_Function(function)
{
}

RTTI::Object * Lua::Constructor::invokeDynamic(size_t n, const RTTI::ConstReference * args) const
{
	Lua::Object * obj = new Lua::Object(m_VM, m_MetaObject);

	try {
		m_VM.push(m_Function);
		pushArguments(m_VM, obj, n, args);
		m_VM.call(static_cast<int>(n) + 1, 0);
	} catch (...) {
		delete obj;
		throw;
	}

	return obj;
}

RTTI::Object * Lua::Constructor::pinvokeDynamic(void * ptr, size_t n, const RTTI::ConstReference * args) const
{
	Lua::Object * obj = new (ptr) Lua::Object(m_VM, m_MetaObject);

	m_VM.push(m_Function);
	pushArguments(m_VM, obj, n, args);
	m_VM.call(static_cast<int>(n) + 1, 0);

	return obj;
}

/* Method */

Lua::Method::Method(Lua::VM & vm, const char * nm, const Lua::Value & function)
	: RTTI::Method(nm, RTTI::typeOf<Lua::Value>(), 0),
	  m_VM(vm),
	  m_Function(function)
{
}

void Lua::Method::invokeDynamic(RTTI::Object * object, const RTTI::Reference & out,
			size_t n, const RTTI::ConstReference * args) const
{
	Lua::Object * obj = NULL;
	if (object->instanceOf(Lua::Object::staticMetaObject()))
		obj = static_cast<Lua::Object *>(object);

	m_VM.push(m_Function);
	if (!obj)
		Lua::pushRttiObjectRawPtr(m_VM, object);
	pushArguments(m_VM, obj, n, args);

	m_VM.call(static_cast<int>(n) + 1, 1);

	out.setValue(Lua::StackEntry(m_VM, m_VM.L->top - 1));
	--m_VM.L->top;
}

/* Property */

Lua::Property::Property(Lua::VM & vm, const char * nm, const Lua::Value & getter, const Lua::Value & setter)
	: RTTI::Property(nm, RTTI::typeOf<Lua::Value>()),
	  m_VM(vm),
	  m_Getter(getter),
	  m_Setter(setter)
{
}

void Lua::Property::getValue(const RTTI::Object * object, void * output, RTTI::Type outputType) const
{
	m_VM.push(m_Getter);

	if (!object->instanceOf(Lua::Object::staticMetaObject()))
		Lua::pushRttiObjectRawPtr(m_VM, const_cast<RTTI::Object *>(object));
	else
		m_VM.pushTable(static_cast<const Lua::Object *>(object)->table());

	m_VM.call(1, 1);

	RTTI::convert(output, outputType, Lua::StackEntry(m_VM, m_VM.L->top - 1));
	--m_VM.L->top;
}

void Lua::Property::setValue(RTTI::Object * object, const void * ptr, RTTI::Type valueType) const
{
	if (m_Setter.type() <= 0)
		throw RTTI::PropertyNotWritable(metaObject()->name(), name());

	m_VM.push(m_Setter);

	if (!object->instanceOf(Lua::Object::staticMetaObject()))
		Lua::pushRttiObjectRawPtr(m_VM, object);
	else
		m_VM.pushTable(static_cast<Lua::Object *>(object)->table());

	lua_setnilvalue(m_VM.L->top);
	++m_VM.L->top;
	Lua::StackEntry entry(m_VM, m_VM.L->top - 1);
	RTTI::convert(entry, ptr, valueType);

	m_VM.call(2, 0);
}

/* Object */

Lua::Object::Object(Lua::VM & vmref, const Lua::MetaObject * metaObj)
	: m_VM(vmref),
	  m_MetaObject(metaObj)
{
	m_Table = m_VM.pushNewTable();
	m_VM.rawSetP(m_VM.registry(), m_Table);
}

Lua::Object::~Object()
{
	Lua::StackTop top(m_VM);

	m_VM.push(m_MetaObject->m_Destructor);
	if (lua_isfunction(m_VM.L, -1))
	{
		m_VM.pushTable(m_Table);
		m_VM.pcall(1, 0);
	}

	lua_pushnil(m_VM.L);
	m_VM.rawSetP(m_VM.registry(), m_Table);
}
