/*
** $Id: lstring.h,v 1.49.1.1 2013/04/12 18:48:47 roberto Exp $
** String table (keep all strings handled by Lua)
** See Copyright Notice in lua.h
*/

#ifndef lstring_h
#define lstring_h

#include "lgc.h"
#include "lobject.h"
#include "lstate.h"


#define lua_sizestring(s)	(sizeof(union lua_TString)+((s)->len+1)*sizeof(char))

#define lua_sizeudata(u)	(sizeof(union lua_Udata)+(u)->len)
#define lua_sizeuptrdata(u)	(sizeof(union lua_Udata)+(u)->len)

#define luaS_newliteral(L, s)	(luaS_newlstr(L, "" s, \
                                 (sizeof(s)/sizeof(char))-1))

#define luaS_fix(s)	lua_setbit((s)->tsv.marked, LUA_FIXEDBIT)


/*
** test whether a string is a reserved word
*/
#define lua_isreserved(s)	((s)->tsv.tt == LUA_TSHRSTR && (s)->tsv.extra > 0)


/*
** equality for short strings, which are always internalized
*/
#define lua_eqshrstr(a,b)	lua_check_exp((a)->tsv.tt == LUA_TSHRSTR, (a) == (b))


LUAI_FUNC unsigned int luaS_hash (const char *str, size_t l, unsigned int seed);
LUAI_FUNC int luaS_eqlngstr (lua_TString *a, lua_TString *b);
LUAI_FUNC int luaS_eqstr (lua_TString *a, lua_TString *b);
LUAI_FUNC void luaS_resize (lua_State *L, int newsize);
LUAI_FUNC lua_Udata *luaS_newudata (lua_State *L, size_t s, lua_Table *e);
LUAI_FUNC lua_Udata *luaS_newuserpointer (lua_State *L, lua_Table *e);
LUAI_FUNC lua_TString *luaS_newlstr (lua_State *L, const char *str, size_t l);
LUAI_FUNC lua_TString *luaS_new (lua_State *L, const char *str);


#endif
