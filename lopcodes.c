/*
** $Id: lopcodes.c,v 1.49.1.1 2013/04/12 18:48:47 roberto Exp $
** Opcodes for Lua virtual machine
** See Copyright Notice in lua.h
*/


#define lopcodes_c
#define LUA_CORE


#include "lopcodes.h"


/* ORDER OP */

LUAI_DDEF const char *const luaP_opnames[LUA_NUM_OPCODES+1] = {
  "MOVE",
  "LOADK",
  "LOADKX",
  "LOADBOOL",
  "LOADNIL",
  "GETUPVAL",
  "GETTABUP",
  "GETTABLE",
  "SETTABUP",
  "SETUPVAL",
  "SETTABLE",
  "NEWTABLE",
  "SELF",
  "VEC2",
  "ADD",
  "SUB",
  "MUL",
  "DIV",
  "MOD",
  "POW",
  "UNM",
  "NOT",
  "LEN",
  "CONCAT",
  "JMP",
  "EQ",
  "LT",
  "LE",
  "TEST",
  "TESTSET",
  "CALL",
  "TAILCALL",
  "RETURN",
  "FORLOOP",
  "FORPREP",
  "TFORCALL",
  "TFORLOOP",
  "SETLIST",
  "CLOSURE",
  "VARARG",
  "EXTRAARG",
  NULL
};


#define opmode(t,a,b,c,m) (((t)<<7) | ((a)<<6) | ((b)<<4) | ((c)<<2) | (m))

LUAI_DDEF const lua_ubyte luaP_opmodes[LUA_NUM_OPCODES] = {
/*       T  A    B       C     mode		   opcode	*/
  opmode(0, 1, lua_OpArgR, lua_OpArgN, lua_iABC)		/* LUA_OP_MOVE */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgN, lua_iABx)		/* LUA_OP_LOADK */
 ,opmode(0, 1, lua_OpArgN, lua_OpArgN, lua_iABx)		/* LUA_OP_LOADKX */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgU, lua_iABC)		/* LUA_OP_LOADBOOL */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgN, lua_iABC)		/* LUA_OP_LOADNIL */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgN, lua_iABC)		/* LUA_OP_GETUPVAL */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgK, lua_iABC)		/* LUA_OP_GETTABUP */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgK, lua_iABC)		/* LUA_OP_GETTABLE */
 ,opmode(0, 0, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_SETTABUP */
 ,opmode(0, 0, lua_OpArgU, lua_OpArgN, lua_iABC)		/* LUA_OP_SETUPVAL */
 ,opmode(0, 0, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_SETTABLE */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgU, lua_iABC)		/* LUA_OP_NEWTABLE */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgK, lua_iABC)		/* LUA_OP_SELF */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_VEC2 */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_ADD */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_SUB */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_MUL */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_DIV */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_MOD */
 ,opmode(0, 1, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_POW */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgN, lua_iABC)		/* LUA_OP_UNM */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgN, lua_iABC)		/* LUA_OP_NOT */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgN, lua_iABC)		/* LUA_OP_LEN */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgR, lua_iABC)		/* LUA_OP_CONCAT */
 ,opmode(0, 0, lua_OpArgR, lua_OpArgN, lua_iAsBx)		/* LUA_OP_JMP */
 ,opmode(1, 0, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_EQ */
 ,opmode(1, 0, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_LT */
 ,opmode(1, 0, lua_OpArgK, lua_OpArgK, lua_iABC)		/* LUA_OP_LE */
 ,opmode(1, 0, lua_OpArgN, lua_OpArgU, lua_iABC)		/* LUA_OP_TEST */
 ,opmode(1, 1, lua_OpArgR, lua_OpArgU, lua_iABC)		/* LUA_OP_TESTSET */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgU, lua_iABC)		/* LUA_OP_CALL */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgU, lua_iABC)		/* LUA_OP_TAILCALL */
 ,opmode(0, 0, lua_OpArgU, lua_OpArgN, lua_iABC)		/* LUA_OP_RETURN */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgN, lua_iAsBx)		/* LUA_OP_FORLOOP */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgN, lua_iAsBx)		/* LUA_OP_FORPREP */
 ,opmode(0, 0, lua_OpArgN, lua_OpArgU, lua_iABC)		/* LUA_OP_TFORCALL */
 ,opmode(0, 1, lua_OpArgR, lua_OpArgN, lua_iAsBx)		/* LUA_OP_TFORLOOP */
 ,opmode(0, 0, lua_OpArgU, lua_OpArgU, lua_iABC)		/* LUA_OP_SETLIST */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgN, lua_iABx)		/* LUA_OP_CLOSURE */
 ,opmode(0, 1, lua_OpArgU, lua_OpArgN, lua_iABC)		/* LUA_OP_VARARG */
 ,opmode(0, 0, lua_OpArgU, lua_OpArgU, lua_iAx)		/* LUA_OP_EXTRAARG */
};

