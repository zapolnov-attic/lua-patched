/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "lexer.hpp"

Lua::Lexer::Lexer()
{
	L = luaL_newstate();

	try
	{
		luaZ_initbuffer(L, &m_LexBuffer);
		m_LexState.buff = &m_LexBuffer;

		luaX_setinput(L, &m_LexState, NULL, NULL, 0);
		m_LexState.custom_lexer = this;
		m_LexState.highlight_error = errorCallback;
	}
	catch (...)
	{
		lua_close(L);
		throw;
	}
}

Lua::Lexer::~Lexer()
{
	luaZ_freebuffer(L, &m_LexBuffer);
	lua_close(L);
}

int Lua::Lexer::parse(const void * data, size_t length, int prevBlockState)
{
	int state = prevBlockState;
	int top = lua_gettop(L);

	try
	{
		lua_ZIO z;
		z.n = length;
		z.p = reinterpret_cast<const char *>(data);
		z.reader = NULL;
		z.data = NULL;
		z.L = L;

		lua_FuncState fs;
		fs.h = luaH_new(L);
		lua_sethvalue2s(L, L->top, fs.h);
		lua_api_incr_top(L);

		int sep = state & 0xFFFF;
		int is_long_string = state & 0x10000;
		int is_long_comment = state & 0x20000;

		m_LexState.current = lua_zgetc(&z);
		m_LexState.currentoff = 0;
		m_LexState.lookahead.token = LUA_TK_EOS;
		m_LexState.z = &z;
		m_LexState.fs = &fs;
		m_LexState.linenumber = 1;
		m_LexState.lastline = 1;
		m_LexState.sep = sep;
		m_LexState.in_long_string = (is_long_string ? 1 : 0);
		m_LexState.in_long_comment = (is_long_comment ? 1 : 0);

		onBeforeParse();

		do
		{
			luaX_next(&m_LexState);
			onToken(m_LexState.t.start, m_LexState.t.end - m_LexState.t.start, m_LexState.t.token);
		}
		while (m_LexState.t.token != LUA_TK_EOS &&
			   m_LexState.t.token != LUA_TK_UNFINISHED_STRING &&
			   m_LexState.t.token != LUA_TK_UNFINISHED_COMMENT);

		onAfterParse();

		state = 0;
		if (m_LexState.in_long_string || m_LexState.in_long_comment)
		{
			state |= m_LexState.sep & 0xFFFF;
			if (m_LexState.in_long_string)
				state |= 0x100;
			if (m_LexState.in_long_comment)
				state |= 0x200;
		}
	}
	catch (...)
	{
		lua_settop(L, top);
		throw;
	}

	lua_settop(L, top);

	return state;
}

void Lua::Lexer::errorCallback(lua_LexState * state, size_t start, size_t end, int baseToken)
{
	Lexer * lexer = reinterpret_cast<Lexer *>(state->custom_lexer);
	lexer->onError(start, end, baseToken);
}
