/*
** $Id: lopcodes.h,v 1.142.1.1 2013/04/12 18:48:47 roberto Exp $
** Opcodes for Lua virtual machine
** See Copyright Notice in lua.h
*/

#ifndef lopcodes_h
#define lopcodes_h

#include "llimits.h"


/*===========================================================================
  We assume that instructions are unsigned numbers.
  All instructions have an opcode in the first 6 bits.
  Instructions can have the following fields:
	`A' : 8 bits
	`B' : 9 bits
	`C' : 9 bits
	'Ax' : 26 bits ('A', 'B', and 'C' together)
	`Bx' : 18 bits (`B' and `C' together)
	`sBx' : signed Bx

  A signed argument is represented in excess K; that is, the number
  value is the unsigned value minus K. K is exactly the maximum value
  for that argument (so that -max is represented by 0, and +max is
  represented by 2*max), which is half the maximum for the corresponding
  unsigned argument.
===========================================================================*/


enum lua_OpMode {lua_iABC, lua_iABx, lua_iAsBx, lua_iAx};  /* basic instruction format */


/*
** size and position of opcode arguments.
*/
#define LUA_SIZE_C		9
#define LUA_SIZE_B		9
#define LUA_SIZE_Bx		(LUA_SIZE_C + LUA_SIZE_B)
#define LUA_SIZE_A		8
#define LUA_SIZE_Ax		(LUA_SIZE_C + LUA_SIZE_B + LUA_SIZE_A)

#define LUA_SIZE_OP		6

#define LUA_POS_OP		0
#define LUA_POS_A		(LUA_POS_OP + LUA_SIZE_OP)
#define LUA_POS_C		(LUA_POS_A + LUA_SIZE_A)
#define LUA_POS_B		(LUA_POS_C + LUA_SIZE_C)
#define LUA_POS_Bx		LUA_POS_C
#define LUA_POS_Ax		LUA_POS_A


/*
** limits for opcode arguments.
** we use (signed) int to manipulate most arguments,
** so they must fit in LUAI_BITSINT-1 bits (-1 for sign)
*/
#if LUA_SIZE_Bx < LUAI_BITSINT-1
#define LUA_MAXARG_Bx        ((1<<LUA_SIZE_Bx)-1)
#define LUA_MAXARG_sBx        (LUA_MAXARG_Bx>>1)         /* `sBx' is signed */
#else
#define LUA_MAXARG_Bx        LUA_MAX_INT
#define LUA_MAXARG_sBx        LUA_MAX_INT
#endif

#if LUA_SIZE_Ax < LUAI_BITSINT-1
#define LUA_MAXARG_Ax	((1<<LUA_SIZE_Ax)-1)
#else
#define LUA_MAXARG_Ax	LUA_MAX_INT
#endif


#define LUA_MAXARG_A        ((1<<LUA_SIZE_A)-1)
#define LUA_MAXARG_B        ((1<<LUA_SIZE_B)-1)
#define LUA_MAXARG_C        ((1<<LUA_SIZE_C)-1)


/* creates a mask with `n' 1 bits at position `p' */
#define LUA_MASK1(n,p)	((~((~(lua_Instruction)0)<<(n)))<<(p))

/* creates a mask with `n' 0 bits at position `p' */
#define LUA_MASK0(n,p)	(~LUA_MASK1(n,p))

/*
** the following macros help to manipulate instructions
*/

#define LUA_GET_OPCODE(i)	(lua_cast(lua_OpCode, ((i)>>LUA_POS_OP) & LUA_MASK1(LUA_SIZE_OP,0)))
#define LUA_SET_OPCODE(i,o)	((i) = (((i)&LUA_MASK0(LUA_SIZE_OP,LUA_POS_OP)) | \
		((lua_cast(lua_Instruction, o)<<LUA_POS_OP)&LUA_MASK1(LUA_SIZE_OP,LUA_POS_OP))))

#define lua_getarg(i,pos,size)	(lua_cast(int, ((i)>>pos) & LUA_MASK1(size,0)))
#define lua_setarg(i,v,pos,size)	((i) = (((i)&LUA_MASK0(size,pos)) | \
                ((lua_cast(lua_Instruction, v)<<pos)&LUA_MASK1(size,pos))))

#define LUA_GETARG_A(i)	lua_getarg(i, LUA_POS_A, LUA_SIZE_A)
#define LUA_SETARG_A(i,v)	lua_setarg(i, v, LUA_POS_A, LUA_SIZE_A)

#define LUA_GETARG_B(i)	lua_getarg(i, LUA_POS_B, LUA_SIZE_B)
#define LUA_SETARG_B(i,v)	lua_setarg(i, v, LUA_POS_B, LUA_SIZE_B)

#define LUA_GETARG_C(i)	lua_getarg(i, LUA_POS_C, LUA_SIZE_C)
#define LUA_SETARG_C(i,v)	lua_setarg(i, v, LUA_POS_C, LUA_SIZE_C)

#define LUA_GETARG_Bx(i)	lua_getarg(i, LUA_POS_Bx, LUA_SIZE_Bx)
#define LUA_SETARG_Bx(i,v)	lua_setarg(i, v, LUA_POS_Bx, LUA_SIZE_Bx)

#define LUA_GETARG_Ax(i)	lua_getarg(i, LUA_POS_Ax, LUA_SIZE_Ax)
#define LUA_SETARG_Ax(i,v)	lua_setarg(i, v, LUA_POS_Ax, LUA_SIZE_Ax)

#define LUA_GETARG_sBx(i)	(LUA_GETARG_Bx(i)-LUA_MAXARG_sBx)
#define LUA_SETARG_sBx(i,b)	LUA_SETARG_Bx((i),lua_cast(unsigned int, (b)+LUA_MAXARG_sBx))


#define LUA_CREATE_ABC(o,a,b,c)	((lua_cast(lua_Instruction, o)<<LUA_POS_OP) \
			| (lua_cast(lua_Instruction, a)<<LUA_POS_A) \
			| (lua_cast(lua_Instruction, b)<<LUA_POS_B) \
			| (lua_cast(lua_Instruction, c)<<LUA_POS_C))

#define LUA_CREATE_ABx(o,a,bc)	((lua_cast(lua_Instruction, o)<<LUA_POS_OP) \
			| (lua_cast(lua_Instruction, a)<<LUA_POS_A) \
			| (lua_cast(lua_Instruction, bc)<<LUA_POS_Bx))

#define LUA_CREATE_Ax(o,a)		((lua_cast(lua_Instruction, o)<<LUA_POS_OP) \
			| (lua_cast(lua_Instruction, a)<<LUA_POS_Ax))


/*
** Macros to operate RK indices
*/

/* this bit 1 means constant (0 means register) */
#define LUA_BITRK		(1 << (LUA_SIZE_B - 1))

/* test whether value is a constant */
#define LUA_ISK(x)		((x) & LUA_BITRK)

/* gets the index of the constant */
#define LUA_INDEXK(r)	((int)(r) & ~LUA_BITRK)

#define LUA_MAXINDEXRK	(LUA_BITRK - 1)

/* code a constant index as a RK value */
#define LUA_RKASK(x)	((x) | LUA_BITRK)


/*
** invalid register that fits in 8 bits
*/
#define LUA_NO_REG		LUA_MAXARG_A


/*
** R(x) - register
** Kst(x) - constant (in constant table)
** RK(x) == if LUA_ISK(x) then Kst(LUA_INDEXK(x)) else R(x)
*/


/*
** grep "ORDER OP" if you change these enums
*/

typedef enum {
/*----------------------------------------------------------------------
name		args	description
------------------------------------------------------------------------*/
LUA_OP_MOVE,/*	A B	R(A) := R(B)					*/
LUA_OP_LOADK,/*	A Bx	R(A) := Kst(Bx)					*/
LUA_OP_LOADKX,/*	A 	R(A) := Kst(extra arg)				*/
LUA_OP_LOADBOOL,/*	A B C	R(A) := (Bool)B; if (C) pc++			*/
LUA_OP_LOADNIL,/*	A B	R(A), R(A+1), ..., R(A+B) := nil		*/
LUA_OP_GETUPVAL,/*	A B	R(A) := UpValue[B]				*/

LUA_OP_GETTABUP,/*	A B C	R(A) := UpValue[B][RK(C)]			*/
LUA_OP_GETTABLE,/*	A B C	R(A) := R(B)[RK(C)]				*/

LUA_OP_SETTABUP,/*	A B C	UpValue[A][RK(B)] := RK(C)			*/
LUA_OP_SETUPVAL,/*	A B	UpValue[B] := R(A)				*/
LUA_OP_SETTABLE,/*	A B C	R(A)[RK(B)] := RK(C)				*/

LUA_OP_NEWTABLE,/*	A B C	R(A) := {} (size = B,C)				*/

LUA_OP_SELF,/*	A B C	R(A+1) := R(B); R(A) := R(B)[RK(C)]		*/

LUA_OP_VEC2,/*	A B C	R(A) := VEC2(RK(B), RK(C))				*/

LUA_OP_ADD,/*	A B C	R(A) := RK(B) + RK(C)				*/
LUA_OP_SUB,/*	A B C	R(A) := RK(B) - RK(C)				*/
LUA_OP_MUL,/*	A B C	R(A) := RK(B) * RK(C)				*/
LUA_OP_DIV,/*	A B C	R(A) := RK(B) / RK(C)				*/
LUA_OP_MOD,/*	A B C	R(A) := RK(B) % RK(C)				*/
LUA_OP_POW,/*	A B C	R(A) := RK(B) ^ RK(C)				*/
LUA_OP_UNM,/*	A B	R(A) := -R(B)					*/
LUA_OP_NOT,/*	A B	R(A) := not R(B)				*/
LUA_OP_LEN,/*	A B	R(A) := length of R(B)				*/

LUA_OP_CONCAT,/*	A B C	R(A) := R(B).. ... ..R(C)			*/

LUA_OP_JMP,/*	A sBx	pc+=sBx; if (A) close all upvalues >= R(A) + 1	*/
LUA_OP_EQ,/*	A B C	if ((RK(B) == RK(C)) ~= A) then pc++		*/
LUA_OP_LT,/*	A B C	if ((RK(B) <  RK(C)) ~= A) then pc++		*/
LUA_OP_LE,/*	A B C	if ((RK(B) <= RK(C)) ~= A) then pc++		*/

LUA_OP_TEST,/*	A C	if not (R(A) <=> C) then pc++			*/
LUA_OP_TESTSET,/*	A B C	if (R(B) <=> C) then R(A) := R(B) else pc++	*/

LUA_OP_CALL,/*	A B C	R(A), ... ,R(A+C-2) := R(A)(R(A+1), ... ,R(A+B-1)) */
LUA_OP_TAILCALL,/*	A B C	return R(A)(R(A+1), ... ,R(A+B-1))		*/
LUA_OP_RETURN,/*	A B	return R(A), ... ,R(A+B-2)	(see note)	*/

LUA_OP_FORLOOP,/*	A sBx	R(A)+=R(A+2);
			if R(A) <?= R(A+1) then { pc+=sBx; R(A+3)=R(A) }*/
LUA_OP_FORPREP,/*	A sBx	R(A)-=R(A+2); pc+=sBx				*/

LUA_OP_TFORCALL,/*	A C	R(A+3), ... ,R(A+2+C) := R(A)(R(A+1), R(A+2));	*/
LUA_OP_TFORLOOP,/*	A sBx	if R(A+1) ~= nil then { R(A)=R(A+1); pc += sBx }*/

LUA_OP_SETLIST,/*	A B C	R(A)[(C-1)*FPF+i] := R(A+i), 1 <= i <= B	*/

LUA_OP_CLOSURE,/*	A Bx	R(A) := closure(KPROTO[Bx])			*/

LUA_OP_VARARG,/*	A B	R(A), R(A+1), ..., R(A+B-2) = vararg		*/

LUA_OP_EXTRAARG/*	Ax	extra (larger) argument for previous opcode	*/
} lua_OpCode;


#define LUA_NUM_OPCODES	(lua_cast(int, LUA_OP_EXTRAARG) + 1)



/*===========================================================================
  Notes:
  (*) In LUA_OP_CALL, if (B == 0) then B = top. If (C == 0), then `top' is
  set to last_result+1, so next open instruction (LUA_OP_CALL, LUA_OP_RETURN,
  LUA_OP_SETLIST) may use `top'.

  (*) In LUA_OP_VARARG, if (B == 0) then use actual number of varargs and
  set top (like in LUA_OP_CALL with C == 0).

  (*) In LUA_OP_RETURN, if (B == 0) then return up to `top'.

  (*) In LUA_OP_SETLIST, if (B == 0) then B = `top'; if (C == 0) then next
  'instruction' is EXTRAARG(real C).

  (*) In LUA_OP_LOADKX, the next 'instruction' is always EXTRAARG.

  (*) For comparisons, A specifies what condition the test should accept
  (true or false).

  (*) All `skips' (pc++) assume that next instruction is a jump.

===========================================================================*/


/*
** masks for instruction properties. The format is:
** bits 0-1: op mode
** bits 2-3: C arg mode
** bits 4-5: B arg mode
** bit 6: instruction set register A
** bit 7: operator is a test (next instruction must be a jump)
*/

enum lua_OpArgMask {
  lua_OpArgN,  /* argument is not used */
  lua_OpArgU,  /* argument is used */
  lua_OpArgR,  /* argument is a register or a jump offset */
  lua_OpArgK   /* argument is a constant or register/constant */
};

LUAI_DDEC const lua_ubyte luaP_opmodes[LUA_NUM_OPCODES];

#define lua_getOpMode(m)	(lua_cast(enum lua_OpMode, luaP_opmodes[m] & 3))
#define lua_getBMode(m)	(lua_cast(enum lua_OpArgMask, (luaP_opmodes[m] >> 4) & 3))
#define lua_getCMode(m)	(lua_cast(enum lua_OpArgMask, (luaP_opmodes[m] >> 2) & 3))
#define lua_testAMode(m)	(luaP_opmodes[m] & (1 << 6))
#define lua_testTMode(m)	(luaP_opmodes[m] & (1 << 7))


LUAI_DDEC const char *const luaP_opnames[LUA_NUM_OPCODES+1];  /* opcode names */


/* number of list items to accumulate before a SETLIST instruction */
#define LUA_LFIELDS_PER_FLUSH	50


#endif
