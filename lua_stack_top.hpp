/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __c53d3e0f4c633bebacb8282a7271f83a__
#define __c53d3e0f4c633bebacb8282a7271f83a__

#include "lua_vm.hpp"

namespace Lua
{
	class StackTop
	{
	public:
		inline StackTop(VM & vm) : m_VM(vm) { m_Value = lua_gettop(vm.L); }
		inline ~StackTop() { lua_settop(m_VM.L, m_Value); }

		inline operator int() const { return m_Value; }

		inline StackTop & operator=(int top) { m_Value = top; return *this; }
		inline StackTop & operator+=(int top) { m_Value += top; return *this; }
		inline StackTop & operator-=(int top) { m_Value -= top; return *this; }

		inline bool operator==(int value) const { return m_Value == value; }
		inline bool operator!=(int value) const { return m_Value != value; }
		inline bool operator<(int value) const { return m_Value < value; }
		inline bool operator<=(int value) const { return m_Value <= value; }
		inline bool operator>(int value) const { return m_Value > value; }
		inline bool operator>=(int value) const { return m_Value >= value; }

	private:
		VM & m_VM;
		int m_Value;

		StackTop(const StackTop &);
		StackTop & operator=(const StackTop &);
	};
}

#endif
