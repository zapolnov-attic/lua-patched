/*
** $Id: lgc.h,v 2.58.1.1 2013/04/12 18:48:47 roberto Exp $
** Garbage Collector
** See Copyright Notice in lua.h
*/

#ifndef lgc_h
#define lgc_h


#include "lobject.h"
#include "lstate.h"

/*
** Collectable objects may have one of three colors: white, which
** means the object is not marked; gray, which means the
** object is marked, but its references may be not marked; and
** black, which means that the object and all its references are marked.
** The main invariant of the garbage collector, while marking objects,
** is that a black object can never point to a white one. Moreover,
** any gray object must be in a "gray list" (gray, grayagain, weak,
** allweak, ephemeron) so that it can be visited again before finishing
** the collection cycle. These lists have no meaning when the invariant
** is not being enforced (e.g., sweep phase).
*/



/* how much to allocate before next GC step */
#if !defined(LUA_GCSTEPSIZE)
/* ~100 small strings */
#define LUA_GCSTEPSIZE	(lua_cast_int(100 * sizeof(lua_TString)))
#endif


/*
** Possible states of the Garbage Collector
*/
#define LUA_GCSpropagate	0
#define LUA_GCSatomic	1
#define LUA_GCSsweepstring	2
#define LUA_GCSsweepudata	3
#define LUA_GCSsweep	4
#define LUA_GCSpause	5


#define lua_issweepphase(g)  \
	(LUA_GCSsweepstring <= (g)->gcstate && (g)->gcstate <= LUA_GCSsweep)

#define lua_isgenerational(g)	((g)->gckind == LUA_KGC_GEN)

/*
** macros to tell when main invariant (white objects cannot point to black
** ones) must be kept. During a non-generational collection, the sweep
** phase may break the invariant, as objects turned white may point to
** still-black objects. The invariant is restored when sweep ends and
** all objects are white again. During a generational collection, the
** invariant must be kept all times.
*/

#define lua_keepinvariant(g)	(lua_isgenerational(g) || g->gcstate <= LUA_GCSatomic)


/*
** Outside the collector, the state in generational mode is kept in
** 'propagate', so 'lua_keepinvariant' is always true.
*/
#define lua_keepinvariantout(g)  \
  lua_check_exp(g->gcstate == LUA_GCSpropagate || !lua_isgenerational(g),  \
            g->gcstate <= LUA_GCSatomic)


/*
** some useful bit tricks
*/
#define lua_resetbits(x,m)		((x) &= lua_cast(lua_ubyte, ~(m)))
#define lua_setbits(x,m)		((x) |= (m))
#define lua_testbits(x,m)		((x) & (m))
#define lua_bitmask(b)		(1<<(b))
#define lua_bit2mask(b1,b2)		(lua_bitmask(b1) | lua_bitmask(b2))
#define lua_setbit(x,b)		lua_setbits(x, lua_bitmask(b))
#define lua_resetbit(x,b)		lua_resetbits(x, lua_bitmask(b))
#define lua_testbit(x,b)		lua_testbits(x, lua_bitmask(b))


/* Layout for bit use in `marked' field: */
#define LUA_WHITE0BIT	0  /* object is white (type 0) */
#define LUA_WHITE1BIT	1  /* object is white (type 1) */
#define LUA_BLACKBIT	2  /* object is black */
#define LUA_FINALIZEDBIT	3  /* object has been separated for finalization */
#define LUA_SEPARATED	4  /* object is in 'finobj' list or in 'tobefnz' */
#define LUA_FIXEDBIT	5  /* object is fixed (should not be collected) */
#define LUA_OLDBIT		6  /* object is old (only in generational mode) */
/* bit 7 is currently used by tests (luaL_checkmemory) */

#define LUA_WHITEBITS	lua_bit2mask(LUA_WHITE0BIT, LUA_WHITE1BIT)


#define lua_iswhite(x)      lua_testbits((x)->gch.marked, LUA_WHITEBITS)
#define lua_isblack(x)      lua_testbit((x)->gch.marked, LUA_BLACKBIT)
#define lua_isgray(x)  /* neither white nor black */  \
	(!lua_testbits((x)->gch.marked, LUA_WHITEBITS | lua_bitmask(LUA_BLACKBIT)))

#define lua_isold(x)	lua_testbit((x)->gch.marked, LUA_OLDBIT)

/* MOVE OLD rule: whenever an object is moved to the beginning of
   a GC list, its old bit must be cleared */
#define lua_resetoldbit(o)	lua_resetbit((o)->gch.marked, LUA_OLDBIT)

#define lua_otherwhite(g)	(g->currentwhite ^ LUA_WHITEBITS)
#define lua_isdeadm(ow,m)	(!(((m) ^ LUA_WHITEBITS) & (ow)))
#define lua_isdead(g,v)	lua_isdeadm(lua_otherwhite(g), (v)->gch.marked)

#define lua_changewhite(x)	((x)->gch.marked ^= LUA_WHITEBITS)
#define lua_gray2black(x)	lua_setbit((x)->gch.marked, LUA_BLACKBIT)

#define lua_valiswhite(x)	(lua_iscollectable(x) && lua_iswhite(lua_gcvalue(x)))

#define luaC_white(g)	lua_cast(lua_ubyte, (g)->currentwhite & LUA_WHITEBITS)


#define luaC_condGC(L,c) \
	{if (LUA_G(L)->GCdebt > 0) {c;}; lua_condchangemem(L);}
#define luaC_checkGC(L)		luaC_condGC(L, luaC_step(L);)


#define luaC_barrier(L,p,v) { if (lua_valiswhite(v) && lua_isblack(lua_obj2gco(p)))  \
	luaC_barrier_(L,lua_obj2gco(p),lua_gcvalue(v)); }

#define luaC_barrierback(L,p,v) { if (lua_valiswhite(v) && lua_isblack(lua_obj2gco(p)))  \
	luaC_barrierback_(L,p); }

#define luaC_objbarrier(L,p,o)  \
	{ if (lua_iswhite(lua_obj2gco(o)) && lua_isblack(lua_obj2gco(p))) \
		luaC_barrier_(L,lua_obj2gco(p),lua_obj2gco(o)); }

#define luaC_objbarrierback(L,p,o)  \
   { if (lua_iswhite(lua_obj2gco(o)) && lua_isblack(lua_obj2gco(p))) luaC_barrierback_(L,p); }

#define luaC_barrierproto(L,p,c) \
   { if (lua_isblack(lua_obj2gco(p))) luaC_barrierproto_(L,p,c); }

LUAI_FUNC void luaC_freeallobjects (lua_State *L);
LUAI_FUNC void luaC_step (lua_State *L);
LUAI_FUNC void luaC_forcestep (lua_State *L);
LUAI_FUNC void luaC_runtilstate (lua_State *L, int statesmask);
LUAI_FUNC void luaC_fullgc (lua_State *L, int isemergency);
LUAI_FUNC lua_GCObject *luaC_newobj (lua_State *L, int tt, size_t sz,
                                 lua_GCObject **list, int offset);
LUAI_FUNC void luaC_barrier_ (lua_State *L, lua_GCObject *o, lua_GCObject *v);
LUAI_FUNC void luaC_barrierback_ (lua_State *L, lua_GCObject *o);
LUAI_FUNC void luaC_barrierproto_ (lua_State *L, lua_Proto *p, lua_Closure *c);
LUAI_FUNC void luaC_checkfinalizer (lua_State *L, lua_GCObject *o, lua_Table *mt);
LUAI_FUNC void luaC_checkupvalcolor (global_State *g, lua_UpVal *uv);
LUAI_FUNC void luaC_changemode (lua_State *L, int mode);

#endif
