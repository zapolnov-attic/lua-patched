/*
** $Id: lapi.c,v 2.171.1.1 2013/04/12 18:48:47 roberto Exp $
** Lua API
** See Copyright Notice in lua.h
*/


#include <stdarg.h>
#include <string.h>

#define lapi_c
#define LUA_CORE

#include "lua.h"

#include "lapi.h"
#include "ldebug.h"
#include "ldo.h"
#include "lfunc.h"
#include "lgc.h"
#include "lmem.h"
#include "lobject.h"
#include "lstate.h"
#include "lstring.h"
#include "ltable.h"
#include "ltm.h"
#include "lundump.h"
#include "lvm.h"



const char lua_ident[] =
  "$LuaVersion: " LUA_COPYRIGHT " $"
  "$LuaAuthors: " LUA_AUTHORS " $";


/* value at a non-valid index */
#define NONVALIDVALUE		lua_cast(lua_TValue *, luaO_nilobject)

/* corresponding test */
#define isvalid(o)	((o) != luaO_nilobject)

/* test for pseudo index */
#define ispseudo(i)		((i) <= LUA_REGISTRYINDEX)

/* test for valid but not pseudo index */
#define isstackindex(i, o)	(isvalid(o) && !ispseudo(i))

#define api_checkvalidindex(L, o)  lua_api_check(L, isvalid(o), "invalid index")

#define api_checkstackindex(L, i, o)  \
	lua_api_check(L, isstackindex(i, o), "index not in the stack")


lua_TValue *lua_index2addr (lua_State *L, int idx) {
  lua_CallInfo *ci = L->ci;
  if (idx > 0) {
    lua_TValue *o = ci->func + idx;
    lua_api_check(L, idx <= ci->top - (ci->func + 1), "unacceptable index");
    if (o >= L->top) return NONVALIDVALUE;
    else return o;
  }
  else if (!ispseudo(idx)) {  /* negative index */
    lua_api_check(L, idx != 0 && -idx <= L->top - (ci->func + 1), "invalid index");
    return L->top + idx;
  }
  else if (idx == LUA_REGISTRYINDEX)
    return &LUA_G(L)->l_registry;
  else {  /* upvalues */
    idx = LUA_REGISTRYINDEX - idx;
    lua_api_check(L, idx <= LUA_MAXUPVAL + 1, "upvalue index too large");
    if (lua_ttislcf(ci->func))  /* light C function? */
      return NONVALIDVALUE;  /* it has no upvalues */
    else {
      lua_CClosure *func = lua_clCvalue(ci->func);
      return (idx <= func->nupvalues) ? &func->upvalue[idx-1] : NONVALIDVALUE;
    }
  }
}


/*
** to be called by 'lua_checkstack' in protected mode, to grow stack
** capturing memory errors
*/
static void growstack (lua_State *L, void *ud) {
  int size = *(int *)ud;
  luaD_growstack(L, size);
}


LUA_API int lua_checkstack (lua_State *L, int size) {
  int res;
  lua_CallInfo *ci = L->ci;
  lua_lock(L);
  if (L->stack_last - L->top > size)  /* stack large enough? */
    res = 1;  /* yes; check is OK */
  else {  /* no; need to grow stack */
    int inuse = lua_cast_int(L->top - L->stack) + LUA_EXTRA_STACK;
    if (inuse > LUAI_MAXSTACK - size)  /* can grow without overflow? */
      res = 0;  /* no */
    else  /* try to grow stack */
      res = (luaD_rawrunprotected(L, &growstack, &size) == LUA_OK);
  }
  if (res && ci->top < L->top + size)
    ci->top = L->top + size;  /* adjust frame top */
  lua_unlock(L);
  return res;
}


LUA_API void lua_xmove (lua_State *from, lua_State *to, int n) {
  int i;
  if (from == to) return;
  lua_lock(to);
  lua_checknelems(from, n);
  lua_api_check(from, LUA_G(from) == LUA_G(to), "moving among independent states");
  lua_api_check(from, to->ci->top - to->top >= n, "not enough elements to move");
  from->top -= n;
  for (i = 0; i < n; i++) {
    lua_setobj2s(to, to->top++, from->top + i);
  }
  lua_unlock(to);
}


LUA_API lua_CFunction lua_atpanic (lua_State *L, lua_CFunction panicf) {
  lua_CFunction old;
  lua_lock(L);
  old = LUA_G(L)->panic;
  LUA_G(L)->panic = panicf;
  lua_unlock(L);
  return old;
}


LUA_API const lua_Number *lua_version (lua_State *L) {
  static const lua_Number version = LUA_VERSION_NUM;
  if (L == NULL) return &version;
  else return LUA_G(L)->version;
}



/*
** basic stack manipulation
*/


/*
** convert an acceptable stack index into an absolute index
*/
LUA_API int lua_absindex (lua_State *L, int idx) {
  return (idx > 0 || ispseudo(idx))
         ? idx
         : lua_cast_int(L->top - L->ci->func + idx);
}


LUA_API int lua_gettop (lua_State *L) {
  return lua_cast_int(L->top - (L->ci->func + 1));
}


LUA_API void lua_settop (lua_State *L, int idx) {
  lua_StkId func = L->ci->func;
  lua_lock(L);
  if (idx >= 0) {
    lua_api_check(L, idx <= L->stack_last - (func + 1), "new top too large");
    while (L->top < (func + 1) + idx)
      lua_setnilvalue(L->top++);
    L->top = (func + 1) + idx;
  }
  else {
    lua_api_check(L, -(idx+1) <= (L->top - (func + 1)), "invalid new top");
    L->top += idx+1;  /* `subtract' index (index is negative) */
  }
  lua_unlock(L);
}


LUA_API void lua_remove (lua_State *L, int idx) {
  lua_StkId p;
  lua_lock(L);
  p = lua_index2addr(L, idx);
  api_checkstackindex(L, idx, p);
  while (++p < L->top) lua_setobjs2s(L, p-1, p);
  L->top--;
  lua_unlock(L);
}


LUA_API void lua_insert (lua_State *L, int idx) {
  lua_StkId p;
  lua_StkId q;
  lua_lock(L);
  p = lua_index2addr(L, idx);
  api_checkstackindex(L, idx, p);
  for (q = L->top; q > p; q--)  /* use L->top as a temporary */
    lua_setobjs2s(L, q, q - 1);
  lua_setobjs2s(L, p, L->top);
  lua_unlock(L);
}


static void moveto (lua_State *L, lua_TValue *fr, int idx) {
  lua_TValue *to = lua_index2addr(L, idx);
  api_checkvalidindex(L, to);
  lua_setobj(L, to, fr);
  if (idx < LUA_REGISTRYINDEX)  /* function upvalue? */
    luaC_barrier(L, lua_clCvalue(L->ci->func), fr);
  /* LUA_REGISTRYINDEX does not need gc barrier
     (collector revisits it before finishing collection) */
}


LUA_API void lua_replace (lua_State *L, int idx) {
  lua_lock(L);
  lua_checknelems(L, 1);
  moveto(L, L->top - 1, idx);
  L->top--;
  lua_unlock(L);
}


LUA_API void lua_copy (lua_State *L, int fromidx, int toidx) {
  lua_TValue *fr;
  lua_lock(L);
  fr = lua_index2addr(L, fromidx);
  moveto(L, fr, toidx);
  lua_unlock(L);
}


LUA_API void lua_pushvalue (lua_State *L, int idx) {
  lua_lock(L);
  lua_setobj2s(L, L->top, lua_index2addr(L, idx));
  lua_api_incr_top(L);
  lua_unlock(L);
}



/*
** access functions (stack -> C)
*/


LUA_API int lua_type (lua_State *L, int idx) {
  lua_StkId o = lua_index2addr(L, idx);
  return (isvalid(o) ? lua_ttypenv(o) : LUA_TNONE);
}


LUA_API const char *lua_typename (lua_State *L, int t) {
  LUA_UNUSED(L);
  return lua_ttypename(t);
}


LUA_API int lua_iscfunction (lua_State *L, int idx) {
  lua_StkId o = lua_index2addr(L, idx);
  return (lua_ttislcf(o) || (lua_ttisCclosure(o)));
}


LUA_API int lua_isnumber (lua_State *L, int idx) {
  lua_TValue n;
  const lua_TValue *o = lua_index2addr(L, idx);
  return luai_tonumber(o, &n);
}


LUA_API int lua_isstring (lua_State *L, int idx) {
  int t = lua_type(L, idx);
  return (t == LUA_TSTRING || t == LUA_TNUMBER);
}


LUA_API int lua_isuserdata (lua_State *L, int idx) {
  const lua_TValue *o = lua_index2addr(L, idx);
  return (lua_ttisuserdata(o) || lua_ttislightuserdata(o));
}


LUA_API int lua_isuserpointer (lua_State *L, int idx) {
  const lua_TValue *o = lua_index2addr(L, idx);
  return lua_ttisuserpointer(o);
}


LUA_API int lua_rawequal (lua_State *L, int index1, int index2) {
  lua_StkId o1 = lua_index2addr(L, index1);
  lua_StkId o2 = lua_index2addr(L, index2);
  return (isvalid(o1) && isvalid(o2)) ? luaV_rawequalobj(o1, o2) : 0;
}


LUA_API void lua_arith (lua_State *L, int op) {
  lua_StkId o1;  /* 1st operand */
  lua_StkId o2;  /* 2nd operand */
  lua_lock(L);
  if (op != LUA_OPUNM) /* all other operations expect two operands */
    lua_checknelems(L, 2);
  else {  /* for unary minus, add fake 2nd operand */
    lua_checknelems(L, 1);
    lua_setobjs2s(L, L->top, L->top - 1);
    L->top++;
  }
  o1 = L->top - 2;
  o2 = L->top - 1;
  if (lua_ttisnumber(o1) && lua_ttisnumber(o2)) {
    lua_setnvalue(o1, luaO_arith(op, lua_nvalue(o1), lua_nvalue(o2)));
  } else if (lua_ttisvec2(o1) && lua_ttisvec2(o2)) {
    float v[2];
    luaO_arithvec2(op, v, lua_vec2value(o1), lua_vec2value(o2));
    lua_setvec2value(o1, v[0], v[1]);
  }
  else
    luaV_arith(L, o1, o1, o2, lua_cast(lua_TMS, op - LUA_OPADD + LUA_TM_ADD));
  L->top--;
  lua_unlock(L);
}


LUA_API int lua_compare (lua_State *L, int index1, int index2, int op) {
  lua_StkId o1, o2;
  int i = 0;
  lua_lock(L);  /* may call tag method */
  o1 = lua_index2addr(L, index1);
  o2 = lua_index2addr(L, index2);
  if (isvalid(o1) && isvalid(o2)) {
    switch (op) {
      case LUA_OPEQ: i = lua_equalobj(L, o1, o2); break;
      case LUA_OPLT: i = luaV_lessthan(L, o1, o2); break;
      case LUA_OPLE: i = luaV_lessequal(L, o1, o2); break;
      default: lua_api_check(L, 0, "invalid option");
    }
  }
  lua_unlock(L);
  return i;
}


LUA_API lua_Number lua_tonumberx (lua_State *L, int idx, int *isnum) {
  lua_TValue n;
  const lua_TValue *o = lua_index2addr(L, idx);
  if (luai_tonumber(o, &n)) {
    if (isnum) *isnum = 1;
    return lua_nvalue(o);
  }
  else {
    if (isnum) *isnum = 0;
    return 0;
  }
}


LUA_API lua_Integer lua_tointegerx (lua_State *L, int idx, int *isnum) {
  lua_TValue n;
  const lua_TValue *o = lua_index2addr(L, idx);
  if (luai_tonumber(o, &n)) {
    lua_Integer res;
    lua_Number num = lua_nvalue(o);
    lua_number2integer(res, num);
    if (isnum) *isnum = 1;
    return res;
  }
  else {
    if (isnum) *isnum = 0;
    return 0;
  }
}


LUA_API lua_Unsigned lua_tounsignedx (lua_State *L, int idx, int *isnum) {
  lua_TValue n;
  const lua_TValue *o = lua_index2addr(L, idx);
  if (luai_tonumber(o, &n)) {
    lua_Unsigned res;
    lua_Number num = lua_nvalue(o);
    lua_number2unsigned(res, num);
    if (isnum) *isnum = 1;
    return res;
  }
  else {
    if (isnum) *isnum = 0;
    return 0;
  }
}


LUA_API int lua_toboolean (lua_State *L, int idx) {
  const lua_TValue *o = lua_index2addr(L, idx);
  return !lua_isfalse(o);
}


LUA_API const char *lua_tolstring (lua_State *L, int idx, size_t *len) {
  lua_StkId o = lua_index2addr(L, idx);
  if (!lua_ttisstring(o)) {
    lua_lock(L);  /* `luaV_tostring' may create a new string */
    if (!luaV_tostring(L, o)) {  /* conversion failed? */
      if (len != NULL) *len = 0;
      lua_unlock(L);
      return NULL;
    }
    luaC_checkGC(L);
    o = lua_index2addr(L, idx);  /* previous call may reallocate the stack */
    lua_unlock(L);
  }
  if (len != NULL) *len = lua_tsvalue(o)->len;
  return lua_svalue(o);
}


LUA_API size_t lua_rawlen (lua_State *L, int idx) {
  lua_StkId o = lua_index2addr(L, idx);
  switch (lua_ttypenv(o)) {
    case LUA_TSTRING: return lua_tsvalue(o)->len;
    case LUA_TUSERPOINTER: return lua_uptrvalue(o)->len;
    case LUA_TUSERDATA: return lua_uvalue(o)->len;
    case LUA_TTABLE: return luaH_getn(lua_hvalue(o));
    default: return 0;
  }
}


LUA_API lua_CFunction lua_tocfunction (lua_State *L, int idx) {
  lua_StkId o = lua_index2addr(L, idx);
  if (lua_ttislcf(o)) return lua_fvalue(o);
  else if (lua_ttisCclosure(o))
    return lua_clCvalue(o)->f;
  else return NULL;  /* not a C function */
}


LUA_API void *lua_touserdata (lua_State *L, int idx) {
  lua_StkId o = lua_index2addr(L, idx);
  switch (lua_ttypenv(o)) {
    case LUA_TUSERDATA: return (lua_rawuvalue(o) + 1);
    case LUA_TLIGHTUSERDATA: return lua_pvalue(o);
    default: return NULL;
  }
}


LUA_API lua_State *lua_tothread (lua_State *L, int idx) {
  lua_StkId o = lua_index2addr(L, idx);
  return (!lua_ttisthread(o)) ? NULL : lua_thvalue(o);
}


LUA_API const void *lua_topointer (lua_State *L, int idx) {
  lua_StkId o = lua_index2addr(L, idx);
  switch (lua_ttype(o)) {
    case LUA_TTABLE: return lua_hvalue(o);
    case LUA_TLCL: return lua_clLvalue(o);
    case LUA_TCCL: return lua_clCvalue(o);
    case LUA_TLCF: return lua_cast(void *, lua_cast(size_t, lua_fvalue(o)));
    case LUA_TTHREAD: return lua_thvalue(o);
    case LUA_TUSERPOINTER: return (lua_rawuptrvalue(o) + 1);
    case LUA_TUSERDATA:
    case LUA_TLIGHTUSERDATA:
      return lua_touserdata(L, idx);
    default: return NULL;
  }
}


LUA_API const float *lua_tovec2 (lua_State *L, int idx, float *out)
{
  lua_TValue n;
  const lua_TValue *o = lua_index2addr(L, idx);
  if (lua_ttisvec2(o))
    return lua_vec2value(o);
  if (luai_tonumber(o, &n))
    out[0] = out[1] = (float)lua_nvalue(o);
  return NULL;
}

/*
** push functions (C -> stack)
*/


LUA_API void lua_pushnil (lua_State *L) {
  lua_lock(L);
  lua_setnilvalue(L->top);
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API void lua_pushnumber (lua_State *L, lua_Number n) {
  lua_lock(L);
  lua_setnvalue(L->top, n);
  luai_checknum(L, L->top,
    luaG_runerror(L, "C API - attempt to push a signaling NaN"));
  lua_api_incr_top(L);
  lua_unlock(L);
}

LUA_API void lua_pushvec2 (lua_State *L, float x, float y) {
  lua_lock(L);
  lua_setvec2value(L->top, x, y);
  lua_api_incr_top(L);
  lua_unlock(L);
}

LUA_API void lua_pushinteger (lua_State *L, lua_Integer n) {
  lua_lock(L);
  lua_setnvalue(L->top, lua_cast_num(n));
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API void lua_pushunsigned (lua_State *L, lua_Unsigned u) {
  lua_Number n;
  lua_lock(L);
  n = lua_unsigned2number(u);
  lua_setnvalue(L->top, n);
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API const char *lua_pushlstring (lua_State *L, const char *s, size_t len) {
  lua_TString *ts;
  lua_lock(L);
  luaC_checkGC(L);
  ts = luaS_newlstr(L, s, len);
  lua_setsvalue2s(L, L->top, ts);
  lua_api_incr_top(L);
  lua_unlock(L);
  return lua_getstr(ts);
}


LUA_API const char *lua_pushstring (lua_State *L, const char *s) {
  if (s == NULL) {
    lua_pushnil(L);
    return NULL;
  }
  else {
    lua_TString *ts;
    lua_lock(L);
    luaC_checkGC(L);
    ts = luaS_new(L, s);
    lua_setsvalue2s(L, L->top, ts);
    lua_api_incr_top(L);
    lua_unlock(L);
    return lua_getstr(ts);
  }
}


LUA_API const char *lua_pushvfstring (lua_State *L, const char *fmt,
                                      va_list argp) {
  const char *ret;
  lua_lock(L);
  luaC_checkGC(L);
  ret = luaO_pushvfstring(L, fmt, argp);
  lua_unlock(L);
  return ret;
}


LUA_API const char *lua_pushfstring (lua_State *L, const char *fmt, ...) {
  const char *ret;
  va_list argp;
  lua_lock(L);
  luaC_checkGC(L);
  va_start(argp, fmt);
  ret = luaO_pushvfstring(L, fmt, argp);
  va_end(argp);
  lua_unlock(L);
  return ret;
}


LUA_API void lua_pushcclosure (lua_State *L, lua_CFunction fn, int n) {
  lua_lock(L);
  if (n == 0) {
    lua_setfvalue(L->top, fn);
  }
  else {
    lua_Closure *cl;
    lua_checknelems(L, n);
    lua_api_check(L, n <= LUA_MAXUPVAL, "upvalue index too large");
    luaC_checkGC(L);
    cl = luaF_newCclosure(L, n);
    cl->c.f = fn;
    L->top -= n;
    while (n--)
      lua_setobj2n(L, &cl->c.upvalue[n], L->top + n);
    lua_setclCvalue(L, L->top, cl);
  }
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API void lua_pushboolean (lua_State *L, int b) {
  lua_lock(L);
  lua_setbvalue(L->top, (b != 0));  /* ensure that true is 1 */
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API void lua_pushlightuserdata (lua_State *L, void *p) {
  lua_lock(L);
  lua_setpvalue(L->top, p);
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API int lua_pushthread (lua_State *L) {
  lua_lock(L);
  lua_setthvalue(L, L->top, L);
  lua_api_incr_top(L);
  lua_unlock(L);
  return (LUA_G(L)->mainthread == L);
}



/*
** get functions (Lua -> stack)
*/


LUA_API void lua_getglobal (lua_State *L, const char *var) {
  lua_Table *reg = lua_hvalue(&LUA_G(L)->l_registry);
  const lua_TValue *gt;  /* global table */
  lua_lock(L);
  gt = luaH_getint(reg, LUA_RIDX_GLOBALS);
  lua_setsvalue2s(L, L->top++, luaS_new(L, var));
  luaV_gettable(L, gt, L->top - 1, L->top - 1);
  lua_unlock(L);
}


LUA_API void lua_gettable (lua_State *L, int idx) {
  lua_StkId t;
  lua_lock(L);
  t = lua_index2addr(L, idx);
  luaV_gettable(L, t, L->top - 1, L->top - 1);
  lua_unlock(L);
}


LUA_API void lua_getfield (lua_State *L, int idx, const char *k) {
  lua_StkId t;
  lua_lock(L);
  t = lua_index2addr(L, idx);
  lua_setsvalue2s(L, L->top, luaS_new(L, k));
  lua_api_incr_top(L);
  luaV_gettable(L, t, L->top - 1, L->top - 1);
  lua_unlock(L);
}


LUA_API void lua_rawget (lua_State *L, int idx) {
  lua_StkId t;
  lua_lock(L);
  t = lua_index2addr(L, idx);
  lua_api_check(L, lua_ttistable(t), "table expected");
  lua_setobj2s(L, L->top - 1, luaH_get(lua_hvalue(t), L->top - 1));
  lua_unlock(L);
}


LUA_API void lua_rawgeti (lua_State *L, int idx, int n) {
  lua_StkId t;
  lua_lock(L);
  t = lua_index2addr(L, idx);
  lua_api_check(L, lua_ttistable(t), "table expected");
  lua_setobj2s(L, L->top, luaH_getint(lua_hvalue(t), n));
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API void lua_rawgetp (lua_State *L, int idx, const void *p) {
  lua_StkId t;
  lua_TValue k;
  lua_lock(L);
  t = lua_index2addr(L, idx);
  lua_api_check(L, lua_ttistable(t), "table expected");
  lua_setpvalue(&k, lua_cast(void *, p));
  lua_setobj2s(L, L->top, luaH_get(lua_hvalue(t), &k));
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API void lua_createtable (lua_State *L, int narray, int nrec) {
  lua_Table *t;
  lua_lock(L);
  luaC_checkGC(L);
  t = luaH_new(L);
  lua_sethvalue(L, L->top, t);
  lua_api_incr_top(L);
  if (narray > 0 || nrec > 0)
    luaH_resize(L, t, narray, nrec);
  lua_unlock(L);
}


LUA_API int lua_getmetatable (lua_State *L, int objindex) {
  const lua_TValue *obj;
  lua_Table *mt = NULL;
  int res;
  lua_lock(L);
  obj = lua_index2addr(L, objindex);
  switch (lua_ttypenv(obj)) {
    case LUA_TTABLE:
      mt = lua_hvalue(obj)->metatable;
      break;
    case LUA_TUSERDATA:
      mt = lua_uvalue(obj)->metatable;
      break;
    case LUA_TUSERPOINTER:
      mt = lua_uptrvalue(obj)->metatable;
      break;
    default:
      mt = LUA_G(L)->mt[lua_ttypenv(obj)];
      break;
  }
  if (mt == NULL)
    res = 0;
  else {
    lua_sethvalue(L, L->top, mt);
    lua_api_incr_top(L);
    res = 1;
  }
  lua_unlock(L);
  return res;
}


LUA_API void lua_getuservalue (lua_State *L, int idx) {
  lua_StkId o;
  lua_lock(L);
  o = lua_index2addr(L, idx);
  if (lua_ttisuserpointer(o))
  {
    if (lua_uptrvalue(o)->env) {
     lua_sethvalue(L, L->top, lua_uptrvalue(o)->env);
    } else
      lua_setnilvalue(L->top);
    lua_api_incr_top(L);
    lua_unlock(L);
    return;
  }
  lua_api_check(L, lua_ttisuserdata(o), "userdata expected");
  if (lua_uvalue(o)->env) {
    lua_sethvalue(L, L->top, lua_uvalue(o)->env);
  } else
    lua_setnilvalue(L->top);
  lua_api_incr_top(L);
  lua_unlock(L);
}


/*
** set functions (stack -> Lua)
*/


LUA_API void lua_setglobal (lua_State *L, const char *var) {
  lua_Table *reg = lua_hvalue(&LUA_G(L)->l_registry);
  const lua_TValue *gt;  /* global table */
  lua_lock(L);
  lua_checknelems(L, 1);
  gt = luaH_getint(reg, LUA_RIDX_GLOBALS);
  lua_setsvalue2s(L, L->top++, luaS_new(L, var));
  luaV_settable(L, gt, L->top - 1, L->top - 2);
  L->top -= 2;  /* pop value and key */
  lua_unlock(L);
}


LUA_API void lua_settable (lua_State *L, int idx) {
  lua_StkId t;
  lua_lock(L);
  lua_checknelems(L, 2);
  t = lua_index2addr(L, idx);
  luaV_settable(L, t, L->top - 2, L->top - 1);
  L->top -= 2;  /* pop index and value */
  lua_unlock(L);
}


LUA_API void lua_setfield (lua_State *L, int idx, const char *k) {
  lua_StkId t;
  lua_lock(L);
  lua_checknelems(L, 1);
  t = lua_index2addr(L, idx);
  lua_setsvalue2s(L, L->top++, luaS_new(L, k));
  luaV_settable(L, t, L->top - 1, L->top - 2);
  L->top -= 2;  /* pop value and key */
  lua_unlock(L);
}


LUA_API void lua_rawset (lua_State *L, int idx) {
  lua_StkId t;
  lua_lock(L);
  lua_checknelems(L, 2);
  t = lua_index2addr(L, idx);
  lua_api_check(L, lua_ttistable(t), "table expected");
  lua_setobj2t(L, luaH_set(L, lua_hvalue(t), L->top-2), L->top-1);
  lua_invalidateTMcache(lua_hvalue(t));
  luaC_barrierback(L, lua_gcvalue(t), L->top-1);
  L->top -= 2;
  lua_unlock(L);
}


LUA_API void lua_rawseti (lua_State *L, int idx, int n) {
  lua_StkId t;
  lua_lock(L);
  lua_checknelems(L, 1);
  t = lua_index2addr(L, idx);
  lua_api_check(L, lua_ttistable(t), "table expected");
  luaH_setint(L, lua_hvalue(t), n, L->top - 1);
  luaC_barrierback(L, lua_gcvalue(t), L->top-1);
  L->top--;
  lua_unlock(L);
}


LUA_API void lua_rawsetp (lua_State *L, int idx, const void *p) {
  lua_StkId t;
  lua_TValue k;
  lua_lock(L);
  lua_checknelems(L, 1);
  t = lua_index2addr(L, idx);
  lua_api_check(L, lua_ttistable(t), "table expected");
  lua_setpvalue(&k, lua_cast(void *, p));
  lua_setobj2t(L, luaH_set(L, lua_hvalue(t), &k), L->top - 1);
  luaC_barrierback(L, lua_gcvalue(t), L->top - 1);
  L->top--;
  lua_unlock(L);
}


LUA_API int lua_setmetatable (lua_State *L, int objindex) {
  lua_TValue *obj;
  lua_Table *mt;
  lua_lock(L);
  lua_checknelems(L, 1);
  obj = lua_index2addr(L, objindex);
  if (lua_ttisnil(L->top - 1))
    mt = NULL;
  else {
    lua_api_check(L, lua_ttistable(L->top - 1), "table expected");
    mt = lua_hvalue(L->top - 1);
  }
  switch (lua_ttypenv(obj)) {
    case LUA_TTABLE: {
      lua_hvalue(obj)->metatable = mt;
      if (mt) {
        luaC_objbarrierback(L, lua_gcvalue(obj), mt);
        luaC_checkfinalizer(L, lua_gcvalue(obj), mt);
      }
      break;
    }
    case LUA_TUSERPOINTER: {
      lua_uptrvalue(obj)->metatable = mt;
      if (mt) {
        luaC_objbarrier(L, lua_rawuptrvalue(obj), mt);
        luaC_checkfinalizer(L, lua_gcvalue(obj), mt);
      }
      break;
    }
    case LUA_TUSERDATA: {
      lua_uvalue(obj)->metatable = mt;
      if (mt) {
        luaC_objbarrier(L, lua_rawuvalue(obj), mt);
        luaC_checkfinalizer(L, lua_gcvalue(obj), mt);
      }
      break;
    }
    default: {
      LUA_G(L)->mt[lua_ttypenv(obj)] = mt;
      break;
    }
  }
  L->top--;
  lua_unlock(L);
  return 1;
}


LUA_API void lua_setuservalue (lua_State *L, int idx) {
  lua_StkId o;
  lua_lock(L);
  lua_checknelems(L, 1);
  o = lua_index2addr(L, idx);
  if (lua_ttisuserpointer(o)) {
    if (lua_ttisnil(L->top - 1))
      lua_uptrvalue(o)->env = NULL;
    else {
      lua_api_check(L, lua_ttistable(L->top - 1), "table expected");
      lua_uptrvalue(o)->env = lua_hvalue(L->top - 1);
      luaC_objbarrier(L, lua_gcvalue(o), lua_hvalue(L->top - 1));
    }
    L->top--;
    lua_unlock(L);
    return;
  }
  lua_api_check(L, lua_ttisuserdata(o), "userdata expected");
  if (lua_ttisnil(L->top - 1))
    lua_uvalue(o)->env = NULL;
  else {
    lua_api_check(L, lua_ttistable(L->top - 1), "table expected");
    lua_uvalue(o)->env = lua_hvalue(L->top - 1);
    luaC_objbarrier(L, lua_gcvalue(o), lua_hvalue(L->top - 1));
  }
  L->top--;
  lua_unlock(L);
}


/*
** `load' and `call' functions (run Lua code)
*/




LUA_API int lua_getctx (lua_State *L, int *ctx) {
  if (L->ci->callstatus & LUA_CIST_YIELDED) {
    if (ctx) *ctx = L->ci->u.c.ctx;
    return L->ci->u.c.status;
  }
  else return LUA_OK;
}


LUA_API void lua_callk (lua_State *L, int nargs, int nresults, int ctx,
                        lua_CFunction k) {
  lua_StkId func;
  lua_lock(L);
  lua_api_check(L, k == NULL || !isLua(L->ci),
    "cannot use continuations inside hooks");
  lua_checknelems(L, nargs+1);
  lua_api_check(L, L->status == LUA_OK, "cannot do calls on non-normal thread");
  lua_checkresults(L, nargs, nresults);
  func = L->top - (nargs+1);
  if (k != NULL && L->nny == 0) {  /* need to prepare continuation? */
    L->ci->u.c.k = k;  /* save continuation */
    L->ci->u.c.ctx = ctx;  /* save context */
    luaD_call(L, func, nresults, 1);  /* do the call */
  }
  else  /* no continuation or no yieldable */
    luaD_call(L, func, nresults, 0);  /* just do the call */
  lua_adjustresults(L, nresults);
  lua_unlock(L);
}



/*
** Execute a protected call.
*/


void lua_f_call (lua_State *L, void *ud) {
  struct lua_CallS *c = lua_cast(struct lua_CallS *, ud);
  luaD_call(L, c->func, c->nresults, 0);
}



LUA_API int lua_pcallk (lua_State *L, int nargs, int nresults, int errfunc,
                        int ctx, lua_CFunction k) {
  struct lua_CallS c;
  int status;
  ptrdiff_t func;
  lua_lock(L);
  lua_api_check(L, k == NULL || !isLua(L->ci),
    "cannot use continuations inside hooks");
  lua_checknelems(L, nargs+1);
  lua_api_check(L, L->status == LUA_OK, "cannot do calls on non-normal thread");
  lua_checkresults(L, nargs, nresults);
  if (errfunc == 0)
    func = 0;
  else {
    lua_StkId o = lua_index2addr(L, errfunc);
    api_checkstackindex(L, errfunc, o);
    func = lua_savestack(L, o);
  }
  c.func = L->top - (nargs+1);  /* function to be called */
  if (k == NULL || L->nny > 0) {  /* no continuation or no yieldable? */
    c.nresults = nresults;  /* do a 'conventional' protected call */
    status = luaD_pcall(L, lua_f_call, &c, lua_savestack(L, c.func), func);
  }
  else {  /* prepare continuation (call is already protected by 'resume') */
    lua_CallInfo *ci = L->ci;
    ci->u.c.k = k;  /* save continuation */
    ci->u.c.ctx = ctx;  /* save context */
    /* save information for error recovery */
    ci->extra = lua_savestack(L, c.func);
    ci->u.c.old_allowhook = L->allowhook;
    ci->u.c.old_errfunc = L->errfunc;
    L->errfunc = func;
    /* mark that function may do error recovery */
    ci->callstatus |= LUA_CIST_YPCALL;
    luaD_call(L, c.func, nresults, 1);  /* do the call */
    ci->callstatus &= ~LUA_CIST_YPCALL;
    L->errfunc = ci->u.c.old_errfunc;
    status = LUA_OK;  /* if it is here, there were no errors */
  }
  lua_adjustresults(L, nresults);
  lua_unlock(L);
  return status;
}


LUA_API int lua_load (lua_State *L, lua_Reader reader, void *data,
                      const char *chunkname, const char *mode) {
  lua_ZIO z;
  int status;
  lua_lock(L);
  if (!chunkname) chunkname = "?";
  luaZ_init(L, &z, reader, data);
  status = luaD_protectedparser(L, &z, chunkname, mode);
  if (status == LUA_OK) {  /* no errors? */
    lua_LClosure *f = lua_clLvalue(L->top - 1);  /* get newly created function */
    if (f->nupvalues == 1) {  /* does it have one upvalue? */
      /* get global table from registry */
      lua_Table *reg = lua_hvalue(&LUA_G(L)->l_registry);
      const lua_TValue *gt = luaH_getint(reg, LUA_RIDX_GLOBALS);
      /* set global table as 1st upvalue of 'f' (may be LUA_ENV) */
      lua_setobj(L, f->upvals[0]->v, gt);
      luaC_barrier(L, f->upvals[0], gt);
    }
  }
  lua_unlock(L);
  return status;
}


LUA_API int lua_dump (lua_State *L, lua_Writer writer, void *data) {
  int status;
  lua_TValue *o;
  lua_lock(L);
  lua_checknelems(L, 1);
  o = L->top - 1;
  if (lua_isLfunction(o))
    status = luaU_dump(L, lua_getproto(o), writer, data, 0);
  else
    status = 1;
  lua_unlock(L);
  return status;
}


LUA_API int lua_status (lua_State *L) {
  return L->status;
}


/*
** Garbage-collection function
*/

LUA_API int lua_gc (lua_State *L, int what, int data) {
  int res = 0;
  global_State *g;
  lua_lock(L);
  g = LUA_G(L);
  switch (what) {
    case LUA_GCSTOP: {
      g->gcrunning = 0;
      break;
    }
    case LUA_GCRESTART: {
      luaE_setdebt(g, 0);
      g->gcrunning = 1;
      break;
    }
    case LUA_GCCOLLECT: {
      luaC_fullgc(L, 0);
      break;
    }
    case LUA_GCCOUNT: {
      /* GC values are expressed in Kbytes: #bytes/2^10 */
      res = lua_cast_int(lua_gettotalbytes(g) >> 10);
      break;
    }
    case LUA_GCCOUNTB: {
      res = lua_cast_int(lua_gettotalbytes(g) & 0x3ff);
      break;
    }
    case LUA_GCSTEP: {
      if (g->gckind == LUA_KGC_GEN) {  /* generational mode? */
        res = (g->GCestimate == 0);  /* true if it will do major collection */
        luaC_forcestep(L);  /* do a single step */
      }
      else {
       lua_umem debt = lua_cast(lua_umem, data) * 1024 - LUA_GCSTEPSIZE;
       if (g->gcrunning)
         debt += g->GCdebt;  /* include current debt */
       luaE_setdebt(g, debt);
       luaC_forcestep(L);
       if (g->gcstate == LUA_GCSpause)  /* end of cycle? */
         res = 1;  /* signal it */
      }
      break;
    }
    case LUA_GCSETPAUSE: {
      res = g->gcpause;
      g->gcpause = data;
      break;
    }
    case LUA_GCSETMAJORINC: {
      res = g->gcmajorinc;
      g->gcmajorinc = data;
      break;
    }
    case LUA_GCSETSTEPMUL: {
      res = g->gcstepmul;
      g->gcstepmul = data;
      break;
    }
    case LUA_GCISRUNNING: {
      res = g->gcrunning;
      break;
    }
    case LUA_GCGEN: {  /* change collector to generational mode */
      luaC_changemode(L, LUA_KGC_GEN);
      break;
    }
    case LUA_GCINC: {  /* change collector to incremental mode */
      luaC_changemode(L, LUA_KGC_NORMAL);
      break;
    }
    default: res = -1;  /* invalid option */
  }
  lua_unlock(L);
  return res;
}



/*
** miscellaneous functions
*/


LUA_API int lua_error (lua_State *L) {
  lua_lock(L);
  lua_checknelems(L, 1);
  luaG_errormsg(L);
  /* code unreachable; will unlock when control actually leaves the kernel */
  return 0;  /* to avoid warnings */
}


LUA_API int lua_next (lua_State *L, int idx) {
  lua_StkId t;
  int more;
  lua_lock(L);
  t = lua_index2addr(L, idx);
  lua_api_check(L, lua_ttistable(t), "table expected");
  more = luaH_next(L, lua_hvalue(t), L->top - 1);
  if (more) {
    lua_api_incr_top(L);
  }
  else  /* no more elements */
    L->top -= 1;  /* remove key */
  lua_unlock(L);
  return more;
}


LUA_API void lua_concat (lua_State *L, int n) {
  lua_lock(L);
  lua_checknelems(L, n);
  if (n >= 2) {
    luaC_checkGC(L);
    luaV_concat(L, n);
  }
  else if (n == 0) {  /* push empty string */
    lua_setsvalue2s(L, L->top, luaS_newlstr(L, "", 0));
    lua_api_incr_top(L);
  }
  /* else n == 1; nothing to do */
  lua_unlock(L);
}


LUA_API void lua_len (lua_State *L, int idx) {
  lua_StkId t;
  lua_lock(L);
  t = lua_index2addr(L, idx);
  luaV_objlen(L, L->top, t);
  lua_api_incr_top(L);
  lua_unlock(L);
}


LUA_API lua_Alloc lua_getallocf (lua_State *L, void **ud) {
  lua_Alloc f;
  lua_lock(L);
  if (ud) *ud = LUA_G(L)->ud;
  f = LUA_G(L)->frealloc;
  lua_unlock(L);
  return f;
}


LUA_API void lua_setallocf (lua_State *L, lua_Alloc f, void *ud) {
  lua_lock(L);
  LUA_G(L)->ud = ud;
  LUA_G(L)->frealloc = f;
  lua_unlock(L);
}


LUA_API void *lua_newuserdata (lua_State *L, size_t size) {
  lua_Udata *u;
  lua_lock(L);
  luaC_checkGC(L);
  u = luaS_newudata(L, size, NULL);
  lua_setuvalue(L, L->top, u);
  lua_api_incr_top(L);
  lua_unlock(L);
  return u + 1;
}

LUA_API void lua_pushuserpointer (lua_State *L, void *ptr, char weak) {
  lua_Udata *u;
  lua_lock(L);
  luaC_checkGC(L);
  u = luaS_newuserpointer(L, NULL);
  ((lua_UserPointer *)(u + 1))->ptr = ptr;
  ((lua_UserPointer *)(u + 1))->weak = weak;
  lua_setuptrvalue(L, L->top, u);
  lua_api_incr_top(L);
  lua_unlock(L);
}


static const char *aux_upvalue (lua_StkId fi, int n, lua_TValue **val,
                                lua_GCObject **owner) {
  switch (lua_ttype(fi)) {
    case LUA_TCCL: {  /* C closure */
      lua_CClosure *f = lua_clCvalue(fi);
      if (!(1 <= n && n <= f->nupvalues)) return NULL;
      *val = &f->upvalue[n-1];
      if (owner) *owner = lua_obj2gco(f);
      return "";
    }
    case LUA_TLCL: {  /* Lua closure */
      lua_LClosure *f = lua_clLvalue(fi);
      lua_TString *name;
      lua_Proto *p = f->p;
      if (!(1 <= n && n <= p->sizeupvalues)) return NULL;
      *val = f->upvals[n-1]->v;
      if (owner) *owner = lua_obj2gco(f->upvals[n - 1]);
      name = p->upvalues[n-1].name;
      return (name == NULL) ? "" : lua_getstr(name);
    }
    default: return NULL;  /* not a closure */
  }
}


LUA_API const char *lua_getupvalue (lua_State *L, int funcindex, int n) {
  const char *name;
  lua_TValue *val = NULL;  /* to avoid warnings */
  lua_lock(L);
  name = aux_upvalue(lua_index2addr(L, funcindex), n, &val, NULL);
  if (name) {
    lua_setobj2s(L, L->top, val);
    lua_api_incr_top(L);
  }
  lua_unlock(L);
  return name;
}


LUA_API const char *lua_setupvalue (lua_State *L, int funcindex, int n) {
  const char *name;
  lua_TValue *val = NULL;  /* to avoid warnings */
  lua_GCObject *owner = NULL;  /* to avoid warnings */
  lua_StkId fi;
  lua_lock(L);
  fi = lua_index2addr(L, funcindex);
  lua_checknelems(L, 1);
  name = aux_upvalue(fi, n, &val, &owner);
  if (name) {
    L->top--;
    lua_setobj(L, val, L->top);
    luaC_barrier(L, owner, L->top);
  }
  lua_unlock(L);
  return name;
}


static lua_UpVal **getupvalref (lua_State *L, int fidx, int n, lua_LClosure **pf) {
  lua_LClosure *f;
  lua_StkId fi = lua_index2addr(L, fidx);
  lua_api_check(L, lua_ttisLclosure(fi), "Lua function expected");
  f = lua_clLvalue(fi);
  lua_api_check(L, (1 <= n && n <= f->p->sizeupvalues), "invalid upvalue index");
  if (pf) *pf = f;
  return &f->upvals[n - 1];  /* get its upvalue pointer */
}


LUA_API void *lua_upvalueid (lua_State *L, int fidx, int n) {
  lua_StkId fi = lua_index2addr(L, fidx);
  switch (lua_ttype(fi)) {
    case LUA_TLCL: {  /* lua closure */
      return *getupvalref(L, fidx, n, NULL);
    }
    case LUA_TCCL: {  /* C closure */
      lua_CClosure *f = lua_clCvalue(fi);
      lua_api_check(L, 1 <= n && n <= f->nupvalues, "invalid upvalue index");
      return &f->upvalue[n - 1];
    }
    default: {
      lua_api_check(L, 0, "closure expected");
      return NULL;
    }
  }
}


LUA_API void lua_upvaluejoin (lua_State *L, int fidx1, int n1,
                                            int fidx2, int n2) {
  lua_LClosure *f1;
  lua_UpVal **up1 = getupvalref(L, fidx1, n1, &f1);
  lua_UpVal **up2 = getupvalref(L, fidx2, n2, NULL);
  *up1 = *up2;
  luaC_objbarrier(L, f1, *up2);
}

