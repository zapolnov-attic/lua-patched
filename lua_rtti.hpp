/*
 * Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef __6725b6db1c369271b0bcaf7c0a440623__
#define __6725b6db1c369271b0bcaf7c0a440623__

#include "lua_vm.hpp"
#include <cassert>

namespace RTTI
{
	class Object;
	class MetaObject;
	class Property;
	class Method;
	class Constructor;
	class Enum;
}

namespace Lua
{
	extern char IDX_META_OBJECT;
	extern char IDX_PROPERTIES_TABLE;
	extern char IDX_METHODS_TABLE;

/*
	void * allocRttiObject(VM & vm, const RTTI::MetaObject * metaObject);
	template <class TYPE> inline void * alloc(VM & vm) { return allocRttiObject(vm, TYPE::staticMetaObject()); }
*/

	RTTI::Object * toRttiObject(VM & vm, const lua_TValue * value, const RTTI::MetaObject * metaObject = NULL);
	inline RTTI::Object * toRttiObject(VM & vm, int index, const RTTI::MetaObject * metaObject = NULL)
		{ return toRttiObject(vm, lua_index2addr(vm.L, index), metaObject); }

	template <class TYPE> inline TYPE * toObject(VM & vm, const lua_TValue * value)
		{ return static_cast<TYPE *>(toRttiObject(vm, value, TYPE::staticMetaObject())); }
	template <class TYPE> inline TYPE * toObject(VM & vm, int index)
		{ return static_cast<TYPE *>(toRttiObject(vm, lua_index2addr(vm.L, index), TYPE::staticMetaObject())); }

	template <class TYPE> inline TYPE * toObjectDynamic(VM & vm, const lua_TValue * value)
		{ return dynamic_cast<TYPE *>(toRttiObject(vm, value, TYPE::staticMetaObject())); }
	template <class TYPE> inline TYPE * toObjectDynamic(VM & vm, int index)
		{ return dynamic_cast<TYPE *>(toRttiObject(vm, lua_index2addr(vm.L, index), TYPE::staticMetaObject())); }

	void pushRttiObjectRawPtr(VM & vm, RTTI::Object * object);
	void pushRttiObjectStrongPtr(VM & vm, const RTTI::ObjectPtr & object);

	bool setRttiObjectToNil(VM & vm, const lua_TValue * value);
	inline bool setRttiObjectToNil(VM & vm, int index) { return setRttiObjectToNil(vm, lua_index2addr(vm.L, index)); }

	void initMetatableForClass(Lua::VM & vm, lua_Table * metatable, const RTTI::MetaObject * metaObject);
	template <class TYPE> inline void initMetatableForClass(VM & vm, lua_Table * metatable)
		{ initMetatableForClass(vm, metatable, TYPE::staticMetaObject()); }

	lua_Table * metatableForClass(VM & vm, const RTTI::MetaObject * metaObject);
	template <class TYPE> inline lua_Table * metatableForClass(VM & vm)
		{ return metatableForClass(vm, TYPE::staticMetaObject()); }

	int propertyAccessor(VM & vm, RTTI::Object * object, const RTTI::Property * property);
	void pushAccessorForProperty(VM & vm, const RTTI::Property * property);

	int methodCaller(VM & vm, RTTI::Object * object, const RTTI::Method * method);
	void pushMethod(VM & vm, const RTTI::Method * method);

	void pushConstructor(VM & vm, const RTTI::Constructor * constructorPtr);
	template <class TYPE> inline void pushConstructorForClass(VM & vm)
	{
		const RTTI::Constructor * constructor = TYPE::staticMetaObject()->constructor();
		lua_assert(constructor != NULL);
		pushConstructor(vm, constructor);
	}

	lua_Table * pushEnum(VM & vm, const RTTI::Enum * enumPtr);
	void addAllEnumsIntoTable(VM & vm, lua_Table * table);
}

#endif
