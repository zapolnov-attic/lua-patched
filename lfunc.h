/*
** $Id: lfunc.h,v 2.8.1.1 2013/04/12 18:48:47 roberto Exp $
** Auxiliary functions to manipulate prototypes and closures
** See Copyright Notice in lua.h
*/

#ifndef lfunc_h
#define lfunc_h


#include "lobject.h"


#define lua_sizeCclosure(n)	(lua_cast(int, sizeof(lua_CClosure)) + \
                         lua_cast(int, sizeof(lua_TValue)*((n)-1)))

#define lua_sizeLclosure(n)	(lua_cast(int, sizeof(lua_LClosure)) + \
                         lua_cast(int, sizeof(lua_TValue *)*((n)-1)))


LUAI_FUNC lua_Proto *luaF_newproto (lua_State *L);
LUAI_FUNC lua_Closure *luaF_newCclosure (lua_State *L, int nelems);
LUAI_FUNC lua_Closure *luaF_newLclosure (lua_State *L, int nelems);
LUAI_FUNC lua_UpVal *luaF_newupval (lua_State *L);
LUAI_FUNC lua_UpVal *luaF_findupval (lua_State *L, lua_StkId level);
LUAI_FUNC void luaF_close (lua_State *L, lua_StkId level);
LUAI_FUNC void luaF_freeproto (lua_State *L, lua_Proto *f);
LUAI_FUNC void luaF_freeupval (lua_State *L, lua_UpVal *uv);
LUAI_FUNC const char *luaF_getlocalname (const lua_Proto *func, int local_number,
                                         int pc);


#endif
