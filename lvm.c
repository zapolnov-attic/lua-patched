/*
** $Id: lvm.c,v 2.155.1.1 2013/04/12 18:48:47 roberto Exp $
** Lua virtual machine
** See Copyright Notice in lua.h
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define lvm_c
#define LUA_CORE

#include "lua.h"

#include "ldebug.h"
#include "ldo.h"
#include "lfunc.h"
#include "lgc.h"
#include "lobject.h"
#include "lopcodes.h"
#include "lstate.h"
#include "lstring.h"
#include "ltable.h"
#include "ltm.h"
#include "lvm.h"



/* limit for table tag-method chains (to avoid loops) */
#define MAXTAGLOOP	100


const lua_TValue *luaV_tonumber (const lua_TValue *obj, lua_TValue *n) {
  lua_Number num;
  if (lua_ttisnumber(obj)) return obj;
  if (lua_ttisstring(obj) && luaO_str2d(lua_svalue(obj), lua_tsvalue(obj)->len, &num)) {
    lua_setnvalue(n, num);
    return n;
  }
  else
    return NULL;
}

const lua_TValue *luaV_tovec2 (const lua_TValue *obj, lua_TValue *n) {
  if (lua_ttisvec2(obj)) return obj;
  if (lua_ttisnumber(obj)) {
    float v = (float)lua_nvalue(obj);
    lua_setvec2value(n, v, v);
    return n;
  }
  else
    return NULL;
}

int luaV_tostring (lua_State *L, lua_StkId obj) {
  if (!lua_ttisnumber(obj))
    return 0;
  else {
    char s[LUAI_MAXNUMBER2STR];
    lua_Number n = lua_nvalue(obj);
    int l = lua_number2str(s, n);
    lua_setsvalue2s(L, obj, luaS_newlstr(L, s, l));
    return 1;
  }
}


static void traceexec (lua_State *L) {
  lua_CallInfo *ci = L->ci;
  lua_ubyte mask = L->hookmask;
  int counthook = ((mask & LUA_MASKCOUNT) && L->hookcount == 0);
  if (counthook)
    lua_resethookcount(L);  /* reset count */
  if (ci->callstatus & LUA_CIST_HOOKYIELD) {  /* called hook last time? */
    ci->callstatus &= ~LUA_CIST_HOOKYIELD;  /* erase mark */
    return;  /* do not call hook again (VM yielded, so it did not move) */
  }
  if (counthook)
    luaD_hook(L, LUA_HOOKCOUNT, -1);  /* call count hook */
  if (mask & LUA_MASKLINE) {
    lua_Proto *p = lua_ci_func(ci)->p;
    int npc = lua_pcRel(ci->u.l.savedpc, p);
    int newline = lua_getfuncline(p, npc);
    if (npc == 0 ||  /* call linehook when enter a new function, */
        ci->u.l.savedpc <= L->oldpc ||  /* when jump back (loop), or when */
        newline != lua_getfuncline(p, lua_pcRel(L->oldpc, p)))  /* enter a new line */
      luaD_hook(L, LUA_HOOKLINE, newline);  /* call line hook */
  }
  L->oldpc = ci->u.l.savedpc;
  if (L->status == LUA_YIELD) {  /* did hook yield? */
    if (counthook)
      L->hookcount = 1;  /* undo decrement to zero */
    ci->u.l.savedpc--;  /* undo increment (resume will increment it again) */
    ci->callstatus |= LUA_CIST_HOOKYIELD;  /* mark that it yielded */
    ci->func = L->top - 1;  /* protect stack below results */
    luaD_throw(L, LUA_YIELD);
  }
}


static void callTM (lua_State *L, const lua_TValue *f, const lua_TValue *p1,
                    const lua_TValue *p2, lua_TValue *p3, int hasres) {
  ptrdiff_t result = lua_savestack(L, p3);
  lua_setobj2s(L, L->top++, f);  /* push function */
  lua_setobj2s(L, L->top++, p1);  /* 1st argument */
  lua_setobj2s(L, L->top++, p2);  /* 2nd argument */
  if (!hasres)  /* no result? 'p3' is third argument */
    lua_setobj2s(L, L->top++, p3);  /* 3rd argument */
  /* metamethod may yield only when called from Lua code */
  luaD_call(L, L->top - (4 - hasres), hasres, isLua(L->ci));
  if (hasres) {  /* if has result, move it to its place */
    p3 = lua_restorestack(L, result);
    lua_setobjs2s(L, p3, --L->top);
  }
}


void luaV_gettable (lua_State *L, const lua_TValue *t, lua_TValue *key, lua_StkId val) {
  int loop;
  for (loop = 0; loop < MAXTAGLOOP; loop++) {
    const lua_TValue *tm;
    if (lua_ttistable(t)) {  /* `t' is a table? */
      lua_Table *h = lua_hvalue(t);
      const lua_TValue *res = luaH_get(h, key); /* do a primitive get */
      if (!lua_ttisnil(res) ||  /* result is not nil? */
          (tm = lua_fasttm(L, h->metatable, LUA_TM_INDEX)) == NULL) { /* or no TM? */
        lua_setobj2s(L, val, res);
        return;
      }
    }
    else if (lua_ttisvec2(t)) {  /* `t' is a vec2? */
      const float * v2 = lua_vec2value(t);
      if (lua_ttisnumber(key)) {
        int index = (int)lua_nvalue(key);
        if (index == 0 || index == 1) {
          lua_setnvalue(val, v2[index]);
          return;
        }
      } else if (lua_ttisstring(key)) {
        const char * p = lua_getstr(lua_tsvalue(key));
        size_t len = lua_tsvalue(key)->len;
        if (len == 1) {
          switch (*p) {
          case 'x': lua_setnvalue(val, v2[0]); return;
          case 'y': lua_setnvalue(val, v2[1]); return;
          }
        }
        else if (len == 2) {
          float x, y;
          switch (p[0]) {
          case 'x': x = v2[0]; break;
          case 'y': x = v2[1]; break;
          default: goto invalid;
          }
          switch (p[1])
          {
          case 'x': y = v2[0]; break;
          case 'y': y = v2[1]; break;
          default: goto invalid;
          }
          lua_setvec2value(val, x, y);
          return;
        }
        invalid:;
      }
      /* else will try the tag method */
      if (lua_ttisnil(tm = luaT_gettmbyobj(L, t, LUA_TM_INDEX)))
        luaG_typeerror(L, t, "use invalid index to access");
    }
    else if (lua_ttisnil(tm = luaT_gettmbyobj(L, t, LUA_TM_INDEX)))
      luaG_typeerror(L, t, "index");
    if (lua_ttisfunction(tm)) {
      callTM(L, tm, t, key, val, 1);
      return;
    }
    t = tm;  /* else repeat with 'tm' */
  }
  luaG_runerror(L, "loop in gettable");
}


void luaV_settable (lua_State *L, const lua_TValue *t, lua_TValue *key, lua_StkId val) {
  int loop;
  for (loop = 0; loop < MAXTAGLOOP; loop++) {
    const lua_TValue *tm;
    if (lua_ttistable(t)) {  /* `t' is a table? */
      lua_Table *h = lua_hvalue(t);
      lua_TValue *oldval = lua_cast(lua_TValue *, luaH_get(h, key));
      /* if previous value is not nil, there must be a previous entry
         in the table; moreover, a metamethod has no relevance */
      if (!lua_ttisnil(oldval) ||
         /* previous value is nil; must check the metamethod */
         ((tm = lua_fasttm(L, h->metatable, LUA_TM_NEWINDEX)) == NULL &&
         /* no metamethod; is there a previous entry in the table? */
         (oldval != luaO_nilobject ||
         /* no previous entry; must create one. (The next test is
            always true; we only need the assignment.) */
         (oldval = luaH_newkey(L, h, key), 1)))) {
        /* no metamethod and (now) there is an entry with given key */
        lua_setobj2t(L, oldval, val);  /* assign new value to that entry */
        lua_invalidateTMcache(h);
        luaC_barrierback(L, lua_obj2gco(h), val);
        return;
      }
      /* else will try the metamethod */
    }
    else  /* not a table; check metamethod */
      if (lua_ttisnil(tm = luaT_gettmbyobj(L, t, LUA_TM_NEWINDEX)))
        luaG_typeerror(L, t, "index");
    /* there is a metamethod */
    if (lua_ttisfunction(tm)) {
      callTM(L, tm, t, key, val, 0);
      return;
    }
    t = tm;  /* else repeat with 'tm' */
  }
  luaG_runerror(L, "loop in settable");
}


static int call_binTM (lua_State *L, const lua_TValue *p1, const lua_TValue *p2,
                       lua_StkId res, lua_TMS event) {
  const lua_TValue *tm = luaT_gettmbyobj(L, p1, event);  /* try first operand */
  if (lua_ttisnil(tm))
    tm = luaT_gettmbyobj(L, p2, event);  /* try second operand */
  if (lua_ttisnil(tm)) return 0;
  callTM(L, tm, p1, p2, res, 1);
  return 1;
}


static const lua_TValue *get_equalTM (lua_State *L, lua_Table *mt1, lua_Table *mt2,
                                  lua_TMS event) {
  const lua_TValue *tm1 = lua_fasttm(L, mt1, event);
  const lua_TValue *tm2;
  if (tm1 == NULL) return NULL;  /* no metamethod */
  if (mt1 == mt2) return tm1;  /* same metatables => same metamethods */
  tm2 = lua_fasttm(L, mt2, event);
  if (tm2 == NULL) return NULL;  /* no metamethod */
  if (luaV_rawequalobj(tm1, tm2))  /* same metamethods? */
    return tm1;
  return NULL;
}


static int call_orderTM (lua_State *L, const lua_TValue *p1, const lua_TValue *p2,
                         lua_TMS event) {
  if (!call_binTM(L, p1, p2, L->top, event))
    return -1;  /* no metamethod */
  else
    return !lua_isfalse(L->top);
}


static int l_strcmp (const lua_TString *ls, const lua_TString *rs) {
  const char *l = lua_getstr(ls);
  size_t ll = ls->tsv.len;
  const char *r = lua_getstr(rs);
  size_t lr = rs->tsv.len;
  for (;;) {
    int temp = strcoll(l, r);
    if (temp != 0) return temp;
    else {  /* strings are equal up to a `\0' */
      size_t len = strlen(l);  /* index of first `\0' in both strings */
      if (len == lr)  /* r is finished? */
        return (len == ll) ? 0 : 1;
      else if (len == ll)  /* l is finished? */
        return -1;  /* l is smaller than r (because r is not finished) */
      /* both strings longer than `len'; go on comparing (after the `\0') */
      len++;
      l += len; ll -= len; r += len; lr -= len;
    }
  }
}


int luaV_lessthan (lua_State *L, const lua_TValue *l, const lua_TValue *r) {
  int res;
  if (lua_ttisnumber(l) && lua_ttisnumber(r))
    return luai_numlt(L, lua_nvalue(l), lua_nvalue(r));
  else if (lua_ttisstring(l) && lua_ttisstring(r))
    return l_strcmp(lua_rawtsvalue(l), lua_rawtsvalue(r)) < 0;
  else if ((res = call_orderTM(L, l, r, LUA_TM_LT)) < 0)
    luaG_ordererror(L, l, r);
  return res;
}


int luaV_lessequal (lua_State *L, const lua_TValue *l, const lua_TValue *r) {
  int res;
  if (lua_ttisnumber(l) && lua_ttisnumber(r))
    return luai_numle(L, lua_nvalue(l), lua_nvalue(r));
  else if (lua_ttisstring(l) && lua_ttisstring(r))
    return l_strcmp(lua_rawtsvalue(l), lua_rawtsvalue(r)) <= 0;
  else if ((res = call_orderTM(L, l, r, LUA_TM_LE)) >= 0)  /* first try `le' */
    return res;
  else if ((res = call_orderTM(L, r, l, LUA_TM_LT)) < 0)  /* else try `lt' */
    luaG_ordererror(L, l, r);
  return !res;
}


/*
** equality of Lua values. L == NULL means raw equality (no metamethods)
*/
int luaV_equalobj_ (lua_State *L, const lua_TValue *t1, const lua_TValue *t2) {
  const lua_TValue *tm;
  lua_assert(lua_ttisequal(t1, t2));
  switch (lua_ttype(t1)) {
    case LUA_TNIL: return 1;
    case LUA_TNUMBER: return luai_numeq(lua_nvalue(t1), lua_nvalue(t2));
    case LUA_TBOOLEAN: return lua_bvalue(t1) == lua_bvalue(t2);  /* true must be 1 !! */
    case LUA_TLIGHTUSERDATA: return lua_pvalue(t1) == lua_pvalue(t2);
    case LUA_TLCF: return lua_fvalue(t1) == lua_fvalue(t2);
    case LUA_TSHRSTR: return lua_eqshrstr(lua_rawtsvalue(t1), lua_rawtsvalue(t2));
    case LUA_TLNGSTR: return luaS_eqlngstr(lua_rawtsvalue(t1), lua_rawtsvalue(t2));
    case LUA_TVEC2: {
      const float * a = lua_vec2value(t1);
      const float * b = lua_vec2value(t2);
      return a[0] == b[0] && a[1] == b[1];
      }
    case LUA_TUSERPOINTER: {
      if (lua_uptrvalue(t1) == lua_uptrvalue(t2)) return 1;
      else {
         const void * p1 = ((const lua_UserPointer *)(lua_rawuptrvalue(t1) + 1))->ptr;
         const void * p2 = ((const lua_UserPointer *)(lua_rawuptrvalue(t2) + 1))->ptr;
         if (p1 == p2) return 1;
         else if (L == NULL) return 0;
      }
      tm = get_equalTM(L, lua_uptrvalue(t1)->metatable, lua_uptrvalue(t2)->metatable, LUA_TM_EQ);
      break;  /* will try TM */
    }
    case LUA_TUSERDATA: {
      if (lua_uvalue(t1) == lua_uvalue(t2)) return 1;
      else if (L == NULL) return 0;
      tm = get_equalTM(L, lua_uvalue(t1)->metatable, lua_uvalue(t2)->metatable, LUA_TM_EQ);
      break;  /* will try TM */
    }
    case LUA_TTABLE: {
      if (lua_hvalue(t1) == lua_hvalue(t2)) return 1;
      else if (L == NULL) return 0;
      tm = get_equalTM(L, lua_hvalue(t1)->metatable, lua_hvalue(t2)->metatable, LUA_TM_EQ);
      break;  /* will try TM */
    }
    default:
      lua_assert(lua_iscollectable(t1));
      return lua_gcvalue(t1) == lua_gcvalue(t2);
  }
  if (tm == NULL) return 0;  /* no TM? */
  callTM(L, tm, t1, t2, L->top, 1);  /* call TM */
  return !lua_isfalse(L->top);
}


void luaV_concat (lua_State *L, int total) {
  lua_assert(total >= 2);
  do {
    lua_StkId top = L->top;
    int n = 2;  /* number of elements handled in this pass (at least 2) */
    if (!(lua_ttisstring(top-2) || lua_ttisnumber(top-2)) || !luai_tostring(L, top-1)) {
      if (!call_binTM(L, top-2, top-1, top-2, LUA_TM_CONCAT))
        luaG_concaterror(L, top-2, top-1);
    }
    else if (lua_tsvalue(top-1)->len == 0)  /* second operand is empty? */
      (void)luai_tostring(L, top - 2);  /* result is first operand */
    else if (lua_ttisstring(top-2) && lua_tsvalue(top-2)->len == 0) {
      lua_setobjs2s(L, top - 2, top - 1);  /* result is second op. */
    }
    else {
      /* at least two non-empty string values; get as many as possible */
      size_t tl = lua_tsvalue(top-1)->len;
      char *buffer;
      int i;
      /* collect total length */
      for (i = 1; i < total && luai_tostring(L, top-i-1); i++) {
        size_t l = lua_tsvalue(top-i-1)->len;
        if (l >= (LUA_MAX_SIZET/sizeof(char)) - tl)
          luaG_runerror(L, "string length overflow");
        tl += l;
      }
      buffer = luaZ_openspace(L, &LUA_G(L)->buff, tl);
      tl = 0;
      n = i;
      do {  /* concat all strings */
        size_t l = lua_tsvalue(top-i)->len;
        memcpy(buffer+tl, lua_svalue(top-i), l * sizeof(char));
        tl += l;
      } while (--i > 0);
      lua_setsvalue2s(L, top-n, luaS_newlstr(L, buffer, tl));
    }
    total -= n-1;  /* got 'n' strings to create 1 new */
    L->top -= n-1;  /* popped 'n' strings and pushed one */
  } while (total > 1);  /* repeat until only 1 result left */
}


void luaV_objlen (lua_State *L, lua_StkId ra, const lua_TValue *rb) {
  const lua_TValue *tm;
  switch (lua_ttypenv(rb)) {
    case LUA_TTABLE: {
      lua_Table *h = lua_hvalue(rb);
      tm = lua_fasttm(L, h->metatable, LUA_TM_LEN);
      if (tm) break;  /* metamethod? break switch to call it */
      lua_setnvalue(ra, lua_cast_num(luaH_getn(h)));  /* else primitive len */
      return;
    }
    case LUA_TSTRING: {
      lua_setnvalue(ra, lua_cast_num(lua_tsvalue(rb)->len));
      return;
    }
    case LUA_TVEC2: {
      lua_setnvalue(ra, 2);
      return;
    }
    default: {  /* try metamethod */
      tm = luaT_gettmbyobj(L, rb, LUA_TM_LEN);
      if (lua_ttisnil(tm))  /* no metamethod? */
        luaG_typeerror(L, rb, "get length of");
      break;
    }
  }
  callTM(L, tm, rb, rb, ra, 1);
}


void luaV_arith (lua_State *L, lua_StkId ra, const lua_TValue *rb,
                 const lua_TValue *rc, lua_TMS op) {
  lua_TValue tempb, tempc;
  const lua_TValue *b, *c;
  if ((b = luaV_tonumber(rb, &tempb)) != NULL &&
      (c = luaV_tonumber(rc, &tempc)) != NULL) {
    lua_Number res = luaO_arith(op - LUA_TM_ADD + LUA_OPADD, lua_nvalue(b), lua_nvalue(c));
    lua_setnvalue(ra, res);
  }
  else if ((b = luaV_tovec2(rb, &tempb)) != NULL &&
           (c = luaV_tovec2(rc, &tempc)) != NULL) {
    float res[2];
    luaO_arithvec2(op - LUA_TM_ADD + LUA_OPADD, res, lua_vec2value(b), lua_vec2value(c));
    lua_setvec2value(ra, res[0], res[1]);
  }
  else if (!call_binTM(L, rb, rc, ra, op))
    luaG_aritherror(L, rb, rc);
}


/*
** check whether cached closure in prototype 'p' may be reused, that is,
** whether there is a cached closure with the same upvalues needed by
** new closure to be created.
*/
static lua_Closure *getcached (lua_Proto *p, lua_UpVal **encup, lua_StkId base) {
  lua_Closure *c = p->cache;
  if (c != NULL) {  /* is there a cached closure? */
    int nup = p->sizeupvalues;
    lua_Upvaldesc *uv = p->upvalues;
    int i;
    for (i = 0; i < nup; i++) {  /* check whether it has right upvalues */
      lua_TValue *v = uv[i].instack ? base + uv[i].idx : encup[uv[i].idx]->v;
      if (c->l.upvals[i]->v != v)
        return NULL;  /* wrong upvalue; cannot reuse closure */
    }
  }
  return c;  /* return cached closure (or NULL if no cached closure) */
}


/*
** create a new Lua closure, push it in the stack, and initialize
** its upvalues. Note that the call to 'luaC_barrierproto' must come
** before the assignment to 'p->cache', as the function needs the
** original value of that field.
*/
static void pushclosure (lua_State *L, lua_Proto *p, lua_UpVal **encup, lua_StkId base,
                         lua_StkId ra) {
  int nup = p->sizeupvalues;
  lua_Upvaldesc *uv = p->upvalues;
  int i;
  lua_Closure *ncl = luaF_newLclosure(L, nup);
  ncl->l.p = p;
  lua_setclLvalue(L, ra, ncl);  /* anchor new closure in stack */
  for (i = 0; i < nup; i++) {  /* fill in its upvalues */
    if (uv[i].instack)  /* upvalue refers to local variable? */
      ncl->l.upvals[i] = luaF_findupval(L, base + uv[i].idx);
    else  /* get upvalue from enclosing function */
      ncl->l.upvals[i] = encup[uv[i].idx];
  }
  luaC_barrierproto(L, p, ncl);
  p->cache = ncl;  /* save it on cache for reuse */
}


/*
** finish execution of an opcode interrupted by an yield
*/
void luaV_finishOp (lua_State *L) {
  lua_CallInfo *ci = L->ci;
  lua_StkId base = ci->u.l.base;
  lua_Instruction inst = *(ci->u.l.savedpc - 1);  /* interrupted instruction */
  lua_OpCode op = LUA_GET_OPCODE(inst);
  switch (op) {  /* finish its execution */
    case LUA_OP_ADD: case LUA_OP_SUB: case LUA_OP_MUL: case LUA_OP_DIV:
    case LUA_OP_MOD: case LUA_OP_POW: case LUA_OP_UNM: case LUA_OP_LEN:
    case LUA_OP_GETTABUP: case LUA_OP_GETTABLE: case LUA_OP_SELF: {
      lua_setobjs2s(L, base + LUA_GETARG_A(inst), --L->top);
      break;
    }
    case LUA_OP_LE: case LUA_OP_LT: case LUA_OP_EQ: {
      int res = !lua_isfalse(L->top - 1);
      L->top--;
      /* metamethod should not be called when operand is K */
      lua_assert(!LUA_ISK(LUA_GETARG_B(inst)));
      if (op == LUA_OP_LE &&  /* "<=" using "<" instead? */
          lua_ttisnil(luaT_gettmbyobj(L, base + LUA_GETARG_B(inst), LUA_TM_LE)))
        res = !res;  /* invert result */
      lua_assert(LUA_GET_OPCODE(*ci->u.l.savedpc) == LUA_OP_JMP);
      if (res != LUA_GETARG_A(inst))  /* condition failed? */
        ci->u.l.savedpc++;  /* skip jump instruction */
      break;
    }
    case LUA_OP_CONCAT: {
      lua_StkId top = L->top - 1;  /* top when 'call_binTM' was called */
      int b = LUA_GETARG_B(inst);      /* first element to concatenate */
      int total = lua_cast_int(top - 1 - (base + b));  /* yet to concatenate */
      lua_setobj2s(L, top - 2, top);  /* put TM result in proper position */
      if (total > 1) {  /* are there elements to concat? */
        L->top = top - 1;  /* top is one after last element (at top-2) */
        luaV_concat(L, total);  /* concat them (may yield again) */
      }
      /* move final result to final position */
      lua_setobj2s(L, ci->u.l.base + LUA_GETARG_A(inst), L->top - 1);
      L->top = ci->top;  /* restore top */
      break;
    }
    case LUA_OP_TFORCALL: {
      lua_assert(LUA_GET_OPCODE(*ci->u.l.savedpc) == LUA_OP_TFORLOOP);
      L->top = ci->top;  /* correct top */
      break;
    }
    case LUA_OP_CALL: {
      if (LUA_GETARG_C(inst) - 1 >= 0)  /* nresults >= 0? */
        L->top = ci->top;  /* adjust results */
      break;
    }
    case LUA_OP_TAILCALL: case LUA_OP_SETTABUP: case LUA_OP_SETTABLE:
      break;
    default: lua_assert(0);
  }
}



/*
** some macros for common tasks in `luaV_execute'
*/

#if !defined luai_runtimecheck
#define luai_runtimecheck(L, c)		/* void */
#endif


#define RA(i)	(base+LUA_GETARG_A(i))
/* to be used after possible stack reallocation */
#define RB(i)	lua_check_exp(lua_getBMode(LUA_GET_OPCODE(i)) == lua_OpArgR, base+LUA_GETARG_B(i))
#define RC(i)	lua_check_exp(lua_getCMode(LUA_GET_OPCODE(i)) == lua_OpArgR, base+LUA_GETARG_C(i))
#define RKB(i)	lua_check_exp(lua_getBMode(LUA_GET_OPCODE(i)) == lua_OpArgK, \
	LUA_ISK(LUA_GETARG_B(i)) ? k+LUA_INDEXK(LUA_GETARG_B(i)) : base+LUA_GETARG_B(i))
#define RKC(i)	lua_check_exp(lua_getCMode(LUA_GET_OPCODE(i)) == lua_OpArgK, \
	LUA_ISK(LUA_GETARG_C(i)) ? k+LUA_INDEXK(LUA_GETARG_C(i)) : base+LUA_GETARG_C(i))
#define KBx(i)  \
  (k + (LUA_GETARG_Bx(i) != 0 ? LUA_GETARG_Bx(i) - 1 : LUA_GETARG_Ax(*ci->u.l.savedpc++)))


/* execute a jump instruction */
#define dojump(ci,i,e) \
  { int a = LUA_GETARG_A(i); \
    if (a > 0) luaF_close(L, ci->u.l.base + a - 1); \
    ci->u.l.savedpc += LUA_GETARG_sBx(i) + e; }

/* for test instructions, execute the jump instruction that follows it */
#define donextjump(ci)	{ i = *ci->u.l.savedpc; dojump(ci, i, 1); }


#define Protect(x)	{ {x;}; base = ci->u.l.base; }

#define checkGC(L,c)  \
  Protect( luaC_condGC(L,{L->top = (c);  /* limit of live values */ \
                          luaC_step(L); \
                          L->top = ci->top;})  /* restore top */ \
           luai_threadyield(L); )


#define arith_op(op,tm) { \
        lua_TValue *rb = RKB(i); \
        lua_TValue *rc = RKC(i); \
        if (lua_ttisnumber(rb) && lua_ttisnumber(rc)) { \
          lua_Number nb = lua_nvalue(rb), nc = lua_nvalue(rc); \
          lua_setnvalue(ra, op(L, nb, nc)); \
        } \
        else { Protect(luaV_arith(L, ra, rb, rc, tm)); } }


#define vmdispatch(o)	switch(o)
#define vmcase(l,b)	case l: {b}  break;
#define vmcasenb(l,b)	case l: {b}		/* nb = no break */

void luaV_execute (lua_State *L) {
  lua_CallInfo *ci = L->ci;
  lua_LClosure *cl;
  lua_TValue *k;
  lua_StkId base;
 newframe:  /* reentry point when frame changes (call/return) */
  lua_assert(ci == L->ci);
  cl = lua_clLvalue(ci->func);
  k = cl->p->k;
  base = ci->u.l.base;
  /* main loop of interpreter */
  for (;;) {
    lua_Instruction i = *(ci->u.l.savedpc++);
    lua_StkId ra;
    if ((L->hookmask & (LUA_MASKLINE | LUA_MASKCOUNT)) &&
        (--L->hookcount == 0 || L->hookmask & LUA_MASKLINE)) {
      Protect(traceexec(L));
    }
    /* WARNING: several calls may realloc the stack and invalidate `ra' */
    ra = RA(i);
    lua_assert(base == ci->u.l.base);
    lua_assert(base <= L->top && L->top < L->stack + L->stacksize);
    vmdispatch (LUA_GET_OPCODE(i)) {
      vmcase(LUA_OP_MOVE,
        lua_setobjs2s(L, ra, RB(i));
      )
      vmcase(LUA_OP_LOADK,
        lua_TValue *rb = k + LUA_GETARG_Bx(i);
        lua_setobj2s(L, ra, rb);
      )
      vmcase(LUA_OP_LOADKX,
        lua_TValue *rb;
        lua_assert(LUA_GET_OPCODE(*ci->u.l.savedpc) == LUA_OP_EXTRAARG);
        rb = k + LUA_GETARG_Ax(*ci->u.l.savedpc++);
        lua_setobj2s(L, ra, rb);
      )
      vmcase(LUA_OP_LOADBOOL,
        lua_setbvalue(ra, LUA_GETARG_B(i));
        if (LUA_GETARG_C(i)) ci->u.l.savedpc++;  /* skip next instruction (if C) */
      )
      vmcase(LUA_OP_LOADNIL,
        int b = LUA_GETARG_B(i);
        do {
          lua_setnilvalue(ra++);
        } while (b--);
      )
      vmcase(LUA_OP_GETUPVAL,
        int b = LUA_GETARG_B(i);
        lua_setobj2s(L, ra, cl->upvals[b]->v);
      )
      vmcase(LUA_OP_GETTABUP,
        int b = LUA_GETARG_B(i);
        Protect(luaV_gettable(L, cl->upvals[b]->v, RKC(i), ra));
      )
      vmcase(LUA_OP_GETTABLE,
        Protect(luaV_gettable(L, RB(i), RKC(i), ra));
      )
      vmcase(LUA_OP_SETTABUP,
        int a = LUA_GETARG_A(i);
        Protect(luaV_settable(L, cl->upvals[a]->v, RKB(i), RKC(i)));
      )
      vmcase(LUA_OP_SETUPVAL,
        lua_UpVal *uv = cl->upvals[LUA_GETARG_B(i)];
        lua_setobj(L, uv->v, ra);
        luaC_barrier(L, uv, ra);
      )
      vmcase(LUA_OP_SETTABLE,
        Protect(luaV_settable(L, ra, RKB(i), RKC(i)));
      )
      vmcase(LUA_OP_NEWTABLE,
        int b = LUA_GETARG_B(i);
        int c = LUA_GETARG_C(i);
        lua_Table *t = luaH_new(L);
        lua_sethvalue(L, ra, t);
        if (b != 0 || c != 0)
          luaH_resize(L, t, luaO_fb2int(b), luaO_fb2int(c));
        checkGC(L, ra + 1);
      )
      vmcase(LUA_OP_SELF,
        lua_StkId rb = RB(i);
        lua_setobjs2s(L, ra+1, rb);
        Protect(luaV_gettable(L, rb, RKC(i), ra));
      )
      vmcase(LUA_OP_VEC2,
        lua_TValue tempb;
        lua_TValue tempc;
        const lua_TValue *b;
        const lua_TValue *c;
        lua_TValue *rb = RKB(i);
        lua_TValue *rc = RKC(i);
        if ((b = luaV_tonumber(rb, &tempb)) != NULL &&
            (c = luaV_tonumber(rc, &tempc)) != NULL) {
          float res[2];
          res[0] = (float)lua_nvalue(b);
          res[1] = (float)lua_nvalue(c);
          lua_setvec2value(ra, res[0], res[1]);
        }
        else luaG_aritherror(L, rb, rc);
      )
      vmcase(LUA_OP_ADD,
        arith_op(luai_numadd, LUA_TM_ADD);
      )
      vmcase(LUA_OP_SUB,
        arith_op(luai_numsub, LUA_TM_SUB);
      )
      vmcase(LUA_OP_MUL,
        arith_op(luai_nummul, LUA_TM_MUL);
      )
      vmcase(LUA_OP_DIV,
        arith_op(luai_numdiv, LUA_TM_DIV);
      )
      vmcase(LUA_OP_MOD,
        arith_op(luai_nummod, LUA_TM_MOD);
      )
      vmcase(LUA_OP_POW,
        arith_op(luai_numpow, LUA_TM_POW);
      )
      vmcase(LUA_OP_UNM,
        lua_TValue *rb = RB(i);
        if (lua_ttisnumber(rb)) {
          lua_Number nb = lua_nvalue(rb);
          lua_setnvalue(ra, luai_numunm(L, nb));
        }
        else {
          Protect(luaV_arith(L, ra, rb, rb, LUA_TM_UNM));
        }
      )
      vmcase(LUA_OP_NOT,
        lua_TValue *rb = RB(i);
        int res = lua_isfalse(rb);  /* next assignment may change this value */
        lua_setbvalue(ra, res);
      )
      vmcase(LUA_OP_LEN,
        Protect(luaV_objlen(L, ra, RB(i)));
      )
      vmcase(LUA_OP_CONCAT,
        int b = LUA_GETARG_B(i);
        int c = LUA_GETARG_C(i);
        lua_StkId rb;
        L->top = base + c + 1;  /* mark the end of concat operands */
        Protect(luaV_concat(L, c - b + 1));
        ra = RA(i);  /* 'luav_concat' may invoke TMs and move the stack */
        rb = b + base;
        lua_setobjs2s(L, ra, rb);
        checkGC(L, (ra >= rb ? ra + 1 : rb));
        L->top = ci->top;  /* restore top */
      )
      vmcase(LUA_OP_JMP,
        dojump(ci, i, 0);
      )
      vmcase(LUA_OP_EQ,
        lua_TValue *rb = RKB(i);
        lua_TValue *rc = RKC(i);
        Protect(
          if (lua_cast_int(lua_equalobj(L, rb, rc)) != LUA_GETARG_A(i))
            ci->u.l.savedpc++;
          else
            donextjump(ci);
        )
      )
      vmcase(LUA_OP_LT,
        Protect(
          if (luaV_lessthan(L, RKB(i), RKC(i)) != LUA_GETARG_A(i))
            ci->u.l.savedpc++;
          else
            donextjump(ci);
        )
      )
      vmcase(LUA_OP_LE,
        Protect(
          if (luaV_lessequal(L, RKB(i), RKC(i)) != LUA_GETARG_A(i))
            ci->u.l.savedpc++;
          else
            donextjump(ci);
        )
      )
      vmcase(LUA_OP_TEST,
        if (LUA_GETARG_C(i) ? lua_isfalse(ra) : !lua_isfalse(ra))
            ci->u.l.savedpc++;
          else
          donextjump(ci);
      )
      vmcase(LUA_OP_TESTSET,
        lua_TValue *rb = RB(i);
        if (LUA_GETARG_C(i) ? lua_isfalse(rb) : !lua_isfalse(rb))
          ci->u.l.savedpc++;
        else {
          lua_setobjs2s(L, ra, rb);
          donextjump(ci);
        }
      )
      vmcase(LUA_OP_CALL,
        int b = LUA_GETARG_B(i);
        int nresults = LUA_GETARG_C(i) - 1;
        if (b != 0) L->top = ra+b;  /* else previous instruction set top */
        if (luaD_precall(L, ra, nresults)) {  /* C function? */
          if (nresults >= 0) L->top = ci->top;  /* adjust results */
          base = ci->u.l.base;
        }
        else {  /* Lua function */
          ci = L->ci;
          ci->callstatus |= LUA_CIST_REENTRY;
          goto newframe;  /* restart luaV_execute over new Lua function */
        }
      )
      vmcase(LUA_OP_TAILCALL,
        int b = LUA_GETARG_B(i);
        if (b != 0) L->top = ra+b;  /* else previous instruction set top */
        lua_assert(LUA_GETARG_C(i) - 1 == LUA_MULTRET);
        if (luaD_precall(L, ra, LUA_MULTRET))  /* C function? */
          base = ci->u.l.base;
        else {
          /* tail call: put called frame (n) in place of caller one (o) */
          lua_CallInfo *nci = L->ci;  /* called frame */
          lua_CallInfo *oci = nci->previous;  /* caller frame */
          lua_StkId nfunc = nci->func;  /* called function */
          lua_StkId ofunc = oci->func;  /* caller function */
          /* last stack slot filled by 'precall' */
          lua_StkId lim = nci->u.l.base + lua_getproto(nfunc)->numparams;
          int aux;
          /* close all upvalues from previous call */
          if (cl->p->sizep > 0) luaF_close(L, oci->u.l.base);
          /* move new frame into old one */
          for (aux = 0; nfunc + aux < lim; aux++)
            lua_setobjs2s(L, ofunc + aux, nfunc + aux);
          oci->u.l.base = ofunc + (nci->u.l.base - nfunc);  /* correct base */
          oci->top = L->top = ofunc + (L->top - nfunc);  /* correct top */
          oci->u.l.savedpc = nci->u.l.savedpc;
          oci->callstatus |= LUA_CIST_TAIL;  /* function was tail called */
          ci = L->ci = oci;  /* remove new frame */
          lua_assert(L->top == oci->u.l.base + lua_getproto(ofunc)->maxstacksize);
          goto newframe;  /* restart luaV_execute over new Lua function */
        }
      )
      vmcasenb(LUA_OP_RETURN,
        int b = LUA_GETARG_B(i);
        if (b != 0) L->top = ra+b-1;
        if (cl->p->sizep > 0) luaF_close(L, base);
        b = luaD_poscall(L, ra);
        if (!(ci->callstatus & LUA_CIST_REENTRY))  /* 'ci' still the called one */
          return;  /* external invocation: return */
        else {  /* invocation via reentry: continue execution */
          ci = L->ci;
          if (b) L->top = ci->top;
          lua_assert(isLua(ci));
          lua_assert(LUA_GET_OPCODE(*((ci)->u.l.savedpc - 1)) == LUA_OP_CALL);
          goto newframe;  /* restart luaV_execute over new Lua function */
        }
      )
      vmcase(LUA_OP_FORLOOP,
        lua_Number step = lua_nvalue(ra+2);
        lua_Number idx = luai_numadd(L, lua_nvalue(ra), step); /* increment index */
        lua_Number limit = lua_nvalue(ra+1);
        if (luai_numlt(L, 0, step) ? luai_numle(L, idx, limit)
                                   : luai_numle(L, limit, idx)) {
          ci->u.l.savedpc += LUA_GETARG_sBx(i);  /* jump back */
          lua_setnvalue(ra, idx);  /* update internal index... */
          lua_setnvalue(ra+3, idx);  /* ...and external index */
        }
      )
      vmcase(LUA_OP_FORPREP,
        const lua_TValue *init = ra;
        const lua_TValue *plimit = ra+1;
        const lua_TValue *pstep = ra+2;
        if (!luai_tonumber(init, ra))
          luaG_runerror(L, LUA_QL("for") " initial value must be a number");
        else if (!luai_tonumber(plimit, ra+1))
          luaG_runerror(L, LUA_QL("for") " limit must be a number");
        else if (!luai_tonumber(pstep, ra+2))
          luaG_runerror(L, LUA_QL("for") " step must be a number");
        lua_setnvalue(ra, luai_numsub(L, lua_nvalue(ra), lua_nvalue(pstep)));
        ci->u.l.savedpc += LUA_GETARG_sBx(i);
      )
      vmcasenb(LUA_OP_TFORCALL,
        lua_StkId cb = ra + 3;  /* call base */
        lua_setobjs2s(L, cb+2, ra+2);
        lua_setobjs2s(L, cb+1, ra+1);
        lua_setobjs2s(L, cb, ra);
        L->top = cb + 3;  /* func. + 2 args (state and index) */
        Protect(luaD_call(L, cb, LUA_GETARG_C(i), 1));
        L->top = ci->top;
        i = *(ci->u.l.savedpc++);  /* go to next instruction */
        ra = RA(i);
        lua_assert(LUA_GET_OPCODE(i) == LUA_OP_TFORLOOP);
        goto l_tforloop;
      )
      vmcase(LUA_OP_TFORLOOP,
        l_tforloop:
        if (!lua_ttisnil(ra + 1)) {  /* continue loop? */
          lua_setobjs2s(L, ra, ra + 1);  /* save control variable */
           ci->u.l.savedpc += LUA_GETARG_sBx(i);  /* jump back */
        }
      )
      vmcase(LUA_OP_SETLIST,
        int n = LUA_GETARG_B(i);
        int c = LUA_GETARG_C(i);
        int last;
        lua_Table *h;
        if (n == 0) n = lua_cast_int(L->top - ra) - 1;
        if (c == 0) {
          lua_assert(LUA_GET_OPCODE(*ci->u.l.savedpc) == LUA_OP_EXTRAARG);
          c = LUA_GETARG_Ax(*ci->u.l.savedpc++);
        }
        luai_runtimecheck(L, lua_ttistable(ra));
        h = lua_hvalue(ra);
        last = ((c-1)*LUA_LFIELDS_PER_FLUSH) + n;
        if (last > h->sizearray)  /* needs more space? */
          luaH_resizearray(L, h, last);  /* pre-allocate it at once */
        for (; n > 0; n--) {
          lua_TValue *val = ra+n;
          luaH_setint(L, h, last--, val);
          luaC_barrierback(L, lua_obj2gco(h), val);
        }
        L->top = ci->top;  /* correct top (in case of previous open call) */
      )
      vmcase(LUA_OP_CLOSURE,
        lua_Proto *p = cl->p->p[LUA_GETARG_Bx(i)];
        lua_Closure *ncl = getcached(p, cl->upvals, base);  /* cached closure */
        if (ncl == NULL)  /* no match? */
          pushclosure(L, p, cl->upvals, base, ra);  /* create a new one */
        else
          lua_setclLvalue(L, ra, ncl);  /* push cashed closure */
        checkGC(L, ra + 1);
      )
      vmcase(LUA_OP_VARARG,
        int b = LUA_GETARG_B(i) - 1;
        int j;
        int n = lua_cast_int(base - ci->func) - cl->p->numparams - 1;
        if (b < 0) {  /* B == 0? */
          b = n;  /* get all var. arguments */
          Protect(luaD_checkstack(L, n));
          ra = RA(i);  /* previous call may change the stack */
          L->top = ra + n;
        }
        for (j = 0; j < b; j++) {
          if (j < n) {
            lua_setobjs2s(L, ra + j, base - n + j);
          }
          else {
            lua_setnilvalue(ra + j);
          }
        }
      )
      vmcase(LUA_OP_EXTRAARG,
        lua_assert(0);
      )
    }
  }
}

