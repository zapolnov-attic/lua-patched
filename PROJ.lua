
SUBPROJECT("lua")

DEFINES {
	"LUA_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua.hpp\"",
	"LUA_VALUE_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua_value.hpp\"",
	"LUA_VM_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua_vm.hpp\"",
	"LUA_STACK_ENTRY_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua_stack_entry.hpp\"",
	"LUA_STACK_TOP_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua_stack_top.hpp\"",
	"LUA_FUNCTION_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua_function.hpp\"",
	"LUA_FILE_HPP=\"" .. CURRENT_SOURCE_DIR .. "/file.hpp\"",
	"LUA_LEXER_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lexer.hpp\"",
}

SOURCE_FILES {
	"file.cpp",
	"file.hpp",
	"lapi.c",
	"lapi.h",
	"lauxlib.cpp",
	"lauxlib.h",
	"lbaselib.c",
	"lbitlib.c",
	"lcode.c",
	"lcode.h",
	"lcorolib.c",
	"lctype.c",
	"lctype.h",
	"ldblib.c",
	"ldebug.c",
	"ldebug.h",
	"ldo.cpp",
	"ldo.h",
	"ldump.c",
	"lexer.cpp",
	"lexer.hpp",
	"lfunc.c",
	"lfunc.h",
	"lgc.c",
	"lgc.h",
	"linit.c",
	"llex.c",
	"llex.h",
	"llimits.h",
	"lmathlib.c",
	"lmem.c",
	"lmem.h",
	"lobject.c",
	"lobject.h",
	"lopcodes.c",
	"lopcodes.h",
	"lparser.c",
	"lparser.h",
	"lstate.c",
	"lstate.h",
	"lstring.c",
	"lstring.h",
	"lstrlib.c",
	"ltable.c",
	"ltable.h",
	"ltablib.c",
	"ltm.c",
	"ltm.h",
	"lua.h",
	"lua.hpp",
	"lua_function.hpp",
	"lua_function_impl.hpp",
	"lua_function_wrappers.hpp",
	"lua_stack_entry.cpp",
	"lua_stack_entry.hpp",
	"lua_stack_top.hpp",
	"lua_value.cpp",
	"lua_value.hpp",
	"lua_vm.cpp",
	"lua_vm.hpp",
	"luaconf.c",
	"luaconf.h",
	"lualib.h",
	"lundump.c",
	"lundump.h",
	"lvm.c",
	"lvm.h",
	"lzio.c",
	"lzio.h",
}

if HAS_SUBPROJECT("cxx-rtti") then
	DEFINES {
		"LUA_RTTI_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua_rtti.hpp\"",
		"LUA_META_OBJECT_HPP=\"" .. CURRENT_SOURCE_DIR .. "/lua_meta_object.hpp\"",
	}
	SOURCE_FILES {
		"lua_rtti.cpp",
		"lua_rtti.hpp",
		"lua_meta_object.cpp",
		"lua_meta_object.hpp",
	}
else
	print("WARNING: Recommended library 'cxx-rtti' was not added into the project.")
end

UTILITY("luai", { "lua.c", "lua_all.cpp" })
UTILITY("luac", { "luac.c", "lua_all.cpp" })
